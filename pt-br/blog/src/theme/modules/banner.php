<?php

/**
* Módulo:
* ***** Banner - Page Template *****
*
* @package WordPress
* @subpackage Grano Studio
* @since Grano Studio 1.0
 */

function module_bannerfotos($bannerfotos, $key){
    echo '<div id="grano-carousel" class="owl-carousel-'.$key.' owl-theme">';
    foreach ( (array) $bannerfotos as $attachment_id => $attachment_url ) {
      $img_url = wp_get_attachment_image_src( $attachment_id, 'large'  );
        echo '<div class="item" style="background-image:url('.$img_url[0].')"></div>';
    }
    echo '</div>';
}
 ?>
