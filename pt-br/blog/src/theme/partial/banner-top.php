<div class="blog-banner">
  <div class="content">
    <div class="cat">
      <a href="#">Destaque</a>, <a href="#">categoria A</a>, <a href="#">categoria B</a>
    </div>
    <h1>Loren ipsum dolor sit amet adispicing consectectur</h1>
    <div class="btn-lg btn-spread icon-seta">
      Leia na íntegra
      <span></span>
    </div>
  </div>
    <div class="mask">
      <svg
     version="1.1"
     id="no-aspect-ratio"
     xmlns="http://www.w3.org/2000/svg"
     xmlns:xlink="http://www.w3.org/1999/xlink"
     x="0px"
     y="0px"
     height="100%"
     width="100%"
     viewBox="0 0 100 100"
     preserveAspectRatio="none">
        <polygon fill="white" points="100,100 0,100 0,0 "/>
      </svg>
    </div>
</div>
