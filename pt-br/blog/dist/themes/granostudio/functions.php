<?php
/**
 * Grano Studio functions and definitions
 *
 *
 * @package WordPress
 * @subpackage Grano Studio
 * @since Grano Studio 1.0
 */

/**
 * granostudio only works in WordPress 4.4 or later.
 */
if ( version_compare( $GLOBALS['wp_version'], '4.4-alpha', '<' ) ) {
	require get_template_directory() . '/inc/back-compat.php';
}

if ( ! function_exists( 'granostudio_setup' ) ) :
	/* Disable WordPress Admin Bar for all users but admins. */
	show_admin_bar(false);
	// SEGURANÇA
	// mudar id o admin
	$user_admin = get_user_by('ID','1');
	if(!empty($user_admin)){
		global $wpdb;
		$wpdb->update( $table_prefix.'users', array( 'ID' => "1024"), array( 'ID' => "1") );
		$wpdb->update( $table_prefix.'usermeta', array( 'user_id' => "1024"), array( 'user_id' => "1") );
	}

	remove_action('wp_head','wp_generator');

	//atualização automatica dos plugins
	add_filter( 'auto_update_plugin', '__return_true' );

	// remover/esconder versão so wp
	remove_action('wp_head', 'wp_generator');


	// PHPMAILER FOR CONTACT FORM 7
	function create_phpmailer( $phpmailer ) {
			$email_admin = get_option('admin_email');
		  $phpmailer->Sender   = $email_admin;                  // enable SMTP authentication
	}
	add_action( 'phpmailer_init', 'create_phpmailer' );



/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 */
function granostudio_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on Twenty Sixteen, use a find and replace
	 * to change 'granostudio' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'granostudio', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );


	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 1200, 9999 );


	// MENU CONFIGURAÇ˜AO =======================================================
	require_once('inc/grano-bootstrap_navwalker.php');

	// This theme uses wp_nav_menu() in two locations.
	register_nav_menus( array(
		'primary' => __( 'Menu Superior', 'granostudio' ),
		'footer'  => __( 'Rodapé', 'granostudio' ),
	) );

	// Check if the menu exists
	$menu_name = 'Menu Superior';
	$menu_exists = wp_get_nav_menu_object( $menu_name );

	// If it doesn't exist, let's create it.
	if( !$menu_exists){
	    $menu_id = wp_create_nav_menu($menu_name);

		// Set up default menu items
	    wp_update_nav_menu_item($menu_id, 0, array(
	        'menu-item-title' =>  __('Home'),
	        'menu-item-classes' => 'home',
	        'menu-item-url' => home_url( '/' ),
	        'menu-item-status' => 'publish'));

	}

	// /MENU CONFIGURAÇ˜AO =====================================================

	// SIBABAR ===================================================================
	if ( function_exists('register_sidebar') )
		register_sidebar(array(
				'name'          => __( 'Sidebar Blog', 'granoexpresso' ),
				'id'            => 'sidebar_blog',
				'description'   => '',
        'class'         => '',
				'before_widget' => '<li id="%1$s" class="widget %2$s">',
				'after_widget'  => '</li>',
				'before_title'  => '<h2 class="widgettitle">',
				'after_title'   => '</h2>'
	));
	// /SIBABAR ==================================================================

		/**
	 * Filter the except length to 20 characters.
	 *
	 * @param int $length Excerpt length.
	 * @return int (Maybe) modified excerpt length.
	 */
		function wpdocs_custom_excerpt_length( $length ) {
		    return 20;
		}
		add_filter( 'excerpt_length', 'wpdocs_custom_excerpt_length', 999 );
		function wpdocs_excerpt_more( $more ) {
		    return '...<a href="'.get_permalink( get_the_ID() ).'">Leia mais.</a>';
		}
		add_filter( 'excerpt_more', 'wpdocs_excerpt_more' );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 *
	 * See: https://codex.wordpress.org/Post_Formats
	 */
	// add_theme_support( 'post-formats', array(
	// 	'aside',
	// 	'image',
	// 	'video',
	// 	'quote',
	// 	'link',
	// 	'gallery',
	// 	'status',
	// 	'audio',
	// 	'chat',
	// ) );


// Grano Studio Expresso Includes

	//Classes úteis
	include 'inc/grano-classes.php';

	//CUstom POst Type
	include 'inc/grano-cpt.php';
	//custom metabox
	include 'inc/cmb2-conf.php';

	//Custom admin users
	include 'inc/grano-useradmin.php';

	//Custom roles
	include 'inc/grano-roles.php';

	//Admin customize
	include 'inc/grano-admincustomize.php';

	//Grano Expresso Options
	include 'inc/grano-themeoptions.php';

	//Page Template Create
	include 'inc/grano-pagetemplate.php';

	//Page Builder
	include 'inc/grano-pagebuilder.php';

	//Page Template Create
	include 'inc/grano-cadastro.php';

	// // Grano Social Share
	// include 'inc/grano-social-share.php';


	// Modules:
	// *** Banner
	include get_stylesheet_directory().'/modules/banner.php';
	include get_stylesheet_directory().'/modules/contato.php';
	include get_stylesheet_directory().'/modules/equipe.php';
	include get_stylesheet_directory().'/modules/posts.php';
	include get_stylesheet_directory().'/modules/portfolio.php';
	include get_stylesheet_directory().'/modules/bannerconteudo.php';
	include get_stylesheet_directory().'/modules/fotocomtexto.php';
	include get_stylesheet_directory().'/modules/textocombotao.php';




	//Require Plgugins com Tgm
	require_once get_template_directory() . '/PluginActivate/class-tgm-plugin-activation.php';
	add_action( 'tgmpa_register', 'cmb2_require_register_required_plugins' );

	function cmb2_require_register_required_plugins() {
		/*
		 * Array of plugin arrays. Required keys are name and slug.
		 * If the source is NOT from the .org repo, then source is also required.
		 */
		$plugins = array(

			// This is an example of how to include a plugin bundled with a theme.
			array(
				'name'               => 'cmb2', // The plugin name.
				'slug'               => 'cmb2', // The plugin slug (typically the folder name).
				'source'             => get_template_directory() . '/lib/cmb2.zip', // The plugin source.
				'required'           => true, // If false, the plugin is only 'recommended' instead of required.
				'version'            => '', // E.g. 1.0.0. If set, the active plugin must be this version or higher. If the plugin version is higher than the plugin version installed, the user will be notified to update the plugin.
				'force_activation'   => true, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch.
				'force_deactivation' => false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins.
				'external_url'       => '', // If set, overrides default API URL and points to an external URL.
				'is_callable'        => '', // If set, this callable will be be checked for availability to determine if a plugin is active.
			),
			array(
				'name'               => 'cmb2-relation', // The plugin name.
				'slug'               => 'cmb2-relation', // The plugin slug (typically the folder name).
				'source'             => get_template_directory() . '/lib/cmb2-relations.zip', // The plugin source.
				'required'           => true, // If false, the plugin is only 'recommended' instead of required.
				'version'            => '', // E.g. 1.0.0. If set, the active plugin must be this version or higher. If the plugin version is higher than the plugin version installed, the user will be notified to update the plugin.
				'force_activation'   => true, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch.
				'force_deactivation' => false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins.
				'external_url'       => '', // If set, overrides default API URL and points to an external URL.
				'is_callable'        => '', // If set, this callable will be be checked for availability to determine if a plugin is active.
			),
			array(
				'name'               => 'Contact Form 7', // The plugin name.
				'slug'               => 'contact-form-7', // The plugin slug (typically the folder name).
				'source'             => get_template_directory() . '/lib/contact-form-7.4.4.2.zip', // The plugin source.
				'required'           => true, // If false, the plugin is only 'recommended' instead of required.
				'version'            => '', // E.g. 1.0.0. If set, the active plugin must be this version or higher. If the plugin version is higher than the plugin version installed, the user will be notified to update the plugin.
				'force_activation'   => true, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch.
				'force_deactivation' => false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins.
				'external_url'       => '', // If set, overrides default API URL and points to an external URL.
				'is_callable'        => '', // If set, this callable will be be checked for availability to determine if a plugin is active.
			),
			array(
				'name'               => 'Login LockDown', // The plugin name.
				'slug'               => 'login-lockdown', // The plugin slug (typically the folder name).
				'source'             => get_template_directory() . '/lib/login-lockdown.1.7.1.zip', // The plugin source.
				'required'           => true, // If false, the plugin is only 'recommended' instead of required.
				'version'            => '', // E.g. 1.0.0. If set, the active plugin must be this version or higher. If the plugin version is higher than the plugin version installed, the user will be notified to update the plugin.
				'force_activation'   => true, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch.
				'force_deactivation' => false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins.
				'external_url'       => '', // If set, overrides default API URL and points to an external URL.
				'is_callable'        => '', // If set, this callable will be be checked for availability to determine if a plugin is active.
			)
		);

		//settings
		$config = array(
			'id'           => 'cmb2-require',                 // Unique ID for hashing notices for multiple instances of TGMPA.
			'default_path' => '',                      // Default absolute path to bundled plugins.
			'menu'         => 'tgmpa-install-plugins', // Menu slug.
			'has_notices'  => true,                    // Show admin notices or not.
			'dismissable'  => true,                    // If false, a user cannot dismiss the nag message.
			'dismiss_msg'  => 'O Plguin CMB2 não pode ser instalado, instale-o manualmente',                      // If 'dismissable' is false, this message will be output at top of nag.
			'is_automatic' => false,                   // Automatically activate plugins after installation or not.
			'message'      => 'Active agora mesmo o Plgugin CMB2',                      // Message to output right before the plugins table.

		);

		tgmpa( $plugins, $config );
	}


}
endif; // granostudio_setup
add_action( 'after_setup_theme', 'granostudio_setup' );






function granostudio_javascript_detection() {
	echo "<script>(function(html){html.className = html.className.replace(/\bno-js\b/,'js')})(document.documentElement);</script>\n";
}
add_action( 'wp_head', 'granostudio_javascript_detection', 0 );




/**
 * Enqueues scripts and styles.
 *
 */

function granostudio_scripts() {

	//Desabilitar jquery
	wp_deregister_script( 'jquery' );

	// Theme stylesheet.
	wp_enqueue_style( 'granostudio-style', get_stylesheet_uri() );

	// import fonts (Google Fonts)
	wp_enqueue_style('granostudio-style-fonts', get_template_directory_uri() . '/css/fonts/fonts.css');
	// Theme front-end stylesheet
	wp_enqueue_style('angular-css', get_template_directory_uri() . '/css/angular.css');
	wp_enqueue_style('granostudio-style-front', get_template_directory_uri() . '/css/main.css');

	//Load angular
	// wp_enqueue_script('angularjs', get_stylesheet_directory_uri() .'/js/dist/angular.min.js', array(), '00002');


	// wp_register_script( 'scripts',  get_stylesheet_directory_uri() . '/js/dist/scripts.min.js' );


	// Angular get_stylesheet_directory_uri()
	wp_localize_script('scripts', 'localized',
	            array(
	                'partials' => get_template_directory_uri() . '/partials/',
									'homesite' => "http://" . $_SERVER['SERVER_NAME']
	                )
	    );

	// wp_enqueue_script('scripts', get_stylesheet_directory_uri() . '/js/dist/scripts.min.js', array( 'angularjs'));


}
add_action( 'wp_enqueue_scripts', 'granostudio_scripts' );

/**
 * Enqueues scripts and style for ADMIN`
 *
 */

function load_admin_scripts()
{
		wp_enqueue_style('admin-angular-css', get_template_directory_uri() . '/css/admin.angular.css');
		wp_enqueue_style('admin-style', get_template_directory_uri() . '/css/admin.css');

    wp_enqueue_script("admin-angularjs", get_stylesheet_directory_uri() .'/js/dist/admin.angular.min.js', false);
		wp_register_script( 'admin-scripts',  get_stylesheet_directory_uri() . '/js/dist/admin.scripts.js' );
		wp_enqueue_script('admin-scripts', get_stylesheet_directory_uri() . '/js/dist/admin.scripts.js', array( 'admin-angularjs'));

		//pagebuilder
		$pagebuildersave = get_post_meta(get_the_ID(), 'complete_layout_data', true);
		$pluginsgranoativos = get_option('grano-plugin-ativo', true);


		wp_localize_script('admin-scripts', 'localized',
		            array(
		                'templateUrl' => get_template_directory_uri(),
										'homesite' => "http://" . $_SERVER['SERVER_NAME'],
										'root' => esc_url_raw( rest_url() ),
								    'nonce' => wp_create_nonce( 'wp_rest' ),
										'pagebuilder' => $pagebuildersave,
										'pluginativos'=> $pluginsgranoativos
		                )
		    );
}

add_action("admin_enqueue_scripts", "load_admin_scripts");

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';
