<?php
/**
 * Grano Site Page Template
 *
 * @author Grano studio
 */

/**
 *
 */
class PageTemplate
{
  // lista de modelos
  private $modelosOptions = array();

  // lista de formulários de contato
  private $contactOptions = array();

  function __construct()
  {
    # code...
  }

  /**
   * Initiate our hooks
   */
  public function hooks() {
      $this->contactShotcodes();
      $this->modelosCheck();
      // add_action( 'cmb2_admin_init', array($this, 'createMetaboxPage') );
      add_action( 'init', array($this, 'editorTexto') );

  }

  // function createMetaboxPage()
  // {
  //   // Start with an underscore to hide fields from custom fields list
  //   $prefix = 'page_';
  //
  //   /**
  //    * Initiate the metabox
  //    */
  //   $cmb = new_cmb2_box( array(
  //       'id'            => 'page_template',
  //       'title'         => __( 'Layout', 'cmb2' ),
  //       'object_types'  => array( 'page', ), // Post type
  //       'context'       => 'normal',
  //       'priority'      => 'high',
  //       'show_names'    => true, // Show field names on the left
  //       // 'cmb_styles' => false, // false to disable the CMB stylesheet
  //       // 'closed'     => true, // Keep the metabox closed by default
  //   ) );
  //
  //   // group field
  //   $group_field_id = $cmb->add_field( array(
  //         'id'          => $prefix.'layout',
  //         'type'        => 'group',
  //         'description' => '',
  //         // 'repeatable'  => false, // use false if you want non-repeatable group
  //         'options'     => array(
  //             'group_title'   => __( 'Módulo {#}', 'granostudio' ), // since version 1.1.4, {#} gets replaced by row number
  //             'add_button'    => __( 'Add outro Módulo', 'granostudio' ),
  //             'remove_button' => __( 'Remover', 'granostudio' ),
  //             'sortable'      => true, // beta
  //             // 'closed'     => true, // true to have the groups closed by default
  //         ),
  //     ) );
  //     $cmb->add_group_field( $group_field_id, array(
  //         'name'             => 'Modelo',
  //         'desc'             => '',
  //         'id'               => $prefix.'modulo',
  //         'type'             => 'select',
  //         'show_option_none' => true,
  //         'default'          => 'custom',
  //         'options'          => $this->modelosOptions,
  //     ) );
  //     //Fotos para Banner
  //     $cmb->add_group_field( $group_field_id, array(
  //           'name' => 'Fotos para Banner',
  //           'desc' => '',
  //           'id'   => $prefix.'bannerfotos',
  //           'type' => 'file_list',
  //           'preview_size' => array( 100, 100 ), // Default: array( 50, 50 )
  //           // Optional, override default text strings
  //           'text' => array(
  //               'add_upload_files_text' => 'Add Foto', // default: "Add or Upload Files"
  //               'remove_image_text' => 'Replacement', // default: "Remove Image"
  //               'file_text' => 'Replacement', // default: "File:"
  //               'file_download_text' => 'Replacement', // default: "Download"
  //               'remove_text' => 'Replacement', // default: "Remove"
  //           ),
  //     ) );
  //     //Shortcode do formulário de contato
  //     $cmb->add_group_field( $group_field_id, array(
  //             'name'             => 'Formulário de contato',
  //             'desc'             => "Crie o seu formulário no menu <a herf='?page=wpcf'>Contact</a>",
  //             'id'               => $prefix.'formcontato',
  //             'type'             => 'select',
  //             'show_option_none' => true,
  //             'default'          => 'custom',
  //             'options'          => $this->contactOptions,
  //     ) );
  //     //Link para modelo externo
  //     $cmb->add_group_field( $group_field_id, array(
  //             'name'    => 'Link para modelo externo',
  //             'desc'    => "Insira apenas o nome do arquivo com a extensão (.html,.php...)",
  //             'default' => '',
  //             'id'      => $prefix.'modeloexterno',
  //             'type'    => 'text_medium'
  //     ) );
  //     // Banner com conteudo
  //     $cmb->add_group_field($group_field_id, array(
  //         'name'        => 'Banner com conteúdo',
  //         'id'          => $prefix.'bannerconteudo',
  //         'type'        => 'post_search_text', // This field type
  //         // post type also as array
  //         'post_type'   => 'banner',
  //         // Default is 'checkbox', used in the modal view to select the post type
  //         'select_type' => 'radio',
  //         // Will replace any selection with selection from modal. Default is 'add'
  //         'select_behavior' => 'replace',
  //     ) );
  //     // Foto com texto
  //     $cmb->add_group_field($group_field_id, array(
  //         'name'    => 'Posição da foto',
  //         'id'      => $prefix.'fotocomtexto_pos',
  //         'type'    => 'radio_inline',
  //         'options' => array(
  //             'direita' => __( 'Direita', 'cmb' ),
  //             'esquerda'   => __( 'Esquerda', 'cmb' ),
  //         ),
  //     ) );
  //     $cmb->add_group_field($group_field_id, array(
  //         'name'        => 'Foto',
  //         'id'          => $prefix.'fotocomtexto_foto',
  //         'type'    => 'file',
  //         // Optionally hide the text input for the url:
  //         'options' => array(
  //             'url' => false,
  //         ),
  //     ) );
  //     $cmb->add_group_field($group_field_id, array(
  //         'name'    => 'Texto',
  //         // 'desc'    => 'field description (optional)',
  //         'id'      => $prefix.'fotocomtexto_texto',
  //         'type'    => 'wysiwyg',
  //         'options' => array(),
  //     ) );
  //     $cmb->add_group_field($group_field_id, array(
  //         'name'    => 'Titulo',
  //         // 'desc'    => 'field description (optional)',
  //         'id'      => $prefix.'textocombotao_titulo',
  //         'type'    => 'text_medium',
  //         'options' => array(),
  //     ) );
  //     $cmb->add_group_field($group_field_id, array(
  //         'name'    => 'Conteudo',
  //         // 'desc'    => 'field description (optional)',
  //         'id'      => $prefix.'textocombotao_conteudo',
  //         'type'    => 'wysiwyg',
  //         'options' => array(),
  //     ) );
  //     $cmb->add_group_field($group_field_id, array(
  //         'name'    => 'Link: Titulo',
  //         // 'desc'    => 'field description (optional)',
  //         'id'      => $prefix.'textocombotao_link_titulo',
  //         'type'    => 'text_medium',
  //         'options' => array(),
  //     ) );
  //     $cmb->add_group_field($group_field_id, array(
  //         'name'    => 'Link: URL',
  //         // 'desc'    => 'field description (optional)',
  //         'id'      => $prefix.'textocombotao_link_url',
  //         'type'    => 'text_medium',
  //         'options' => array(),
  //     ) );
  //
  // }

  // checar quais tipos de conteudos estão habilitados para depois habilitar os modelos
  private function modelosCheck()
  {
    $modelosCheck = new modelosCheck();
    $ativos = $modelosCheck->ativos();
    // print_r($posts_type);
    $newModelos = array(
      'Banner' => 'Banner',
      'Contato' => 'Contato',
      'Equipe' => 'Equipe',
      'Externo' => 'Modelo Externo',
      'Fotocomtexto' => 'Foto com texto',
      'Textocombotao' => 'Texto com botão'
    );

    if(!empty($ativos)){
      // modulos ativos
      foreach ($ativos as $types => $type) {
        if($type=='post') {
          $newModelos['Pots'] = 'Pots';
        }
        if($type=='clientes') {
          $newModelos['Clientes'] = 'Clientes';
        }
        if($type=='portfolio') {
          $newModelos['Portfolio'] = 'Portfolio';
        }
        if($type=='banner') {
          $newModelos['Bannerconteudo'] = 'Banner com conteúdo';
        }
      }
    }

    $this->modelosOptions = $newModelos;

  }

  // listar todos os shotcodes do contact form
  private function contactShotcodes()
  {
    // acessar o database
    global $wpdb;
    $database = $wpdb->get_results(
    	"
    	SELECT ID, post_name
    	FROM $wpdb->posts
    	WHERE post_type = 'wpcf7_contact_form'
    	"
    );

    // criar os options
    foreach ($database as $key => $value) {
      $this->contactOptions[$value->ID] = $value->post_name;
    }

  }


  //Desativar Editor de Texto para determiado tipo de conteudo
  function editorTexto()
  {
      global $_wp_post_type_features;

      $post_type = 'page';
      $feature = "editor";
      if ( !isset($_wp_post_type_features[$post_type]) )
      {

      }
      elseif ( isset($_wp_post_type_features[$post_type][$feature]) )
                      unset($_wp_post_type_features[$post_type][$feature]);
  }
}


//iniciar Class Page Template
$pageTemplate = new PageTemplate();
$pageTemplate->hooks();


//add styles
add_action('admin_init', 'pageTemplateStyle');
function pageTemplateStyle(){
  wp_enqueue_style('granostudio-pagetemplate', get_template_directory_uri() . '/css/grano-pagetemplate.css');
}

//add scripts
// add_action('admin_head', 'pageTemplateScripts');
// function pageTemplateScripts(){
//   wp_enqueue_script('grano-pagetemplate', get_template_directory_uri() . '/js/dist/grano-pagetemplate.min.js');
// }
