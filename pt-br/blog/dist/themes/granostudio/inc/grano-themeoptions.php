<?php
/**
 * Grano Site Options
 *
 * @author    Grano studio
 */
class my_Admin {

    /**
     * Default Option key
     * @var string
     */
    private $key = 'granostudio';

    /**
     * Array of metaboxes/fields
     * @var array
     */
    protected $option_metabox = array();

    /**
     * Options Page title
     * @var string
     */
    protected $title = '';

    /**
     * Options Tab Pages
     * @var array
     */
    protected $options_pages = array();

    /**
     * Constructor
     * @since 0.1.0
     */
    public function __construct() {
        // Set our title
        $this->title = __( 'Site Options', 'granostudio' );
    }

    /**
     * Initiate our hooks
     * @since 0.1.0
     */
    public function hooks() {
        add_action( 'admin_init', array( $this, 'init' ) );
        add_action( 'admin_menu', array( $this, 'add_options_page' ) ); //create tab pages
        add_action( 'cmb2_admin_init', array( $this, 'add_options_page_metabox' ) );
    }

    function add_options_page_metabox(){
      $option_tabs = self::option_fields();
      // print_r($option_tabs);
      foreach ($option_tabs as $option_tab){

        add_action( "cmb2_save_options-page_fields_".$option_tab['id'], array( $this, 'settings_notices' ), 10, 2 );


        $cmb = new_cmb2_box( array(
      			'id'         => $option_tab['id'],
      			'hookup'     => false,
      			'cmb_styles' => true,
            'show_on'    => $option_tab['show_on'],
      		) );

          foreach ($option_tab['fields'] as $field) {
            // print_r($field);
            if (isset($field['options'])){
              $cmb->add_field( array(
          			'name' => __( $field['name'], 'granostudio' ),
          			'desc' => __( $field['desc'], 'granostudio' ),
          			'id'   => $field['id'],
          			'type' => $field['type'],
          			'default' => 'Default Text',
                'options' => $field['options']
          		) );
            }else{
              $cmb->add_field( array(
          			'name' => __( $field['name'], 'granostudio' ),
          			'desc' => __( $field['desc'], 'granostudio' ),
          			'id'   => $field['id'],
          			'type' => $field['type'],
          			'default' => 'Default Text'
          		) );
            }

          }

      }
    }

    function cmb2_option(){

    }

    /**
     * Register our setting tabs to WP
     * @since  0.1.0
     */
    public function init() {
    	$option_tabs = self::option_fields();
        foreach ($option_tabs as $index => $option_tab) {
        	register_setting( $option_tab['id'], $option_tab['id'] );
        }
    }

    /**
     * Add menu options page
     * @since 0.1.0
     */
    public function add_options_page() {
        $option_tabs = self::option_fields();
        foreach ($option_tabs as $index => $option_tab) {
        		$this->options_pages[] = add_submenu_page( 'options-general.php', $option_tab['title'], $option_tab['title'], 'manage_options', $option_tab['id'], array( $this, 'admin_page_display' ) );

        }
    }

    /**
     * Admin page markup. Mostly handled by CMB
     * @since  0.1.0
     */
    public function admin_page_display() {
    	$option_tabs = self::option_fields(); //get all option tabs
      $page_atual = $_GET["page"];
    	// $tab_forms = array();
        ?>
        <div class="wrap cmb_options_page themeoptions<?php echo $this->key; ?>">
          <?php
                // print_r($option_tabs);
                // echo $page_atual;
                  foreach ($option_tabs as $option_tab){
                    if($option_tab['id']==$page_atual){
                      ?>
                      <h2><?php echo esc_html( get_admin_page_title() ); ?></h2>

                      <div id="<?php esc_attr_e($option_tab['id']); ?>" class="group">
                          <p><?php echo  $option_tab['desc']?></p>
                        <?php cmb2_metabox_form( $option_tab['id'], $option_tab['id'] ); ?>
                      </div>
                      <?php
                    }
                  }
                ?>

        </div>
        <?php
    }

    /**
     * Defines the theme option metabox and field configuration
     * @since  0.1.0
     * @return array
     */
    public function option_fields() {

        // Only need to initiate the array once per page-load
        if ( ! empty( $this->option_metabox ) ) {
            return $this->option_metabox;
        }

        $this->option_metabox[] = array(
            'id'         => 'seo', //id used as tab page slug, must be unique
            'title'      => 'SEO',
            'desc'       => 'Preencha todos os campos para que o seu site seja bem visto nos mecânimos de busca e redes sociais.',
            'show_on'    => array( 'key' => 'options-page', 'value' => array( 'redes_sociais' ), ), //value must be same as id
            'show_names' => true,
            'fields'     => array(
                        				array(
                        					'name' => __('Imagem default', 'granostudio'),
                        					'desc' => __('Imagem padrão quando não há imagem destaque na página ou post.', 'granostudio'),
                        					'id' => 'seo_img', //each field id must be unique
                        					'default' => '',
                        					'type' => 'file',
                        				),
                        				array(
                        					'name' => __('Keywords', 'granostudio'),
                        					'desc' => __('Liste com vírgula as palavras que combinam com o seu site.', 'granostudio'),
                        					'id' => 'seo_keywords',
                        					'default' => '',
                        					'type' => 'textarea_small',
                        				),
                        				array(
                                  'name' => __('Google Analytics', 'granostudio'),
                        					'desc' => __('Insira o código', 'granostudio'),
                        					'id' => 'seo_analytics',
                        					'default' => '',
                        					'type' => 'textarea_small',
                        				)
                        			)
                            );

        $this->option_metabox[] = array(
            'id'         => 'info_contato',
            'title'      => 'Informações de Contato',
            'desc'       => '',
            'show_on'    => array( 'key' => 'options-page', 'value' => array( 'options-page' ), ),
            'show_names' => true,
            'fields'     => array(
                        				array(
                        					'name' => __('Facebook Username', 'granostudio'),
                        					'desc' => __('Username of Facebook page.', 'granostudio'),
                        					'id' => 'cont_facebook',
                        					'default' => '',
                        					'type' => 'text'
                        				),
                        				array(
                        					'name' => __('Twitter Username', 'granostudio'),
                        					'desc' => __('Username of Twitter profile.', 'granostudio'),
                        					'id' => 'cont_twitter',
                        					'default' => '',
                        					'type' => 'text'
                        				),
                        				array(
                        					'name' => __('Instagram Username', 'granostudio'),
                        					'desc' => __('Username of Instagram profile.', 'granostudio'),
                        					'id' => 'cont_instagram',
                        					'default' => '',
                        					'type' => 'text'
                        				),
                        				array(
                        					'name' => __('Youtube Username', 'granostudio'),
                        					'desc' => __('Username of Youtube channel.', 'granostudio'),
                        					'id' => 'cont_youtube',
                        					'default' => '',
                        					'type' => 'text'
                        				),
                        				array(
                        					'name' => __('Flickr Username', 'granostudio'),
                        					'desc' => __('Username of Flickr profile.', 'granostudio'),
                        					'id' => 'cont_flickr',
                        					'default' => '',
                        					'type' => 'text'
                        				),
                        				array(
                        					'name' => __('LinkedIn Username', 'granostudio'),
                        					'desc' => __('Username of LinkedIn profile.', 'granostudio'),
                        					'id' => 'cont_linkedin',
                        					'default' => '',
                        					'type' => 'text'
                        				),
                        				array(
                        					'name' => __('Google+ Profile ID', 'granostudio'),
                        					'desc' => __('ID of Google+ profile.', 'granostudio'),
                        					'id' => 'cont_googleplus',
                        					'default' => '',
                        					'type' => 'text'
                        				),
                        				array(
                        					'name' => __('Google Maps', 'granostudio'),
                        					'desc' => __('Google Maps code.', 'granostudio'),
                        					'id' => 'cont_googlepmaps',
                        					'default' => '',
                        					'type' => 'textarea_small'
                        				),
                        				array(
                        					'name' => __('Telefone', 'granostudio'),
                        					'desc' => __('Telefone de contato.', 'granostudio'),
                        					'id' => 'cont_telefone',
                        					'default' => '',
                        					'type' => 'text'
                        				),
                        				array(
                        					'name' => __('Email', 'granostudio'),
                        					'desc' => __('Email de contato.', 'granostudio'),
                        					'id' => 'cont_email',
                        					'default' => '',
                        					'type' => 'text_email'
                        				),
                        				array(
                        					'name' => __('Endereço', 'granostudio'),
                        					'desc' => __('Endereço de contato.', 'granostudio'),
                        					'id' => 'cont_endereço',
                        					'default' => '',
                        					'type' => 'text'
                        				),
                        			)
        );

        $this->option_metabox[] = array(
            'id'         => 'design',
            'title'      => 'Design',
            'desc'       => '',
            'show_on'    => array( 'key' => 'options-page', 'value' => array( 'social_options' ), ),
            'show_names' => true,
            'fields'     => array(
            	                   array(
                          					'name' => __('Logo', 'granostudio'),
                          					'desc' => __('Logo do site.', 'granostudio'),
                          					'id' => 'design_logo',
                          					'default' => 'http://default',
                          					'type' => 'file',
                          				),
                          				array(
                          					'name' => __('Fav Icon', 'granostudio'),
                          					'desc' => __('Fav icon do site.', 'granostudio'),
                          					'id' => 'design_favicon',
                          					'default' => '',
                          					'type' => 'file',
                          				),
                          				array(
                          					'name' => __('Custom CSS', 'granostudio'),
                          					'desc' => __('Custom CSS para alterações que precisam ser imediatamente feitas.', 'granostudio'),
                          					'id' => 'design_customcss',
                          					'default' => '',
                          					'type' => 'textarea',
                          				)
                          			)
        );
        return $this->option_metabox;
    }

    /**
  	 * Register settings notices for display
  	 *
  	 * @since  0.1.0
  	 * @param  int   $object_id Option key
  	 * @param  array $updated   Array of updated fields
  	 * @return void
  	 */
  	public function settings_notices( $object_id, $updated ) {
  		if ( $object_id !== $this->key || empty( $updated ) ) {
  			return;
  		}
  		add_settings_error( $this->key . '-notices', '', __( 'Settings updated.', 'granostudio' ), 'updated' );
  		settings_errors( $this->key . '-notices' );
  	}

    /**
     * Returns the option key for a given field id
     * @since  0.1.0
     * @return array
     */
    public function get_option_key($field_id) {
    	$option_tabs = $this->option_fields();
    	foreach ($option_tabs as $option_tab) { //search all tabs
    		foreach ($option_tab['fields'] as $field) { //search all fields
    			if ($field['id'] == $field_id) {
    				return $option_tab['id'];
    			}
    		}
    	}
    	return $this->key; //return default key if field id not found
    }

    /**
     * Public getter method for retrieving protected/private variables
     * @since  0.1.0
     * @param  string  $field Field to retrieve
     * @return mixed          Field value or exception is thrown
     */
    public function __get( $field ) {

        // Allowed fields to retrieve
        if ( in_array( $field, array( 'key', 'fields', 'title', 'options_pages' ), true ) ) {
            return $this->{$field};
        }
        if ( 'option_metabox' === $field ) {
            return $this->option_fields();
        }

        throw new Exception( 'Invalid property: ' . $field );

    }

}


  $myAdmin = new my_Admin();
  $myAdmin->hooks();
