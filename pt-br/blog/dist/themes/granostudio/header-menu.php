<!-- NAV BAR ============================================================== -->

<nav class="navbar navbar-fixed-top navbar-default" role="navigation">
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="<?php echo get_home_url(); ?>">
          <?php
          $logo_image_url = grano_get_options('design','design_logo');
          if(empty($logo_image_url) || $logo_image_url == "http://default"){
            echo bloginfo('name');

          }else{
            echo '<img src="'.$logo_image_url.'" alt="'.get_bloginfo('name').'" class="navbar-brand-img" />';
          }
           ?>
        </a>
    </div>
        <?php
            wp_nav_menu( array(
                'menu'              => 'primary',
                'theme_location'    => 'primary',
                'depth'             => 2,
                'container'         => 'div',
                'container_class'   => 'navbar-left navbar-collapse collapse',
                'container_id'      => 'bs-example-navbar-collapse-1',
                'menu_class'        => 'nav navbar-nav',
                'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
                'walker'            => new wp_bootstrap_navwalker())
            );
        ?>

        <?php
        // social links
        $social_media = get_option('info_contato');
        // remover elementos que não irão aparecer no header
        if(!empty($social_media['cont_googlepmaps'])){
            unset($social_media['cont_googlepmaps']);
        }
        if(!empty($social_media['cont_telefone'])){
            unset($social_media['cont_telefone']);
        }
        if(!empty($social_media['cont_email'])){
            unset($social_media['cont_email']);
        }
        if(!empty($social_media['cont_endereço'])){
            unset($social_media['cont_endereço']);
        }

        if (!empty($social_media)) {
          ?>
          <ul class="navbar-social navbar-right">
            <?php foreach ($social_media as $key => $value) {

                    switch ($key) {
                      case 'cont_facebook':
                        if($value!='Default Text'){
                          echo '<li class="icon facebook"><a href="'.$value.'">Facebook</a></li>';
                        }
                      break;
                      case 'cont_twitter':
                        if($value!='Default Text'){
                          echo '<li class="icon twitter"><a href="'.$value.'">Twitter</a></li>';
                        }
                      break;
                      case 'cont_instagram':
                        if($value!='Default Text'){
                          echo '<li class="icon instagram"><a href="'.$value.'">Instagram</a></li>';
                        }
                      break;
                      case 'cont_youtube':
                        if($value!='Default Text'){
                          echo '<li class="icon youtube"><a href="'.$value.'">Youtube</a></li>';
                        }
                      break;
                      case 'cont_flickr':
                        if($value!='Default Text'){
                          echo '<li class="icon flickr"><a href="'.$value.'">Flickr</a></li>';
                        }
                      break;
                      case 'cont_linkedin':
                        if($value!='Default Text'){
                          echo '<li class="icon linkedin"><a href="'.$value.'">Linkedin</a></li>';
                        }
                      break;
                      case 'cont_googleplus':
                        if($value!='Default Text'){
                          echo '<li class="icon googleplus"><a href="'.$value.'">Google +</a></li>';
                        }
                      break;
                    }
            } ?>

            <?php
            // Link para contato
            $moduloContato = false;
            if (is_page()) {
              // verificar se página tem o modulo de contato
              $group = get_post_meta( get_the_ID(), 'page_layout', true );
              for ($i=0; $i < count($group); $i++) {
                if($group[$i]['page_modulo'] == 'Contato'){
                  $moduloContato = true;
                }
              }
            }
            if($moduloContato){
              echo '<li class="icon contato"><a href="#contato">Contato</a></li>';
            }else{
              echo '<li class="icon contato"><a href="contato.php">Contato</a></li>';
            }
             ?>
          </ul>
          <?php
        }
         ?>
  </div>

</nav>
<!-- /NAV BAR ============================================================== -->
