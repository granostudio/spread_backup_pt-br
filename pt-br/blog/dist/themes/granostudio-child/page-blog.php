<?php
/**
* Page Template
*
* @package WordPress
* @subpackage Grano Studio
* @since Grano Studio 1.0
 */
get_header();?>
<div class="blog blog-home" >
	<span class='scroll-debug' scroll-position="scroll"></span>
	<!-- Banner -->

		<?php
		$args = array(
        'post_type' => 'post',
        // 'post__not_in' => array( get_the_ID() ),
        'posts_per_page' => 1,
        'cat'     => 7,
        // 'meta_query' => array(
        //             array(
        //             'key' => 'recommended_article',
        //             'value' => '1',
        //             'compare' => '=='
        //                 )
        //             )
    );
    $query = new WP_Query( $args );

		if ( $query->have_posts() ) {
			while ( $query->have_posts() ) {
				$query->the_post();
				?>
				<?php if ( has_post_thumbnail() ) {

					?>
					<div class="blog-banner" style="background-image:url(<?php the_post_thumbnail_url(); ?>)">
					<?php
				}else {
					?>
					<div class="blog-banner" style="background-image:url(http://via.placeholder.com/400x300)">
					<?php
				} ?>
				<div class="content">
			    <!-- <div class="cat"> -->
						<?php // the_category(', '); ?>
			    <!-- </div> -->
			    <h1><?php echo get_the_title(); ?></h1>
			    <a class="btn-lg btn-spread icon-seta" href="<?php echo get_permalink(); ?>">
			      Leia na íntegra
			      <span></span>
			    </a>
			  </div>
				<div class="mask">
		      <svg
		     version="1.1"
		     id="no-aspect-ratio"
		     xmlns="http://www.w3.org/2000/svg"
		     xmlns:xlink="http://www.w3.org/1999/xlink"
		     x="0px"
		     y="0px"
		     height="100%"
		     width="100%"
		     viewBox="0 0 100 100"
		     preserveAspectRatio="none">
		        <polygon fill="white" points="100,100 0,100 0,0 "/>
		      </svg>
		    </div>
		</div>
				<?php
			} // end while
		} // end if
		?>


	<div class="container">
		<div class="row">
			<div class="col-12">
				<!-- <div class="nav-cat">
					<div class="dropdown hidden-md-up">
					  <a class="btn dropdown-toggle" href="https://example.com" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					    Categorias
					  </a>

					  <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
							<?php /*wp_list_categories( array(
									'orderby'          => 'name',
									'hide_empty'       => 0,
									'exclude'          => array( 6, 1 ),
									'separator'        => ' ',
									'show_option_none' => ' ',
									'style'            => '',
									'title_li'         => ' '
									// 'include' => array( 3, 5, 9, 16 )
							) ); */?>
					  </div>
					</div>
					<div class="lista hidden-md-down">
						    <?php /* wp_list_categories( array(
						        'orderby'          => 'name',
										'hide_empty'       => 0,
										'exclude'          => array( 6, 1 ),
										'separator'        => ' ',
										'show_option_none' => ' ',
										'style'            => '',
										'title_li'         => ' '
						        // 'include' => array( 3, 5, 9, 16 )
						    ) ); */ ?>
					</div>
				</div> -->
				<!-- <div class="btn-lg btn-spread icon-lupa busca-principal">
					Buscar no blog
					<span></span>
				</div> -->
			</div>
		</div>
		<div class="row loop">
			<div class="col-12">
				<div class="row">
					<div class="col-12 col-md destaque ">
						<?php
						$args = array(
				        'post_type' => 'post',
				        'posts_per_page' => 1,
				        'cat'     => 8
				    );
				    $query = new WP_Query( $args );

						if ( $query->have_posts() ) {
							while ( $query->have_posts() ) {
								$query->the_post();
								?>
								<?php if ( has_post_thumbnail() ) {
									?>
									<div class="post height-2" style="background-image:url(<?php the_post_thumbnail_url(); ?>)">
									<?php
								}else {
									?>
									<div class="post height-2" style="background-image:url(http://via.placeholder.com/400x300)">
									<?php
								} ?>
								<a href="<?php echo get_permalink(); ?>" class="link-post"></a>
								<div class="content">
									<!-- <div class="cat"> -->
										<?php //the_category(', '); ?>
									<!-- </div> -->
									<h2>
										<?php echo get_the_title(); ?>
									</h2>
									<p><?php the_excerpt() ?></p>
									<div class="footer">
										<div class="redes-sociais">
											<a class="face icon icon-social-facebook">Facebook</a>
											<a class="twitter icon icon-social-twitter">Twitter</a>
											<a class="linkedin icon icon-social-linkedin">LinkedIn</a>
										</div>
										<a href="<?php echo get_permalink(); ?>"  class="btn-lg btn-spread icon-seta">
											saiba mais
											<span></span>
										</a  >
									</div>
								</div>
								<?php
							} // end while
						} // end if
						?>
						</div>
					</div>
					<div class="col destaque-2">
						<?php
						$args = array(
				        'post_type' => 'post',
				        'posts_per_page' => 2,
				        'cat'     => 9
				    );
				    $query = new WP_Query( $args );

						if ( $query->have_posts() ) {
							while ( $query->have_posts() ) {
								$query->the_post();
								?>

								<div class="post">
									<a href="<?php echo get_permalink(); ?>" class="link-post"></a>
									<div class="content">
										<!-- <div class="cat"> -->
											<?php // the_category(', '); ?>
										<!-- </div> -->
										<h2>
											<?php echo get_the_title(); ?>
										</h2>
										<p><?php the_excerpt() ?></p>
										<div class="footer">
											<div class="redes-sociais">
												<a class="face icon icon-social-facebook">Facebook</a>
												<a class="twitter icon icon-social-twitter">Twitter</a>
												<a class="linkedin icon icon-social-linkedin">LinkedIn</a>
											</div>
											<a href="<?php echo get_permalink(); ?>" class="btn-lg btn-spread icon-seta">
												saiba mais
												<span></span>
											</a>
										</div>
									</div>
								</div>
								<?php
							} // end while
						} // end if
						?>
					</div>
				</div>
			</div>
			<div class="col-12 loop-2">
				<div class="row">
					<?php
					//Protect against arbitrary paged values
					$paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
					$args = array(
							'post_type' => 'post',
							'posts_per_page' => 6,
							'paged' => $paged
					);
					$query = new WP_Query( $args );

					if ( $query->have_posts() ) {
						while ( $query->have_posts() ) {
							$query->the_post();
							?>
							<div class="col-12 col-md-6 col-lg-4">
								<?php if ( has_post_thumbnail() ) {
									?>
										<div class="post" style="background-image:url(<?php the_post_thumbnail_url(); ?>)">
									<?php
								}else {
									?>
										<div class="post" >
										<!-- <div class="post" style="background-image:url(http://via.placeholder.com/400x300)"> -->
									<?php
								} ?>
									<a href="<?php echo get_permalink(); ?>" class="link-post"></a>
									<div class="content">
										<!-- <div class="cat"> -->
											<?php // the_category(', '); ?>
										<!-- </div> -->
										<h2>
											<?php echo get_the_title(); ?>
										</h2>
										<p><?php the_excerpt() ?></p>
										<div class="footer">
											<div class="redes-sociais">
												<a class="face icon icon-social-facebook">Facebook</a>
												<a class="twitter icon icon-social-twitter">Twitter</a>
												<a class="linkedin icon icon-social-linkedin">LinkedIn</a>
											</div>
											<a href="<?php echo get_permalink(); ?>" class="btn-lg btn-spread icon-seta">
												saiba mais
												<span></span>
											</a>
										</div>
									</div>
								</div>
							</div>

							<?php
						} // end while
					} // end if
					?>
				</div>

			</div>
		</div>
	</div>
	<!-- <nav aria-label="..." class="paginacao">
		<ul class="pagination justify-content-center"> -->
			<!-- <li class="page-item">
	      <a class="page-link icon-seta" href="<?php // echo get_previous_posts_link('Previous', $query->max_num_pages); ?>" aria-label="Previous">
	        <span class="sr-only">Previous</span>
	      </a>
	    </li> -->

	<?php
		// $big = 999999999; // need an unlikely integer
    //
		// echo paginate_links( array(
		// 	'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
		// 	'format' => '?paged=%#%',
		// 	'current' => max( 1, get_query_var('paged') ),
		// 	'total' => $query->max_num_pages,
		// 	'prev_next'          => true,
		// 	'prev_text'          => __('<span class="sr-only">Previous</span>'),
		// 	'next_text'          => __('<span class="sr-only">Next</span>'),
		// 	'before_page_number' => '<li class="page-item">',
		// 	'after_page_number'  => '</li>'
		// ) );
		?>


	    <!-- <li class="page-item">
	      <a class="page-link icon-seta" href="#" aria-label="Previous">
	        <span class="sr-only">Previous</span>
	      </a>
	    </li>
	    1</a></li>
	    <li class="page-item"><a class="page-link" href="#">2</a></li>
	    <li class="page-item"><a class="page-link" href="#">3</a></li>
	    <li class="page-item">
	      <a class="page-link icon-seta" href="#" aria-label="Next">
	        <span class="sr-only">Next</span>
	      </a>
	    </li> -->
	  <!-- </ul>
	</nav> -->

</div>


<?php

// page

while ( have_posts() ) : the_post();



	// End of the loop.
endwhile;

 ?>

<?php get_footer(); ?>
