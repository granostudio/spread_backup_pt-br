<?php
/**
 * Grano Studio functions and definitions
 *
 *
 * @package WordPress
 * @subpackage Grano Studio
 * @since Grano Studio 1.0
 */


 class bootstrap_4_walker_nav_menu extends Walker_Nav_menu {

 	function start_lvl( &$output, $depth = 0, $args = array() ){ // ul
 		$indent = str_repeat("\t",$depth); // indents the outputted HTML
 		$submenu = ($depth > 0) ? ' sub-menu' : '';
 		$output .= "\n$indent<ul class=\"dropdown-menu$submenu depth_$depth\">\n";
 	}

 	function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ){ // li a span
 		$indent = ( $depth ) ? str_repeat("\t",$depth) : '';

 		$li_attributes = '';
 		$class_names = $value = '';

 		$classes = empty( $item->classes ) ? array() : (array) $item->classes;

 		$classes[] = ($args->walker->has_children) ? 'dropdown' : '';
 		$classes[] = ($item->current || $item->current_item_anchestor) ? 'active' : '';
 		$classes[] = 'nav-item';
 		$classes[] = 'nav-item-' . $item->ID;
 		if( $depth && $args->walker->has_children ){
 			$classes[] = 'dropdown-menu';
 		}

 		$class_names =  join(' ', apply_filters('nav_menu_css_class', array_filter( $classes ), $item, $args ) );
 		$class_names = ' class="' . esc_attr($class_names) . '"';

 		$id = apply_filters('nav_menu_item_id', 'menu-item-'.$item->ID, $item, $args);
 		$id = strlen( $id ) ? ' id="' . esc_attr( $id ) . '"' : '';

 		$output .= $indent . '<li ' . $id . $value . $class_names . $li_attributes . '>';

 		$attributes = ! empty( $item->attr_title ) ? ' title="' . esc_attr($item->attr_title) . '"' : '';
 		$attributes .= ! empty( $item->target ) ? ' target="' . esc_attr($item->target) . '"' : '';
 		$attributes .= ! empty( $item->xfn ) ? ' rel="' . esc_attr($item->xfn) . '"' : '';
 		$attributes .= ! empty( $item->url ) ? ' href="' . esc_attr($item->url) . '"' : '';

 		$attributes .= ( $args->walker->has_children ) ? ' class="nav-link dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"' : ' class="nav-link"';

 		$item_output = $args->before;
 		$item_output .= ( $depth > 0 ) ? '<a class="dropdown-item"' . $attributes . '>' : '<a' . $attributes . '>';
 		$item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
 		$item_output .= '</a>';
 		$item_output .= $args->after;

 		$output .= apply_filters ( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );

 	}

 }


/**
 * Enqueues scripts and styles.
 *
 */

function granostudio_scripts_child() {
	function tt_reading_time() {
	 $content = get_post_field('post_content');
	 $word_count = str_word_count(strip_tags($content));
	 $min = floor($word_count / 200); // tempo médio de leitura: 200 palavras
	 $tempo = 'Escrito por <a href=" '. get_author_posts_url(get_the_author_meta( 'ID' )) .' "> '.get_the_author(). '</a>  &nbsp&nbsp|&nbsp&nbsp  Tempo de leitura: ';
	 if ($min <= 1) {
	 $tempo .= '<span> 1 minuto </span> ';
	 }
	 else {
	 $tempo .= $min . ' minutos';
	 }
	 return $tempo;
	}
	function tt_reading_time_filter( $content ) {
	 $custom_content = '<div id="tt-temp-estim" class="small autor">'.tt_reading_time().'</div>';
	 $custom_content .= $content;
	 return $custom_content;
	}
	add_filter( 'the_content', 'tt_reading_time_filter' );

	// Return an alternate title, without prefix, for every type used in the get_the_archive_title().
	add_filter('get_the_archive_title', function ($title) {
	    if ( is_category() ) {
	        $title = single_cat_title( '', false );
	    }
			// elseif ( is_tag() ) {
	    //     $title = single_tag_title( '', false );
	    // } elseif ( is_author() ) {
	    //     $title = '<span class="vcard">' . get_the_author() . '</span>';
	    // } elseif ( is_year() ) {
	    //     $title = get_the_date( _x( 'Y', 'yearly archives date format' ) );
	    // } elseif ( is_month() ) {
	    //     $title = get_the_date( _x( 'F Y', 'monthly archives date format' ) );
	    // } elseif ( is_day() ) {
	    //     $title = get_the_date( _x( 'F j, Y', 'daily archives date format' ) );
	    // } elseif ( is_tax( 'post_format' ) ) {
	    //     if ( is_tax( 'post_format', 'post-format-aside' ) ) {
	    //         $title = _x( 'Asides', 'post format archive title' );
	    //     } elseif ( is_tax( 'post_format', 'post-format-gallery' ) ) {
	    //         $title = _x( 'Galleries', 'post format archive title' );
	    //     } elseif ( is_tax( 'post_format', 'post-format-image' ) ) {
	    //         $title = _x( 'Images', 'post format archive title' );
	    //     } elseif ( is_tax( 'post_format', 'post-format-video' ) ) {
	    //         $title = _x( 'Videos', 'post format archive title' );
	    //     } elseif ( is_tax( 'post_format', 'post-format-quote' ) ) {
	    //         $title = _x( 'Quotes', 'post format archive title' );
	    //     } elseif ( is_tax( 'post_format', 'post-format-link' ) ) {
	    //         $title = _x( 'Links', 'post format archive title' );
	    //     } elseif ( is_tax( 'post_format', 'post-format-status' ) ) {
	    //         $title = _x( 'Statuses', 'post format archive title' );
	    //     } elseif ( is_tax( 'post_format', 'post-format-audio' ) ) {
	    //         $title = _x( 'Audio', 'post format archive title' );
	    //     } elseif ( is_tax( 'post_format', 'post-format-chat' ) ) {
	    //         $title = _x( 'Chats', 'post format archive title' );
	    //     }
	    // } elseif ( is_post_type_archive() ) {
	    //     $title = post_type_archive_title( '', false );
	    // } elseif ( is_tax() ) {
	    //     $title = single_term_title( '', false );
	    // } else {
	    //     $title = __( 'Archives' );
	    // }
	    return $title;
	});
	//Desabilitar jquery
	wp_deregister_script( 'jquery' );

	// Theme stylesheet.
	wp_enqueue_style( 'granostudio-style', get_stylesheet()  );
	wp_enqueue_style( 'font-awesome', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css'  );
	// import fonts (Google Fonts)
	wp_enqueue_style('granostudio-style-fonts', get_stylesheet_directory_uri() . '/css/fonts/fonts.css', array(), 1.4);
	// Theme front-end stylesheet
	wp_enqueue_style('granostudio-style-front', get_stylesheet_directory_uri() . '/css/main.css', array(), 1.4 );


	// scripts js
	wp_enqueue_script('granostudio-jquery', get_stylesheet_directory_uri() . '/js/jquery-3.1.1.js', '000311', false);
	wp_enqueue_script('granostudio-angular', get_stylesheet_directory_uri() . '/js/angular.min.js', '000311', false);
	wp_enqueue_script('granostudio-scripts', get_stylesheet_directory_uri() . '/js/scripts.min.js', array(), 1.0, false, true);
	// helper angular
	wp_localize_script( 'granostudio-scripts', 'aeJS',
		array(
			'themeUrl' => get_stylesheet_directory_uri(),
		)
	);


}
add_action( 'wp_enqueue_scripts', 'granostudio_scripts_child' );
