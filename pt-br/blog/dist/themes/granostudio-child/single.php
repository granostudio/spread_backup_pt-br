<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage Grano Studio
 * @since Grano Studio 1.0
 */

get_header(); ?>

<!-- Page Content -->
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
    <div class="blog blog-home blog-single">
      <span class='scroll-debug' scroll-position="scroll"></span>

        <?php if ( has_post_thumbnail() ) {
          ?>
          <div class="blog-banner" style="background-image:url(<?php the_post_thumbnail_url(); ?>)">
          <?php
        }else {
          ?>
          <div class="blog-banner">
          <?php
        } ?>
        <div class="content">
			    <!-- <div class="cat"> -->
						<?php //the_category(', '); ?>
			    <!-- </div> -->
			    <h1><?php echo get_the_title(); ?></h1>

			  </div>

				<div class="mask">
		      <svg
		     version="1.1"
		     id="no-aspect-ratio"
		     xmlns="http://www.w3.org/2000/svg"
		     xmlns:xlink="http://www.w3.org/1999/xlink"
		     x="0px"
		     y="0px"
		     height="100%"
		     width="100%"
		     viewBox="0 0 100 100"
		     preserveAspectRatio="none">
		        <polygon fill="white" points="100,100 0,100 0,0 "/>
		      </svg>
		    </div>
      </div>

      <div class="container texto">

        <div class="row">
          <div class="col-12">
            <?php the_content(); ?>
            <div id="cabecalho_newsletter" class="vert_item"><a href="" class="btn btn-primary btn-newsletter">Receba nossa newsletter</a></div>
          </div>
        </div>
      </div>


<?php endwhile; // end of the loop. ?>

      <div class="container loop single-more-posts">
        <div class="row">
          <div class="col-12 titulo">
            <h1>blog</h1>
            <p>Fique por dentro de nossas novidades e tendências de mercado</p>
          </div>
        </div>
        <div class="row">
          <div class="col-12 loop-2">
    				<div class="row">
    					<?php
    					//Protect against arbitrary paged values
    					$paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
    					$args = array(
    							'post_type' => 'post',
    							'posts_per_page' => 3,
    							'paged' => $paged
    					);
    					$query = new WP_Query( $args );

    					if ( $query->have_posts() ) {
    						while ( $query->have_posts() ) {
    							$query->the_post();
    							?>
    							<div class="col-12 col-md-6 col-lg-4">
    								<?php if ( has_post_thumbnail() ) {
    									?>
    										<div class="post" style="background-image:url(<?php the_post_thumbnail_url(); ?>)">
    									<?php
    								}else {
    									?>
    										<div class="post" >
    										<!-- <div class="post" style="background-image:url(http://via.placeholder.com/400x300)"> -->
    									<?php
    								} ?>
    									<a href="<?php echo get_permalink(); ?>" class="link-post"></a>
    									<div class="content">
    										<!-- <div class="cat"> -->
    											<?php // the_category(', '); ?>
    										<!-- </div> -->
    										<h2>
    											<?php echo get_the_title(); ?>
    										</h2>
    										<p><?php the_excerpt() ?></p>
    										<div class="footer">
    											<div class="redes-sociais">
    												<a class="face icon icon-social-facebook">Facebook</a>
    												<a class="twitter icon icon-social-twitter">Twitter</a>
    												<a class="linkedin icon icon-social-linkedin">LinkedIn</a>
    											</div>
    											<a href="<?php echo get_permalink(); ?>" class="btn-lg btn-spread icon-seta">
    												saiba mais
    												<span></span>
    											</a>
    										</div>
    									</div>
    								</div>
    							</div>

    							<?php
    						} // end while
    					} // end if
    					?>
    				</div>

    			</div>
        </div>
          <div class="row">
            <div class="col-6 offset-3">
              <?php
              $url = home_url( '/', 'http' );
              ?>
              <a href="<?php echo esc_url( $url ); ?>" class="btn-lg btn-spread btn-spread-lg icon-seta btn-block btn-blog">
                Ir para blog
                <span></span>
              </a>
            </div>
          </div>


      </div>

      </div>

<?php get_footer(); ?>
