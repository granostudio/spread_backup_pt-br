<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after
 *
 * @package WordPress
 * @subpackage Grano Studio
 * @since Grano Studio 1.0
 */
?>
<?php wp_footer(); ?>




<?php
// livereload
$host = $_SERVER['HTTP_HOST'];
if ($host == "localhost:9001") {
  ?>
<script src="http://localhost:35729/livereload.js"></script>
  <?php
}

// /livereload
 ?>


<!-- FOOTER ============================================================== -->
<footer id="colophon" class="site-footer" role="contentinfo">
  <a id="back-to-top" href="#" class="btn btn-primary btn-lg back-to-top" role="button" title="Click to return on the top page" data-toggle="tooltip" data-placement="left"><span class="icon-seta"></span></a>

		<div class="container">

			<div id="rodape_linha1">
				<div class="row">

					<div class="col-xs-12 col-md-3">
						<div id="rodape_logo">
							<a href="https://spread.com.br/pt-br/site"><img class="rodape_logo_img" src="https://spread.com.br/pt-br/site/wp-content/uploads/2017/12/rodape_logo.png" width="156" height="96" alt="Spread" /></a>
						</div><!-- #rodape_logo -->
					</div><!-- .col -->

					<div class="col-xs-12 col-md-2">
						<div id="rodape_col1">
							<div class="menu-rodape-1-container"><ul id="menu-rodape-1" class="menu"><li id="menu-item-336" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-2 current_page_item menu-item-336"><a href="https://www.spread.com.br/pt-br/site/">Home</a></li>
<li id="menu-item-340" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-340"><a href="https://www.spread.com.br/pt-br/site/solucoes/digital-transformation/">Digital Transformation</a></li>
<li id="menu-item-339" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-339"><a href="https://spread.com.br/pt-br/site/solucoes/digital-studio/">Digital Studio</a></li>
<li id="menu-item-337" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-337"><a href="https://spread.com.br/pt-br/site/solucoes/sap-studio/">SAP Studio</a></li>
<li id="menu-item-338" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-338"><a href="https://spread.com.br/pt-br/site/core-studio/">Core Studio</a></li>
</ul></div>						</div><!-- #rodape_col1 -->
					</div><!-- .col -->

					<div class="col-xs-12 col-md-2">
						<div id="rodape_col2">
							<div class="menu-rodape-2-container"><ul id="menu-rodape-2" class="menu"><li id="menu-item-344" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-344"><a href="https://spread.com.br/pt-br/site/solucoes/devops-quality/">DevOps &#038; Quality</a></li>
<li id="menu-item-343" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-343"><a href="https://spread.com.br/pt-br/site/solucoes/managed-services/">Managed Services</a></li>
<li id="menu-item-345" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-345"><a href="https://spread.com.br/pt-br/site/solucoes/business-outsourcing/">Business Outsourcing</a></li>
<li id="menu-item-342" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-342"><a target="_blank" href="http://genovahub.com.br#new_tab">Genova Hub</a></li>
<li id="menu-item-346" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-346"><a href="https://spread.com.br/pt-br/site/quem-somos/">Quem Somos</a></li>
</ul></div>						</div><!-- #rodape_col2 -->
					</div><!-- .col -->

					<div class="col-xs-12 col-md-2">
						<div id="rodape_col3">
							<div class="menu-rodape-3-container"><ul id="menu-rodape-3" class="menu"><li id="menu-item-350" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-350"><a href="https://spread.com.br/pt-br/blog/">Blog</a></li>
<li id="menu-item-349" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-349"><a href="https://spread.com.br/pt-br/site/agenda-de-eventos/">Agenda</a></li>
<li id="menu-item-348" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-348"><a href="https://spread.com.br/pt-br/site/trabalhe-conosco/">Vagas</a></li>
<li id="menu-item-347" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-347"><a href="https://spread.com.br/pt-br/site/contato/">Contato</a></li>
</ul></div>	
<ul class="menu">
								<li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="" class="btn-newsletter" style="color: #FEA32E!important;text-decoration: underline;">Newsletter</a></li>
							</ul>
					</div><!-- #rodape_col3 -->
					</div><!-- .col -->

					<div class="col-xs-12 col-md-3">
						<div id="rodape_contato">

							<p class="rodape_telefone">+55 11 3874 6000</p>
							<p class="rodape_email"><a id='ofd166d0' href='#' class='ofuscado'></a><script>
jQuery( function($) { $('#ofd166d0').attr( 'href', 'mailto:\u0073\u0070\u0072\u0065\u0061\u0064\u002e\u0074\u0065\u0063\u006e\u006f\u006c\u006f\u0067\u0069\u0061\u0040\u0073\u0070\u0072\u0065\u0061\u0064\u002e\u0063\u006f\u006d\u002e\u0062\u0072' ).html( '&#x73;&#x70;&#x72;&#x65;&#x61;&#x64;&#x2e;&#x74;&#x65;&#x63;&#x6e;&#x6f;&#x6c;&#x6f;&#x67;&#x69;&#x61;&#x40;&#x73;&#x70;&#x72;&#x65;&#x61;&#x64;&#x2e;&#x63;&#x6f;&#x6d;&#x2e;&#x62;&#x72;' ); } );
</script></p>
							<address class="rodape_endereco">Matriz:<br />
<br />
Rua Verbo Divino, 1661 - Cj. 61 - 6º andar<br />
Chácara Santo Antônio<br />
<br />
CEP: 04719-002 - São Paulo - SP</address>

						</div><!-- #rodape_contato -->
					</div><!-- .col -->

				</div><!-- .row -->
			</div><!-- #rodape_linha1 -->

			<div id="rodape_linha2" class="vert_ancestral">
				<div class="row">

					<div class="col-xs-12 col-md-8">
						<p id="rodape_copyright" class="vert_item">© 2018 Spread Tecnologia | Desenvolvido por Grano Studio</p>
					</div><!-- .col -->

					<div class="col-xs-12 col-md-4" style="text-align: right;">
						<nav id="rodape_redes" class="vert_item">
							<div class="menu-redes-sociais-container"><ul id="menu-redes-sociais-2" class="menu_social"><li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-351"><a target="_blank" href="https://www.facebook.com/spreadtecnologia">Facebook</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-352"><a target="_blank" href="https://twitter.com/spreadoficial">Twitter</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-353"><a target="_blank" href="https://www.linkedin.com/company/spread">LinkedIn</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-354"><a target="_blank" href="https://www.instagram.com/spreadtecnologia/">Instagram</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1115"><a href="https://www.youtube.com/channel/UCD1k9aCsFVY7vCz-6T_9UFw">YouTube</a></li>
</ul></div>						</nav><!-- #rodape_redes -->
					</div><!-- .col -->

				</div><!-- .row -->
			</div><!-- #rodape_linha2 -->

		</div><!-- .container -->

</footer>
<!-- /FOOTER ============================================================== -->
</div><!-- /.container -->

<!-- Google Analytics -->
<?php $seoanalytics = grano_get_options('seo','seo_analytics');

if(!empty($seoanalytics) || $seoanalytics != "Default Text"){
  echo "<script>";
  echo $seoanalytics;
  echo "</script>";
}

 ?>
<!-- / Google Analytics -->
</div> <!-- / app angular -->
</body>

<!-- Preloader -->
<script type="text/javascript">
    // //<![CDATA[
    //     $(window).on('load', function() { // makes sure the whole site is loaded
    //         $('#status').fadeOut(); // will first fade out the loading animation
    //         $('#preloader').delay(350).fadeOut('slow'); // will fade out the white DIV that covers the website.
    //         $('body').delay(350).css({'overflow':'visible'});
    //     })
    // //]]>
</script>

</html>
