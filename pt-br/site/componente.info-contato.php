<?php
// @requer classe Elemento, formatarEmail
?>

<div class="row">
	<div class="col-xs-12 col-md-6">
		<div class="info_col1">
			<?=
			E::h2( $param_intro_titulo, 'info_titulo intro_titulo' ) .
			E::p( $param_intro_conteudo, 'intro_conteudo' ) .
			E::h2( $param_telefone_titulo, 'info_titulo telefone_titulo' ) .
			E::p( $param_telefone_numero, 'telefone_numero' ) .
			E::h2( $param_email_titulo, 'info_titulo email_titulo' ) .
			E::p( formatarEmail( $param_email_endereco ), 'email_endereco' )
			?>
		</div><!-- .info_col1 -->
	</div><!-- .col -->
	<div class="col-xs-12 col-md-6">
		<div class="info_col2">
			<?=
			do_shortcode( str_replace( '&quot;', '"', $param_form ) )
			?>
		</div><!-- .info_col2 -->
	</div><!-- .col -->
</div><!-- .row -->
