<?php

ob_start();

print '<div class="container">';

// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// INTRO

extract( $param_intro[0], EXTR_PREFIX_ALL, 'intro' );

print '<div class="modal_intro">'
	. E::div( obterImg( $intro_icone, 'full', 'intro_icone_img', $intro_titulo ), 'intro_icone' )
	. E::h1( $intro_titulo, 'intro_titulo' )
	. E::p( $intro_texto, 'intro_texto' )
	. '</div>'
;

// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// ACORDIÃO

$acordParams = array(
	'class'			=> 'modal_acordiao',
	'secoes'		=> array(),
);

foreach ( $param_acordiao as $secao ) :
	
	extract( $secao, EXTR_PREFIX_ALL, 'secao' );
	ob_start();
	
	// HTML
	
	print E::p( $secao_texto, 'secao_texto' );
	
	?>
	<div class="secao_itens colunas_<?= $secao_colunas ?>">
		<div class="row">
			<?php
			$n = 0;
			$classeCols = 'col-xs-12 col-sm-' . col( $secao_colunas );
			foreach ( $secao_itens as $item ) :
				$n++;
				extract( $item, EXTR_PREFIX_ALL, 'item' );
				?>
				<div class="<?= $classeCols ?>">
					<section class="secao_item layout_<?= $secao_colunas ?>">
						<?php
						switch ( $secao_colunas ) :
							case 1 :
								?>
								<div class="row">
									<div class="col-xs-4">
										<div class="item_cabecalho">
											<?=
											E::div( obterImg( $item_icone, 'full', 'item_icone_img', $intro_titulo ), 'item_icone' ) .
											E::h1( quebraEstetica( $item_nome ), 'item_nome' )
											?>
										</div><!-- .item_cabecalho -->
									</div><!-- .col -->
									<div class="col-xs-8">
										<?=
										E::p( $item_texto, 'item_texto' )
										?>
									</div><!-- .col -->
								</div><!-- .row -->
								<?php
							break;
							case 2 :
								print
									'<div class="item_cabecalho">' .
										E::div( obterImg( $item_icone, 'full', 'item_icone_img', $intro_titulo ), 'item_icone' ) .
										E::h1( quebraEstetica( $item_nome ), 'item_nome' ) .
									'</div><!-- .item_cabecalho -->' .
									E::p( $item_texto, 'item_texto' )
								;
							break;
						endswitch;
						?>
					</section><!-- .secao_item -->
				</div><!-- .col -->
				<?php
				row( $n, $classeCols );
			endforeach;
			?>
		</div><!-- .row -->
	</div><!-- .secao_itens -->
	<?php
	
	// Salva a seção
	
	$secao_conteudo = ob_get_contents();
	ob_end_clean();
	
	$acordParams['secoes'][] = array(
		'class'			=> 'modal_acordiao_secao',
		'titulo'		=> $secao_titulo,
		'conteudo'		=> $secao_conteudo,
	);
	
endforeach;

componente( 'acordiao', $acordParams );

// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// MONTAGEM DO MODAL

print '</div><!-- .container -->';

$corpo = ob_get_contents();
ob_end_clean();

$modalParams = array(
	'id'		=> $param_modal_id,
	'class'		=> 'modal_servicos',
	'corpo'		=> $corpo,
);

Componente::modal( $modalParams );

?>
<script>
	+function() {
		var $modal = jQuery('#<?= $param_modal_id ?>');
		$modal.find('.collapse')
			.on( 'shown.bs.collapse', function() {
				jQuery.smoothScroll({
					scrollElement: $modal,
					scrollTarget: this
				});
			} );
	}();
</script>