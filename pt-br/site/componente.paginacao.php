<?php
// Insere a paginação em um arquivo de posts
// @requer fa

global $wp_query;

if ( !$this->total_itens ) :
	// Sem itens - se for o caso, o componente de listagem irá inserir uma mensagem informando que não há posts
	return;
elseif ( $this->_totalPaginas <= 1 && $param_msg_fim ) :
	// Sem navegação
	elemento( 'p', 'Fim da listagem' . '.', cl('sem_paginacao') );
	return;
endif;

?>
<nav <?= $this->attr( $this->_attrWrapper ) ?>>
	<ol class="paginacao_lista">
		<?php if ( $this->_temAnterior ) : ?>
			<li class="paginacao_item texto anterior"><a href="<?= $this->linkPagina( $param_pagina - 1 ) ?>" class="paginacao_link"><?= fa('arrow-circle-left') ?>&ensp;Página anterior</a></li>
		<?php endif; ?>
		<?php for ( $p = 1; $p <= $this->_totalPaginas; $p++ ) : ?>
			<li class="paginacao_item numero <?php if ( $p == $param_pagina ) print 'atual' ?>"><a href="<?= $this->linkPagina( $p ) ?>" class="paginacao_link"><?= $p ?></a></li>
		<?php endfor; ?>
		<?php if ( $this->_temProxima ) : ?>
			<li class="paginacao_item texto proxima"><a href="<?= $this->linkPagina( $param_pagina + 1 ) ?>" class="paginacao_link">Página seguinte&ensp;<?= fa('arrow-circle-right') ?></a></li>
		<?php endif; ?>
	</ol><!-- .paginacao_lista -->
</nav><!-- .paginacao -->