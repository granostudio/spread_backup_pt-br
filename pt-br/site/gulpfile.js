'use strict';

var gulp        = require('gulp'),
    argv        = require('yargs').argv,
    connect     = require('gulp-connect-php'),
    browserSync = require('browser-sync'),
    sass        = require('gulp-sass'),
    uglify      = require('gulp-uglify'),
    concat      = require('gulp-concat'),
    rename      = require('gulp-rename'),
    watch       = require('gulp-watch'),
    runSequence = require('run-sequence'),
    GulpSSH     = require('gulp-ssh'),
    fs          = require('fs'),
    gutil       = require('gulp-util'),
    shell       = require('gulp-shell'),
    prompt      = require('gulp-prompt'),
    json        = require('./.yo-rc.json')['generator-granoexpresso-2']['promptValues'];

//script paths
var jsFiles     = [ 'src/**/**.js'],
    jsDest      = './dist/theme/granostudio-child/js',
    cssFiles    = './dist/theme/granostudio-child/css',
    sassFiles   = './src/theme/sass',
    themeFiles  = './dist/theme/granostudio-child',
    themeOrFiles= './src/theme',
    WpPath      = './app/**';





// criar amnbiente local
gulp.task('connect', function() {
    connect.server({  hostname: 'localhost', port: 8888, keepalive: true}, function(){
      browserSync({
          proxy: 'localhost:8888',
          port: '8888'
      });
    });
});


//compass

gulp.task('sass', function () {
    gulp.src('src/theme/sass/**/**.scss')
      .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
      .pipe(gulp.dest('dist/themes/granostudio-child/css'))

    // gulp.src(['src/plugins/grano-landingpage/sass/**.scss','src/plugins/grano-landingpage/sass/**/**.scss' ])
    //   .pipe(sass().on('error', sass.logError))
    //   .pipe(gulp.dest('dist/plugins/grano-landingpage/css'));
});

// copia o a pasta tema para o granostudio-child
gulp.task('copytheme', function() {
  gulp.src( ['src/theme/**.php', 'src/theme/**/**.php'])
   .pipe(gulp.dest('dist/themes/granostudio-child'));
  // gulp.src( ['src/plugins/grano-landingpage/**.php', 'src/plugins/grano-landingpage/**/**.php'])
  //  .pipe(gulp.dest('dist/plugins/grano-landingpage'));
   gulp.src( ['src/theme/img/**'])
    .pipe(gulp.dest('dist/themes/granostudio-child/img'));
});

// compact os js
gulp.task('scripts', function() {
    gulp.src(['src/theme/js/bootstrap.min.js','src/theme/js/scripts.js'])
        .pipe(concat('scripts.min.js'))
        .pipe(uglify({mangle: false}))
        .on('error', function (err) { gutil.log(gutil.colors.red('[Error]'), err.toString()); })
        .pipe(gulp.dest('dist/themes/granostudio-child/js'));

    // gulp.src(['src/plugin/grano-landingpage/js/**.js'])
    //     .pipe(concat('scripts.min.js'))
    //     .pipe(uglify({mangle: false}))
    //     .on('error', function (err) { gutil.log(gutil.colors.red('[Error]'), err.toString()); })
    //     .pipe(gulp.dest('dist/plugin/grano-landingpage/js'));

});

// watch para alterações
gulp.task('watch', function(){
  gulp.watch(['src/**/**.scss', 'src/**.scss', 'src/**/**.php', 'src/**/**.js'], [])
  .on('change', function(){
    browserSync.reload();
  });
});


// tasks grano
// grano-local para desv local
gulp.task('grano-local',  ['connect'] );
