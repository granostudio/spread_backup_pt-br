<?php
/*
 * @param (string) id
 * @param (string) style
 * @param (string) class
 * @param (bool) fade
 * @param (string) externo
 * @param (string) data-ride
 * @param (string) data-pause
 * @param (string) data-interval
 * @param (bool) indicadores = true
 * @param (bool) setas = true
 * @param (bool) progresso = false
 * @param (array_n) slides
 * 		@param (string) conteudo
 * 		@param (string) class
 * 		@param (string) style
 * @param (array_n) thumbs = null
 * @param (bool) forcar-indicadores = false
 *
 * @requer attr, obter, idUnico
 */
 
isset( $params ) OR die();
 
$sliderAttr = array(
	'id'			=> 'slider' . idUnico('slider'),
	'style'			=> null,
	'data-ride'		=> obter( 'progresso', $params ) ? null : 'carousel',
	'data-pause'	=> 'true',
	'data-interval'	=> '7000',
);

$itemAttr = array(
	'style'			=> null,
);

?>
<div <?= attr( $sliderAttr, $params ) ?> class="carousel slide <?= obter( 'class', $params ) . ' ' . ( obter( 'fade', $params ) ? 'carousel-fade' : '' ) ?>">

	<?= obter( 'externo', $params ) ?>
	
	<div class="carousel-inner">
		<?php
		$total_slides = count( $params['slides'] );
		for ( $i = 0, $c = 1; $i < $total_slides; $i++, $c++ ) :
			$slide =& $params['slides'][$i];
			$item_class = 'item' . $c . ' ';
			if ( $i == 0 )
				$item_class .= 'active ';
			?>
			<section <?= attr( $itemAttr, $slide ) ?> class="item slide_item <?= $item_class; ?><?= $slide['class'] ?>">
				<?= $slide['conteudo'] ?>
			</section><!-- .slide_item -->
			<?php
		endfor;
		?>
	</div><!-- .carousel-inner -->
	
	<?php if ( obter( 'indicadores', $params, true ) && ( $total_slides > 1 || obter( 'forcar-indicadores', $params ) ) ) : ?>
		<ol class="carousel-indicators" data-animate-in="fadeInUp" data-animate-out="fadeOutDown">
			<?php
			$thumbs = obter( 'thumbs', $params, array() );
			for ( $i = 0; $i < $total_slides; $i++ ) {
				?>
				<li class="indicator-item <?php print ( isset( $thumbs[$i] ) ? 'thumb' : 'bullet_c' ); if ( $i == 0 ) print ' active' ?>" data-target="#<?= obter( 'id', $params, $sliderAttr['id'] ) ?>" data-slide-to="<?= $i; ?>"><?php if ( isset( $thumbs[$i] ) ) print $thumbs[$i] ?></li>
				<?php
			}
			?>
		</ol><!-- .carousel-indicators -->
	<?php endif; ?>
	
	<?php if ( obter( 'setas', $params, true ) && $total_slides > 1 ) : ?>
		<a class="left carousel-control" href="#<?= obter( 'id', $params, $sliderAttr['id'] ) ?>" role="button" data-slide="prev" title="Anterior">&lt;</a>
		<a class="right carousel-control" href="#<?= obter( 'id', $params, $sliderAttr['id'] ) ?>" role="button" data-slide="next" title="Próximo">&gt;</a>
	<?php endif; ?>
	
	<?php if ( obter( 'progresso', $params, false ) && $total_slides > 1 ) : ?>
		<hr class="carousel-progress" />
	<?php endif; ?>
	
</div><!-- .carousel -->