<?php
// @requer animAttr, col, row, subElemento, hotspot, obterThumbFundo, cl, elemento, imgTema, linksCompartilhamento
if ( $param_posts ) :
	?>
	
	<div <?= $this->attr( $this->_attrWrapper ) ?>>
		<div class="row equalizar_lista">
			<?php
			$n = 0;
			$classeCols = 'col-xs-12 col-sm-' . col( $param_colunas );
			foreach ( $param_posts as $p ) :
				$n++;
				// $post_link = get_the_permalink( $p->ID );
				$post_link = $p->permalink;
				?>
				<div class="<?= $classeCols ?>">
					<article class="post_item" style="background-image: url(&#34;<?= esc_attr( $p->thumb ? $p->thumb : src( THUMB_PADRAO ) ) ?>&#34;);"><?php // thumbFundo( $p->ID, 'large' ) ?>
						<div class="post_conteudo equalizar_item">
							<p class="post_meta">
								<?php
								// the_category( ', ', 'single', $p->ID );
								// print linksCategorias();
								$linksCats = array();
								foreach ( $p->categories as $cat )
									$linksCats[] = E::a( $cat['link'], $cat['name'], true, 'cat_link' );
								print implode( ', ', $linksCats );
								?>
							</p><!-- .post_meta -->
							<?php
							elemento( 'h2', ancora( $post_link, $p->post_title, true, cl('post_titulo_link'), false ), cl('post_titulo') );
							elemento( 'p', gerarResumo( 150, $p ), cl('post_resumo') );
							?>
							<ul class="post_compartilhar">
								<?php
								linksCompartilhamento( array(
									'url'		=> $post_link,
									'titulo'	=> $p->post_title,
									'resumo'	=> gerarResumo( 60, $p ),
								) );
								?>
							</ul><!-- .post_compartilhar -->
							<?php
							ancora( $post_link, imgTema( 'mais_lilas.png', pll__('Saiba Mais'), 'post_mais_img' ), true, 'post_mais' );
							?>
						</div><!-- .post_conteudo -->
					</article><!-- .post_item -->
				</div><!-- .col -->
				<?php
				row( $n, $classeCols );
			endforeach;
			?>
		</div><!-- .row -->
	</div><!-- .posts_blog -->
	
<?php elseif ( $param_msg_sem_posts ) : ?>

	<p class="sem_posts"><?= is_search() ? pll__('Não há posts que correspondem com sua busca.') : pll__('Não há posts nesta seção.') ?></p>

<?php endif; ?>