<?php

class Componente_Info_Contato extends Componente_Abstrato {
	
	public function __construct( $novosParams = null ) {
		// Configurações do componente
		$this->_nome = 'Info de Contato';
		$this->_slug = 'info_contato';
		$this->_aceitarParamsNovos = false;
		$this->_padroes = array(
			'intro_titulo'		=> null,
			'intro_conteudo'	=> null,
			'telefone_titulo'	=> null,
			'telefone_numero'	=> null,
			'email_titulo'		=> null,
			'email_endereco'	=> null,
			'form'				=> null,
		);
		$this->_sanitizadores = array(
		);
		$this->_obrigatorios = array(
		);
		$this->_validadores = array(
			'intro_titulo'		=> 'is_string',
			'intro_conteudo'	=> 'is_string',
			'telefone_titulo'	=> 'is_string',
			'telefone_numero'	=> 'is_string',
			'email_titulo'		=> 'is_string',
			'email_endereco'	=> 'is_string',
			'form'				=> 'is_string',
		);	
		$this->_regrasJuncao = array(
		);
		// Inicialização
		parent::__construct( $novosParams );
	}
	
	public function renderizar( $_imprimir = true ) {
		
		// @requer obter
		
		// Carrega os campos do ACF da página de Contato
		if ( !$this->intro_titulo )
			$this->configurar( obter( 0, get_field( 'info', PAGINA_CONTATO ) ) );
		if ( !$this->form )
			$this->configurar( array( 'form' => get_field( 'form', PAGINA_CONTATO ) ) );
				
		return parent::renderizar( $_imprimir );
		
	}
		
}