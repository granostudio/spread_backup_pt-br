<?php

/**

 * Template Name: Homepage

 */

global $post;



add_action( 'wp_enqueue_scripts', 'home_enqueue' );



get_header();



extract( $customFields = get_fields() );

$sticky_posts = get_option('sticky_posts');



?>

<article id="home">

	<?php







	// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	// INTRO

	extract( $intro[0], EXTR_PREFIX_ALL, 'intro' );

	$frases = array_column( $intro_titulo2, 'frase' );

	?>

	<section id="intro" class="secao video">

	<video loop autoplay  muted controls="false"  width="auto" class="hiddex-sm" src="<?php echo get_stylesheet_directory_uri();?>/imagens/Spread-home-final.mp4" type="video/webm">
      <!-- <source src="<?php echo get_stylesheet_directory_uri();?>/imagens/Spread-home-final.webm" type="video/webm"> -->
    </video>

		<div id="intro_wrapper" class="secao_wrapper">

			<div class="container">

				<div class="row">



					<div class="col-xs-12">



						<h1 id="intro_titulo" class="secao_titulo">

							<?=

							el( 'span', $intro_titulo1, 'linha1' ) .

							el( 'span', implode( ', ', $frases ), 'sr-only linha2' )

							?>

						</h1><!-- #intro_titulo -->



						<?php



						Componente::renderizar( 'owl', array(

							'id'		=> 'intro_frases',

							'opcoes'	=> array(

								'items'					=> 1,

								'autoplayHoverPause'	=> false,

								'dots'					=> false,

								'nav'					=> false,

								'autoplayTimeout'		=> 4000,

								'smartSpeed'			=> 400,

							),

							'slides'	=> $frases,

						) );



						?>

					</div><!-- .col -->

					<div class="clearfix"></div>

					<div class="col-xs-12 col-sm-8 col-sm-offset-2">

						<?php



						print el( 'p', $intro_texto, 'intro_texto' );

						print el( 'p', '#' . $intro_hashtag, 'intro_hashtag' );



						?>

					</div><!-- .col -->



				</div><!-- .row -->



				<?php

				ancora(

					'#solucoes',

					el( 'span',

						imgTema( 'seta_amarela.png', '&darr;', 'home_scroll_img' ),

					'home_scroll amarelo' ) .

					el( 'span', 'Saiba mais', 'intro_mais_rotulo' ),

					false,

					'intro_mais'

				);

				?>



			</div><!-- .container -->

		</div><!-- #intro_wrapper -->

	</section><!-- #intro -->

	<?php







	// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	// SOLUÇÕES

	extract( $solucoes[0], EXTR_PREFIX_ALL, 'solucoes' );



	?>

	<section id="solucoes" class="secao sesao-blog">

		<div id="solucoes_wrapper" class="secao_wrapper">

			<div class="container">

				<div class="row">

					<div class="col-xs-12 col-sm-8 col-sm-offset-2">



						<h1 id="solucoes_titulo" class="secao_titulo">

							<?=

							el( 'span', $solucoes_titulo1, 'linha1' ) .

							el( 'span', $solucoes_titulo2, 'linha2' ) .

							el( 'span', $solucoes_titulo3, 'linha3' )

							?>

						</h1><!-- #intro_titulo -->



						<div id="solucoes_itens">

							<div class="row">

								<?php

								$classeCols = 'col-xs-12 col-sm-4 col-md-3';

								$n = 0;

								foreach ( $solucoes_itens as $item ) :

									$n++;

									?>

									<div class="<?= $classeCols ?>">

										<article class="solucoes_item">
											<?php $output = str_replace("/n", '<br />', el( 'h2', $item['titulo'], 'item_titulo' )); ?>
											<?=

											el(

												'div',

												obterImg( $item['icone'], 'medium', array( 'alt' => $item['titulo'], 'class' => 'item_icone_img' ) ),

												'item_icone'

											) .

											 $output .

											el( 'p', $item['texto'], 'item_texto' ) .

											ancoraNaoVazia( $item['link'], 'saiba mais', AUTO, 'item_link', false ) .

											hotspot( $item['link'], AUTO )

											?>

										</article><!-- .solucoes_item -->

									</div><!-- .col -->

									<?php

									row( $n, $classeCols );

								endforeach;

								?>

							</div><!-- .row -->

						</div><!-- #solucoes_itens -->



					</div><!-- .col -->

				</div><!-- .row -->



<!-- 				<?php

				ancora(

					'#home_blog',

					imgTema( 'seta_lilas.png', '&darr;', 'home_scroll_img' ),

					false,

					'home_scroll lilas'

				);

				?> -->



			</div><!-- .container -->

		</div><!-- #solucoes_wrapper -->

	</section><!-- #solucoes -->

	<?php







	// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	// BLOG



	// extract( $blog[0], EXTR_PREFIX_ALL, 'blog' );



	?>

	<section id="home_blog" class="secao">

		<div id="home_blog_wrapper" class="secao_wrapper">

			<div class="container">

				<?php


				$sliderParams =& $blog[0];
				$sliderParams['sticky_posts'] =& $sticky_posts;

				// $sliderParams['id'] = 'home_blog_slider';

				Componente::sliderBlog( $sliderParams );

				// ancora(

				// 	'#projetos',
				// 	imgTema( 'seta_lilas.png', '&darr;', 'home_scroll_img' ),
				// 	false,
				// 	'home_scroll lilas'

				// );


				?>

			</div><!-- .container -->

		</div><!-- #home_blog_wrapper -->

	</section><!-- #home_blog -->

	<?php







	// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	// PROJETOS



	extract( $projetos[0], EXTR_PREFIX_ALL, 'projetos' );



	?>

	<section id="projetos" class="secao">

		<div id="projetos_wrapper" class="secao_wrapper">

			<div class="container">

				<?php



				print E::h1( null, 'secao_titulo' )

					->span( $projetos_titulo1, 'linha1' )

					->span( $projetos_titulo2, 'linha2' )

					->span( $projetos_titulo3, 'linha3' );



				print E::p( $projetos_texto, 'secao_intro' );



				// ancora(

				// 	'#estatisticas',

				// 	imgTema( 'seta_amarela.png', '&darr;', 'home_scroll_img' ),

				// 	false,

				// 	'home_scroll amarelo'

				// );



				?>

			</div><!-- .container -->

		</div><!-- #projetos_wrapper -->

	</section><!-- #projetos -->

	<?php







	// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	// ESTATÍSTICAS



	extract( $estatisticas[0], EXTR_PREFIX_ALL, 'estatisticas' );

	extract( $clientes[0], EXTR_PREFIX_ALL, 'clientes' );



	?>

	<section id="estatisticas" <?php fundo( $estatisticas_fundo, 'full' ) ?> class="secao">

		<div id="estatisticas_wrapper" class="secao_wrapper">

			<div class="container">

				<?php



				// -----------------------------------------------------------------------

				// INTRO



				print E::h1( null, 'secao_titulo' )

					->span( $estatisticas_titulo1, 'linha1' )

					->span( $estatisticas_titulo2, 'linha2' );



				// -----------------------------------------------------------------------

				// NÚMEROS



				?>

				<ul id="estatisticas_numeros">

					<?php

					foreach ( $estatisticas_metricas as $m ) :

						?>

						<li class="numeros_item">

							<?=

							E::span( $m['valor'] . '+', 'item_valor' ) .

							E::span( $m['rotulo'], 'item_rotulo' )

							?>

						</li><!-- .numeros_item -->

						<?php

					endforeach;

					?>

				</ul><!-- #estatisticas_numeros -->

				<?php



				// -----------------------------------------------------------------------

				// CLIENTES



				print E::h2( $clientes_titulo, 'clientes_titulo' );



				$owlParams = array(

					'id'			=> 'clientes_slider',

					'slides'		=> array(),

					'opcoes'		=> array(

						'loop'				=> true,

						'nav'				=> true,

						'dots'				=> false,

						'autoplay'			=> true,

						'autoplayTimeout'	=> 3000,

						'autoplayHoverPause'=> true,

						'margin'			=> 25,

						'responsive'	=> array(

							0	=> array(

								'items'		=> 2,

							),

							768 => array(

								'items'		=> 4,

							),

							994 => array(

								'items'		=> 5,

							),

							1250 => array(

								'items'		=> 6,

							),

						),

					),

				);





				foreach ( $clientes_logotipos as $logo )

					$owlParams['slides'][] = el( 'div', obterImg( $logo['imagem'], 'medium', cl('logo_img') ), 'logo' );





				// EXEMPLO

				// $logoExemplo = el( 'div', obterImg( THUMB_PADRAO, 'medium', cl('logo_img') ), 'logo' );
        //
				// $owlParams['slides'] = array_fill( 0, 7, $logoExemplo );



				Componente::owl( $owlParams );



				// -----------------------------------------------------------------------

				// BOTÕES



				ancora( $estatisticas_link_url, $estatisticas_link_rotulo . imgTema( 'mais_branco.png', '&rarr;', 'estatisticas_botao_img' ), 0, 'btn estatisticas_botao' );



				// ancora(

				// 	'#inovacao',

				// 	imgTema( 'seta_turquesa.png', '&darr;', 'home_scroll_img' ),

				// 	false,

				// 	'home_scroll turquesa'

				// );



				?>

			</div><!-- .container -->

		</div><!-- #estatisticas_wrapper -->

	</section><!-- #estatisticas -->

	<?php







	// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	// INOVAÇÃO



	extract( $inovacao[0], EXTR_PREFIX_ALL, 'inovacao' );



	?>

	<section id="inovacao" class="secao">

		<div id="inovacao_wrapper" class="secao_wrapper">

			<div class="container">

				<div class="row estreito">

					<div class="col-xs-12 col-sm-5">

						<?php

						img( $inovacao_grafico, 'full', array( 'alt' => $inovacao_titulo1, 'class' => 'inovacao_grafico_img' ) );

						?>

					</div><!-- .col -->

					<div class="col-xs-12 col-sm-7">

						<div id="inovacao_conteudo">

							<?php



							// -----------------------------------------------------------------------

							// CONTEÚDO



							print E::h1( $inovacao_titulo1, 'secao_titulo' )

								. E::span( $inovacao_titulo2, 'inovacao_titulo_linha2' )

								. E::p( $inovacao_texto, 'inovacao_texto' );



							ancora(

								$inovacao_link_url,

								obterImg( $inovacao_logo, 'full', 'inovacao_logo_img' ),

								AUTO,

								'inovacao_logo'

							);



							// -----------------------------------------------------------------------

							// BOTÕES



							ancora( $inovacao_link_url, $inovacao_link_rotulo . imgTema( 'mais_amarelo.png', '&rarr;', 'inovacao_botao_img' ), 0, 'btn inovacao_botao' );



							?>

						</div><!-- #inovacao_conteudo -->

					</div><!-- .col -->

				</div><!-- .row -->

				<?php



				// ancora(

				// 	'#eventos',

				// 	imgTema( 'seta_amarela.png', '&darr;', 'home_scroll_img' ),

				// 	false,

				// 	'home_scroll amarelo'

				// );



				?>

			</div><!-- .container -->

		</div><!-- #inovacao_wrapper -->

	</section><!-- #inovacao -->

	<?php







	// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	// EVENTOS



	extract( $eventos[0], EXTR_PREFIX_ALL, 'eventos' );



	?>

	<section id="eventos" class="secao">

		<div id="eventos_wrapper" class="secao_wrapper">

			<div class="container">

				<?php



				// -----------------------------------------------------------------------

				// INTRO



				print E::h1( null, 'secao_titulo' )

					->span( $eventos_titulo1, 'linha1' )

					->span( $eventos_titulo2, 'linha2' );



				// -----------------------------------------------------------------------

				// POSTS - DESTAQUE



				if ( $eventos_destaque == 'pick' ) {

					$destaquePost =& $eventos_destaque_post;

				} else {

					$destaqueQuery = array(

						'category'				=> CAT_EVENTOS,

						'order'					=> 'date',

						'posts_per_page'		=> 1,

					);

					if ( $eventos_destaque == 'sticky' ) {

						$destaqueQuery['post__in'] = $sticky_posts;

					} else {

						$destaqueQuery['orderby'] = 'ASC';

						$destaqueQuery['order'] = 'meta_value_datetime';

						$destaqueQuery['meta_key'] = 'data_evento';

					}

					$destaquePost = obter( 0, get_posts( $destaqueQuery ) );

				}



				// -----------------------------------------------------------------------

				// POSTS - SIMPLES



				if ( $eventos_simples == 'pick' ) {

					$simplesPosts =& $eventos_simples_posts;

				} else {

					$simplesQuery = array(

						'category'				=> CAT_EVENTOS,

						// 'ignore_sticky_posts'	=> $eventos_simples != 'sticky',

						'posts_per_page'		=> 2,

					);

					if ( $eventos_simples == 'date' ) {

						$simplesQuery['order'] = 'date';

					} else {

						$simplesQuery['orderby'] = 'ASC';

						$simplesQuery['order'] = 'meta_value_datetime';

						$simplesQuery['meta_key'] = 'data_evento';

					}

					if ( $destaquePost )

						$simplesQuery['post__not_in'] = array( $destaquePost->ID );

					$simplesPosts = get_posts( $simplesQuery );

				}



				// -----------------------------------------------------------------------

				// POSTS - HTML



				?>

				<div id="eventos_posts" class="equalizar_lista">

					<div class="row">

						<div class="col-xs-12 col-sm-7">

							<div id="eventos_post_destaque">

								<?php

								Componente::postsAgenda( array(

									'posts'			=> array( $destaquePost ),

									'post_class'	=> 'equalizar_item',

									'colunas'		=> 1,

									'usar_thumb'	=> true,

								) );

								?>

							</div><!-- #eventos_post_destaque -->

						</div><!-- .col -->

						<div class="col-xs-12 col-sm-5">

							<div id="eventos_posts_simples" class="equalizar_item">

								<?php

								Componente::postsAgenda( array(

									'posts'			=> $simplesPosts,

									'colunas'		=> 1,

								) );

								?>

							</div><!-- #eventos_posts_simples -->

						</div><!-- .col -->

					</div><!-- .row -->

				</div><!-- #eventos_posts -->

				<?php



				// -----------------------------------------------------------------------

				// BOTÕES



				ancora( get_term_link( CAT_EVENTOS, 'category' ), $eventos_link_rotulo . imgTema( 'mais_azul.png', '&rarr;', 'eventos_botao_img' ), AUTO, 'btn eventos_botao' );



				?>

			</div><!-- .container -->

		</div><!-- #eventos_wrapper -->

	</section><!-- #eventos -->







</article><!-- #home -->

<?php

get_footer();
