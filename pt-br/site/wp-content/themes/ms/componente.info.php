<section <?= $this->attr( $this->_attrWrapper ) ?>>
	<div class="info_grafico <?= $param_icone || $param_fa ? 'com' : 'sem' ?>_img <?php if ( $param_alinhar ) print 'vert_item' ?>">
		<?php
		if ( $param_icone ) :
			?>
			<img src="<?= esc_url( $param_icone ) ?>" alt="<?= esc_html( $param_titulo ) ?>" class="info_grafico_img" />
			<?php
		elseif ( $param_fa ) :
			?>
			<i class="fa fa-<?= esc_attr( $param_fa ) ?> info_grafico_icone"></i>
			<?php
		endif;
		?>
	</div><!-- .info_grafico -->
	<div class="info_conteudo <?php if ( $param_alinhar ) print 'vert_item' ?>">
		<h3 class="info_titulo"><?= $param_esc_titulo ? esc_html( $param_titulo ) : $param_titulo ?></h3>
		<?php
		if ( $param_texto ) :
			?>
			<p class="info_texto"><?= $param_esc_texto ? esc_html( $param_texto ) : $param_texto ?></p>
			<?php
		endif;
		?>
	</div><!-- .info_conteudo -->
</section><!-- .info_item -->