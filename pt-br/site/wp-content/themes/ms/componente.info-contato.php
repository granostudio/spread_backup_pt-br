<?php
// @requer classe Elemento, formatarEmail
?>

<div class="row">
	<div class="col-xs-12 col-md-6">
		<div class="info_col1">
			<?=
			E::h2( $param_intro_titulo, 'info_titulo intro_titulo' ) .
			E::p( $param_intro_conteudo, 'intro_conteudo' ) .
			E::h2( $param_telefone_titulo, 'info_titulo telefone_titulo' ) .
			E::p( $param_telefone_numero, 'telefone_numero' ) .
			E::h2( $param_email_titulo, 'info_titulo email_titulo' ) .
			E::p( formatarEmail( $param_email_endereco ), 'email_endereco' )
			?>
		</div><!-- .info_col1 -->
	</div><!-- .col -->
	<div class="col-xs-12 col-md-6">
		<div class="info_col2">
			<div id="formulario-contato-9c4d67f19c687a457ae9"></div>
			<script type="text/javascript" src="https://d335luupugsy2.cloudfront.net/js/rdstation-forms/stable/rdstation-forms.min.js"></script>
			<script type="text/javascript">
			  new RDStationForms('formulario-contato-9c4d67f19c687a457ae9-html', '').createForm();
			</script>
			<style>
			#conversion-formulario-contato-9c4d67f19c687a457ae9{
				background-color: transparent!important;
			}
			#conversion-modal-formulario-contato-9c4d67f19c687a457ae9 .modal-content section form input, #conversion-modal-formulario-contato-9c4d67f19c687a457ae9 .modal-content section form select, #conversion-modal-formulario-contato-9c4d67f19c687a457ae9 .modal-content section form textarea, #conversion-modal-formulario-contato-9c4d67f19c687a457ae9 .modal-content section form .select2-choice, #conversion-formulario-contato-9c4d67f19c687a457ae9 section input, #conversion-formulario-contato-9c4d67f19c687a457ae9 section select, #conversion-formulario-contato-9c4d67f19c687a457ae9 section .select2-choice {
			    display: inline-block !important;
			    background-color: transparent!important;
			    vertical-align: middle !important;
			    font-size: 18px !important;
			    line-height: 20px !important;
			    width: 100% !important;
			    height: 50px !important;
			    margin: 0 !important;
			    padding: 10px !important;
			    -webkit-border-radius: 3px !important;
			    -moz-border-radius: 3px !important;
			    border-radius: 25px !important;
					border:2px solid #fff!important;
					font-family: "Dosis", sans-serif!important;
					color: white!important;
				}
				 #conversion-formulario-contato-9c4d67f19c687a457ae9 section textarea{
					 display: inline-block !important;
 			    background-color: transparent!important;
 			    vertical-align: middle !important;
 			    font-size: 18px !important;
 			    line-height: 20px !important;
 			    width: 100% !important;
					margin: 0 !important;
					padding: 10px !important;
					-webkit-border-radius: 3px !important;
					-moz-border-radius: 3px !important;
					border-radius: 25px !important;
					border:2px solid #fff!important;
					font-family: "Dosis", sans-serif!important;
					color: white!important;
				 }
				 #conversion-formulario-contato-9c4d67f19c687a457ae9 section div.actions .call_button{
				 	border-radius:25px!important;
				 }
				 #conversion-formulario-contato-9c4d67f19c687a457ae9 div.actions .call_button{
					 background: #FEA32E!important;
					 border: 1px solid #FEA32E!important;
					 color: #FFFFFF!important;
					 -webkit-appearance: none !important;
					 -webkit-border-radius: 0 !important;
				 }
				 #conversion-formulario-contato-9c4d67f19c687a457ae9 #conversion-form-formulario-contato-9c4d67f19c687a457ae9 p.notice {
				     font-size: 10px!important;
				     margin-top: 10px!important;
				     word-break: break-word!important;
				     text-align: center!important;
				 }				 
			</style>
			<?php
			// do_shortcode( str_replace( '&quot;', '"', $param_form ) )
			?>
		</div><!-- .info_col2 -->
	</div><!-- .col -->
</div><!-- .row -->
