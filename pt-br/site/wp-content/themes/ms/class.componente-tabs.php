<?php

class Componente_Tabs extends Componente_Abstrato {
	
	protected $_attrWrapper = array( 'id', 'class' );
	
	public function __construct( $novosParams = null ) {
		// Configurações do componente
		$this->_nome = 'Tabs';
		$this->_slug = 'tabs';
		$this->_aceitarParamsNovos = false;
		$this->_padroes = array(
			'id'				=> null,
			'class'				=> 'tabs',
			'tabs'				=> null,
		);
		$this->_sanitizadores = array(
		);
		$this->_obrigatorios = array(
			'tabs'				=> 1,
		);
		$this->_validadores = array(
			'tabs'				=> 'is_array',
			'id'				=> 'is_string',
			'class'				=> 'is_string',
		);
		$this->_regrasJuncao = array(
			'class'				=> '+',
		);
		// Inicialização
		parent::__construct( $novosParams );
	}
		
}