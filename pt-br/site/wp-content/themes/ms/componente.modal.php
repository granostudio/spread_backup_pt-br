<div <?= $this->attr( $this->_attrWrapper ) ?> tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<?php if ( $param_mostrar_btn_fechar ) : ?>
					<button type="button" class="close" data-dismiss="modal" aria-label="Fechar"><span class="rotulo" aria-hidden="true">&times;</span></button>
				<?php endif; ?>
				<?php if ( $param_titulo ) : ?>
					<h4 class="modal-title"><?= esc_html( $param_titulo ) ?></h4>
				<?php endif; ?>
				<?= $param_cabecalho ?>
			</div>
			<?php if ( $param_corpo ) : ?>
				<div class="modal-body">
					<?= $param_corpo ?>
				</div>
			<?php endif; ?>
			<?php if ( $param_rodape ) : ?>
				<div class="modal-footer">
					<?= $param_rodape ?>
				</div>
			<?php endif; ?>
		</div><!-- .modal-content -->
	</div><!-- .modal-dialog -->
</div><!-- .modal -->