<?php

class Componente_Modal extends Componente_Abstrato {
	
	protected $_attrWrapper = array( 'id', 'class' );
	
	public function __construct( $novosParams = null ) {
		// Configurações do componente
		$this->_nome = 'Modal';
		$this->_slug = 'modal';
		$this->_aceitarParamsNovos = false;
		$this->_padroes = array(
			'id'				=> null,
			'class'				=> 'modal',
			'titulo'			=> null,
			'cabecalho'			=> null,
			'corpo'				=> null,
			'rodape'			=> null,
			'mostrar_btn_fechar'=> true,
			'fade'				=> true,
			'post_id'			=> null,
		);
		$this->_sanitizadores = array(
		);
		$this->_obrigatorios = array(
		);
		$this->_validadores = array(
			'id'				=> 'is_string',
			'class'				=> 'is_string',
			'titulo'			=> 'is_string',
			'cabecalho'			=> 'is_string',
			'corpo'				=> 'is_string',
			'rodape'			=> 'is_string',
			'post_id'			=> 'is_int',
		);
		$this->_regrasJuncao = array(
			'class'				=> '+',
		);
		// Inicialização
		parent::__construct( $novosParams );
	}
	
	public function renderizar( $_imprimir = true ) {
		
		global $wpdb;
		
		// ID padrão
		if ( !$this->id )
			$this->id = $this->obterId();
		
		// Carrega conteúdo de um post
		if ( $this->post_id ) {
			$wpdb->query("
				SELECT post_title, post_content
				FROM $wpdb->posts
				WHERE ID = $this->post_id
				LIMIT 1
			");
			if ( $wpdb->query->found_rows ) {
				$this->titulo = $wpdb->get_var( null, 0 );
				$this->corpo = wpautop( do_shortcode( $wpdb->get_var( null, 1 ) ) );
			}
		}
		
		// Fade
		if ( $this->fade )
			$this->_params['class'] .= ' fade';
				
		return parent::renderizar( $_imprimir );
		
	}
		
}