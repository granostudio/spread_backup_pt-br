<?php
/*
 * Funções suplementares para o site
 *
 * Desenvolvido pela MS360, 2017 - www.montarsite.com.br
 */

// Definições

function bit( $casa = null ) {
	// Retorna um número que contém apenas um bit ligado (potência de 2), que aumenta (move o bit para a esquerda) a cada execução
	// Útil para definir os valores para uma série de flags que podem ser combinadas entre si, onde não é importante visualizar o número que cada flag representa
	// Se $casa for true, reseta o contador interno para o primeiro bit
	// Se $casa for um número, reseta o contador para o bit dado (contando a partir de 1)
	static $expAtual = 0;
	if ( $casa && is_bool( $casa ) )
		$expAtual = 0;
	elseif ( $casa && is_numeric( $casa ) )
		$expAtual = abs( $casa ) - 1;
	else
		$expAtual++;
	return pow( 2, $expAtual );
}

function urlAtual() {
	global $wp;
	static $urlAtual = null;
	if ( $urlAtual )
		return $urlAtual;
	return $urlAtual = add_query_arg( $_GET, home_url( $wp->request ) );
}

define( 'AUTO', -1 );

define( 'RESUMO_INCLUIR_ELIPSE', bit(true) );
define( 'RESUMO_PONTUACAO_FINAL', bit() );
define( 'RESUMO_PRESERVAR_QUEBRAS', bit() );
define( 'RESUMO_REMOVER_SHORTCODES', bit() );
define( 'RESUMO_REMOVER_URLS', bit() );
define( 'RESUMO_OPCOES_PADRAO', RESUMO_INCLUIR_ELIPSE | RESUMO_PONTUACAO_FINAL | RESUMO_REMOVER_SHORTCODES | RESUMO_REMOVER_URLS );

define( 'IMG_SVG_INLINE', true );



// Lógica

function vazio( $valor ) {
	// Verifica se um valor é realmente vazio
	if ( is_string( $valor ) )
		return $valor === '';
	return !$valor;
}

function umDe() {
	// Retorna o primeiro valor não vazio dos argumentos passados
	// @requer vazio()
	// @alt primeiroDe()
	$args = func_get_args();
	foreach ( $args as $valor )
		if ( !vazio( $valor ) )
			return $valor;
	return $valor;
}



// Arrays e formulários

function obter( $key, $arr, $default = null ) {
	// Returns a value from the array, if it exists, or the $default otherwise
	if ( !is_array( $arr ) || ( !is_string( $key ) && !is_numeric( $key ) ) )
		return $default;
	if ( !array_key_exists( $key, $arr ) )
		return $default;
	return is_string( $arr[ $key ] )
		? trim( $arr[ $key ] )
		: $arr[ $key ]
	;
}

function obterMult( $chaves, &$arr, $default = null ) {
	// @requer obter
	if ( !is_array( $arr ) )
		return $default;
	if ( !is_array( $chaves ) )
		$chaves = array( $chaves );
	$valores = array();
	foreach ( $chaves as $c )
		$valores[$c] = obter( $c, $arr, $default );
	return $valores;
}

function post( $key, $default = null ) {
	// @alias obter
	return obter( $key, $_POST, $default );
}

function get( $key, $default = null ) {
	// @alias obter
	return obter( $key, $_GET, $default );
}

function req( $key, $default = null ) {
	// @alias obter
	return obter( $key, $_REQUEST, $default );
}

function sess( $key, $default = null ) {
	// @alias obter
	return obter( $key, $_SESSION, $default );
}

function reqMult( $chaves, $default = null ) {
	// @alias obterMult
	$dados = obterMult( $chaves, $_REQUEST, $default );
	$dados = array_map( 'stripslashes', $dados );
	return $dados;
}

function chain( $valorInicial ) {
	// Aplica qualquer quantidade de funções ao $valorInicial enquanto que ele não for vazio
	// @requer vazio
	if ( vazio( $valorInicial ) )
		return false;
	$totalFuncs = func_num_args() - 1;
	for ( $f = 1; $f <= $totalFuncs; $f++ ) {
		$func = func_get_arg($f);
		if ( !is_callable( $func ) )
			continue;
		$valorInicial = call_user_func( $func, $valorInicial );
		if ( vazio( $valorInicial ) )
			return false;
	}
	return $valorInicial;
}

function array_replace_existing( $arrPrimaria ) {
	// Substitui os valores das chaves na $arrPrimaria pelos valores de chaves iguais em outras arrays; ignora chaves não existentes na $arrPrimaria
	if ( !is_array( $arrPrimaria ) )
		return false;
	$total = func_num_args();
	if ( $total <= 1 )
		return $arrPrimaria;
	$args = func_get_args();
	for ( $i = 1; $i < $total; $i++ ) {
		if ( !is_array( $args[$i] ) || empty( $args[$i] ) )
			continue;
		foreach ( $args[$i] as $chave => $valor ) {
			if ( array_key_exists( $chave, $arrPrimaria ) )
				$arrPrimaria[ $chave ] = $valor;
		}
	}
	return $arrPrimaria;
}

if (!function_exists('array_column')) {

    /**
	 * For the full copyright and license information, please view the LICENSE
	 * file that was distributed with this source code.
	 *
	 * @copyright Copyright (c) 2013 Ben Ramsey <http://benramsey.com>
	 * @license http://opensource.org/licenses/MIT MIT
	 *
     * Returns the values from a single column of the input array, identified by
     * the $columnKey.
     *
     * Optionally, you may provide an $indexKey to index the values in the returned
     * array by the values from the $indexKey column in the input array.
     *
     * @param array $input A multi-dimensional array (record set) from which to pull
     *                     a column of values.
     * @param mixed $columnKey The column of values to return. This value may be the
     *                         integer key of the column you wish to retrieve, or it
     *                         may be the string key name for an associative array.
     * @param mixed $indexKey (Optional.) The column to use as the index/keys for
     *                        the returned array. This value may be the integer key
     *                        of the column, or it may be the string key name.
     * @return array
     */
    function array_column($input = null, $columnKey = null, $indexKey = null)
    {
        // Using func_get_args() in order to check for proper number of
        // parameters and trigger errors exactly as the built-in array_column()
        // does in PHP 5.5.
        $argc = func_num_args();
        $params = func_get_args();

        if ($argc < 2) {
            trigger_error("array_column() expects at least 2 parameters, {$argc} given", E_USER_WARNING);
            return null;
        }

        if (!is_array($params[0])) {
            trigger_error('array_column() expects parameter 1 to be array, ' . gettype($params[0]) . ' given', E_USER_WARNING);
            return null;
        }

        if (!is_int($params[1])
            && !is_float($params[1])
            && !is_string($params[1])
            && $params[1] !== null
            && !(is_object($params[1]) && method_exists($params[1], '__toString'))
        ) {
            trigger_error('array_column(): The column key should be either a string or an integer', E_USER_WARNING);
            return false;
        }

        if (isset($params[2])
            && !is_int($params[2])
            && !is_float($params[2])
            && !is_string($params[2])
            && !(is_object($params[2]) && method_exists($params[2], '__toString'))
        ) {
            trigger_error('array_column(): The index key should be either a string or an integer', E_USER_WARNING);
            return false;
        }

        $paramsInput = $params[0];
        $paramsColumnKey = ($params[1] !== null) ? (string) $params[1] : null;

        $paramsIndexKey = null;
        if (isset($params[2])) {
            if (is_float($params[2]) || is_int($params[2])) {
                $paramsIndexKey = (int) $params[2];
            } else {
                $paramsIndexKey = (string) $params[2];
            }
        }

        $resultArray = array();

        foreach ($paramsInput as $row) {

            $key = $value = null;
            $keySet = $valueSet = false;

            if ($paramsIndexKey !== null && array_key_exists($paramsIndexKey, $row)) {
                $keySet = true;
                $key = (string) $row[$paramsIndexKey];
            }

            if ($paramsColumnKey === null) {
                $valueSet = true;
                $value = $row;
            } elseif (is_array($row) && array_key_exists($paramsColumnKey, $row)) {
                $valueSet = true;
                $value = $row[$paramsColumnKey];
            }

            if ($valueSet) {
                if ($keySet) {
                    $resultArray[$key] = $value;
                } else {
                    $resultArray[] = $value;
                }
            }

        }

        return $resultArray;
    }

}



// Números

function randomHash( $comprimento = 6, $base = 16, $especiais = 0 ) {
	$base = max( min( intval( $base ), 36 ), 2 );
	$limite = mt_getrandmax();
	$max = pow( $base, (int) $comprimento ) - 1;
	$charEspeciais = '!@#$%*;:+=';
	$lenEspeciais = strlen( $charEspeciais );
	$especiais = intval( $especiais );
	if ( $max > $limite ) {
		// A criação de uma hash longa deve ser quebrada em partes
		$lenLimite = strlen( base_convert( $limite, 10, $base ) ) - 1;
		$iteracoes = floor( $comprimento / $lenLimite );
		$resto = $comprimento % $lenLimite;
		$hex = '';
		while ( $iteracoes-- )
			$hex .= randomHash( $lenLimite, $base );
		if ( $resto )
			$hex .= randomHash( $resto, $base );
	} else {
		// Hash curta
		$hex = str_pad(
			base_convert(
				mt_rand(
					0,
					$max
				),
				10,
				$base
			),
			$comprimento,
			'0',
			STR_PAD_LEFT
		);
	}
	// Substitui caracteres especiais
	for ( $t = $especiais; $t; $t-- ) {
		$hexC = rand( 0, $comprimento - 1 );
		$espC = rand( 0, $lenEspeciais - 1 );
		$hex{$hexC} = $charEspeciais{$espC};
	}
	return $hex;
}

function idUnico( $nome = 'generico' ) {
	// Retorna um ID numérico único para a denominação $nome na execução atual
	static $arrComponentes = array();
	if ( !isset( $arrComponentes[ $nome ] ) )
		$arrComponentes[ $nome ] = 0;
	return ++$arrComponentes[ $nome ];
}

function sanitizarNumerico( $valor ) {
	// Remove caracteres que não sejam números, mas deixa zeros à esquerda
	if ( is_string( $valor ) )
		$valor = preg_replace( '#\D#', '', $valor );
	return $valor;
}

function sanitizarInteiro( $valor ) {
	// Retorna um número inteiro
	if ( is_string( $valor ) )
		$valor = preg_replace( '#\D#', '', $valor );
	return (int) $valor;
}

function formatarNumero( $n, $casas = AUTO, $forçarCasas = false ) {
	$n = floatval( $n );
	// $s = sprintf( '%0.10f', $n );
	$s = strval( $n );
	$ponto = strpos( $s, '.' );
	if ( $ponto ) {
		$casasAtuais = strlen( substr( $s, $ponto + 1 ) );
		if ( $casas == AUTO )
			$casas = $casasAtuais;
		elseif ( !$forçarCasas )
			$casas = min( $casas, $casasAtuais );
	} else {
		if ( !$forçarCasas )
			$casas = 0;
	}
	$s = number_format( $n, $casas, ',', '.' );
	if ( !$forçarCasas )
		$s = rtrim( $s, '0,' );
	if ( !$s )
		return '0';
	return $s;
}

function formatarTelefone( $valor ) {
	// @requer sanitizarNumerico
	$valor = sanitizarNumerico( $valor );
	if ( $valor === '' )
		return '';
	$len = strlen( $valor );
	$n = 0;
	$tel = '';
	for ( $c = 0; $c < $len; $c++ ) {
		switch ( $c . '/' . $len ) {
			case '0/11' :
			case '0/10' :
				$tel .= '(';
			break;
			case '2/11' :
			case '2/10' :
				$tel .= ') ';
			break;
			case '6/10' :
			case '4/8' :
			case '5/11' :
			case '8/11' :
			case '3/9' :
			case '6/9' :
				$tel .= '-';
			break;
		}
		$tel .= $valor{$c};
	}
	if ( empty( $valor ) )
		return '';
	return $tel;
}

function incCol( $inc, $n, $cols = 3, $base = 0 ) {
	// Multiplica o valor incremental ($inc) pela coluna atual
	// A coluna atual é determinada dividindo o número do item atual ($n) pelo total de colunas ($cols)
	// Dessa forma, o incremento é sempre resetado quando $n indica o início de uma nova coluna
	// Uma $base fixa é por fim adicionada ao resultado do cálculo
	return ( ( ( $n - 1 ) % $cols ) + 1 ) * $inc + $base;
}



// Strings

function sluggify( $string, $cola = '_' ) {
	// Forces the $string to comply with allowed characters for a variable name
	$charsFrom = array( 'à', 'á', 'ã', 'â', 'é', 'è', 'ê', 'í', 'ï', 'ì', 'ò', 'ó', 'õ', 'ô', 'ú', 'ù', 'ü', 'ç', 'ñ', 'ª', 'º', '¼',         '½',         '¾',         'ø',
		'_',   '-',   ' ',   '.',   '+',   '/',   '\\',  '\'' );
	$charsTo   = array( 'a', 'a', 'a', 'a', 'e', 'e', 'e', 'i', 'i', 'i', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'c', 'n', 'a', 'o', "1{$cola}4", "1{$cola}2", "3{$cola}4", "diam{$cola}",
		$cola, $cola, $cola, $cola, $cola, $cola, $cola, $cola );
	$string = mb_strtolower( $string, 'UTF-8' );
	$string = str_replace( $charsFrom, $charsTo, $string );
	$string = preg_replace( "#^[^a-z]+|[^\\{$cola}a-z0-9]#", '', $string );
	$string = preg_replace( "#\\$cola{2,}#", $cola, $string );
	$string = preg_replace( "#\\$cola$#", '', $string );
	return $string;
}

function validarEmail( $valor ) {
	// Retorna se é um e-mail válido
	static $emailRegexp = '#^(?:[a-zA-Z0-9_\'^&amp;/+-])+(?:\.(?:[a-zA-Z0-9_\'^&amp;/+-])+)*@(?:(?:\[?(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?))\.){3}(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\]?)|(?:[a-zA-Z0-9-]+\.)+(?:[a-zA-Z]){2,}\.?)$#';
	if ( !$valor )
		return false;
	if ( !preg_match( $emailRegexp, $valor ) )
		return false;
	return true;
}

function capitalizar( $string ) {
	if ( !$string || !is_string( $string ) )
		return '';
	$string = ucwords( mb_strtolower( trim( $string ), 'UTF-8' ) );
	$string = preg_replace_callback( '/ (de|do|da|e|y|del|della) /i', function( $matches ) { return ' ' . strtolower( $matches[1] ) . ' '; }, $string );
	$string = preg_replace_callback( '/D\'([a-z])/i', function( $matches ) { return 'd\'' . strtoupper( $matches[1] ); }, $string );
	$string = preg_replace( '/ {2,}/', ' ', $string );
	return $string;
}

function resumir( $texto, $limite = 100, $flags = RESUMO_OPCOES_PADRAO ) {
	if ( !$texto || !is_string( $texto ) )
		return false;
	if ( !$limite )
		return $texto;
	$texto = strip_tags( $texto );
	if ( RESUMO_REMOVER_SHORTCODES & $flags ) {
		$texto = preg_replace( '#\[.+?\]#', '', $texto );
	}
	if ( RESUMO_REMOVER_URLS & $flags ) {
		// Remove URLs do corpo do texto
		$texto = preg_replace( '#https?://\S+#', '', $texto );
	}
	if ( !( RESUMO_PRESERVAR_QUEBRAS & $flags ) || RESUMO_PRESERVAR_QUEBRAS & $flags && RESUMO_PONTUACAO_FINAL & $flags ) {
		// Insere pontos finais antes das quebras de linhas, onde não houver
		$texto = preg_replace( '#[^….,;?!\)\}\]\/\\\|\s]\K *(?=[\r\n])#', '.', $texto );
	}
	if ( !( RESUMO_PRESERVAR_QUEBRAS & $flags ) ) {
		// Remove os caracteres de quebra de linha
		$texto = preg_replace( '#[\r\n]+#', ' ', $texto );
	}
	if ( !$texto )
		return false;
	// Limita o texto
	if ( strlen( $texto ) > $limite ) {
		$texto = rtrim( mb_substr( $texto, 0, $limite - ( RESUMO_INCLUIR_ELIPSE & $flags ? 1 : 0 ) ), ' ([{,:“' ) . ( RESUMO_INCLUIR_ELIPSE & $flags ? '…' : '' ); // &hellip;
	}
	$texto = trim( $texto );
	if ( RESUMO_PONTUACAO_FINAL & $flags ) {
		$pontuacao = array( '.', '!', '?', ';', '…' );
		if ( array_search( mb_substr( $texto, -1, 1 ), $pontuacao ) === false )
			$texto .= '.';
	}
	// Fim
	return $texto;
}

function gerarResumo( $limite = 150, $postEspecifico = null, $flags = RESUMO_OPCOES_PADRAO ) {
	// Resume o excerpt ou conteúdo do post dado ou do post atual
	// @requer umDe, resumir
	global $post;
	if ( !$postEspecifico )
		$postEspecifico =& $post;
	elseif ( is_numeric( $postEspecifico ) )
		$postEspecifico = get_post( $postEspecifico );
	if ( !$postEspecifico )
		return false;
	return resumir( umDe( $postEspecifico->post_excerpt, $postEspecifico->post_content ), $limite, $flags );
}

function destacarResultados( $texto, $termos, $comprimento = 100, $incluirElipse = true ) {
	// Destaca $termos de busca separados por espaços ou em array dentro do $texto com a classe 'highlight', limitando-se a mostrar $comprimento de caracteres
	if ( !$texto || !is_string( $texto ) )
		return false;
	$texto = strip_tags( $texto );
	// Insere pontos finais antes das quebras de linhas, onde não houver
	$texto = preg_replace( '#[^.,;?!\)\}\]\/\\\|\s]\K *(?=[\r\n])#', '.', $texto );
	// Remove as quebras de linha
	$texto = preg_replace( '#[\r\n]+#', ' ', $texto );
	$inicio = false;
	$fim = false;
	if ( $termos ) {
		if ( !is_array( $termos ) )
			$termos = explode( ' ', $termos );
		foreach ( $termos as $t ) {
			// Determina o termo que primeiro aparece no texto e também o último que caiba dentro do limite
			$t = trim( $t );
			if ( !$t )
				continue;
			$pos_len = 0;
			while ( ( $pos = stripos( $texto, $t, $pos_len ) ) !== false ) {
				$pos_len = $pos + strlen( $t );
				if ( $inicio === false || $pos < $inicio )
					$inicio = $pos;
				if ( $pos_len > $inicio + $comprimento )
					break;
				// pr("Found $t at $pos-$pos_len.");
				if ( $fim === false || $fim > $inicio + $comprimento || $pos_len > $fim )
					$fim = $pos_len;
			}
		}
	}
	// Busca centralizar o resultado da busca nos primeiros termos relevantes dentro do limite de comprimento
	$inicio = max( $inicio, 0 );
	$fim = min( $fim, $inicio + $comprimento );
	$faixa = $fim - $inicio;
	$margem_media = floor( ( $comprimento - $faixa ) / 2 );
	$margem_esquerda = max( $inicio - $margem_media, 0 );
	// $margem_dif = $margem_media - ( $inicio - $margem_esquerda );
	// $margem_direita = $fim + $margem_media + $margem_dif;
	// $margem_direita = min( $inicio + $comprimento - 1, strlen( $texto ) );
	$faixa_texto = trim( mb_substr( $texto, $margem_esquerda, $comprimento ) );
	// pr($inicio,$fim,$faixa,$margem_media,$margem_esquerda,$faixa_texto,strlen($faixa_texto));
	// Destaca os resultados
	if ( $termos )
		$faixa_texto = preg_replace( '/(' . addslashes( implode( '|', $termos ) ) . ')/i', '<em class="highlight">$1</em>', $faixa_texto );
	/*
	foreach ( $termos as $t ) {
		$t = trim( $t );
		if ( !$t )
			continue;
		$faixa_texto = str_ireplace( $t, "<em class='highlight'>$t</em>", $faixa_texto );
	}
	*/
	// Adiciona as elipses
	if ( $incluirElipse && $margem_esquerda > 0 )
		$faixa_texto = '&hellip;' . $faixa_texto;
	if ( $incluirElipse && $margem_direita < strlen( $texto ) - 1 )
		$faixa_texto .= '&hellip;';
	// Fim
	return $faixa_texto;
}

function destacarResultadosBusca( $post, $comprimento = 100, $incluirElipse = true ) {
	// @alias destacarResultados
	return destacarResultados( $post->post_content, get_search_query(), $comprimento, $incluirElipse );
}

function plural( $n = 1, $singular = '', $plural = 's' ) {
	// Retorna o string para plural ou singular dependendo do número; zero retorna plural
	return abs( $n ) == 1
		? $singular
		: $plural
	;
}

function imprimirJS( $var, $valor = null, $declararVar = false, $continuar = false ) {
	/*
	 * Imprime um $valor como uma variável em JavaScript de nome $var, opcionalmente com declaração var antes
	 * Também aceita o primeiro argumento como array de variáveis e valores, para realizar várias declarações de uma vez
	 */
	// @param string/array $var = nome da variável, ou array de variáveis
	// @param mixed $valor = valor da variável, se não for array
	// @param bool $declararVar = adicionar declaração var, se não for array
	// @param bool $continuar = (interno) regula a adição de vírgulas para arrays
	if ( is_array( $var ) ) {
		$primeiro = key( $var );
		end( $var );
		$ultimo = key( $var );
		foreach ( $var as $varAtual => $valorAtual )
			imprimirJS( $varAtual, $valorAtual, $valor && $varAtual == $primeiro, $valor && $varAtual != $ultimo );
	} else {
		if ( $declararVar )
			print 'var ';
		print $var . ' = ';
		if ( is_array( $valor ) || is_object( $valor ) )
			print 'JSON.parse("' . addslashes( json_encode( $valor ) ) . '")';
		elseif ( is_string( $valor ) )
			print "\"" . addslashes( $valor ) . "\"";
		elseif ( is_null( $valor ) )
			print 'null';
		else
			print $valor;
		print $continuar
			? ",\n\t"
			: ";\n"
		;
	}
}



// Datas

function sanitizarData( $data ) {
	if ( is_int( $data ) )
		return $data;
	elseif ( !is_string( $data ) )
		return false;
	elseif ( preg_match( '#^[0-3][0-9]/[01][0-9]/[12][09][0-9][0-9]$#', $data ) ) {
		// Converte de dd/mm/YYYY para ISO YYYY-mm-dd
		$data =
			substr( $data, 6, 4 ) . '-' .
			substr( $data, 3, 2 ) . '-' .
			substr( $data, 0, 2 );
	} elseif ( preg_match( '#^[12][09][0-9][0-9]-[01][0-9]-[0-3][0-9]$#', $data ) ) {
		// Está em formato ISO
	} else {
		// Formato desconhecido
		return false;
	}
	return strtotime( $data );
}

function formatarData( $data ) {
	if ( !$data )
		return '';
	if ( !is_numeric( $data ) && is_string( $data ) )
		$data = strtotime( $data );
	return date( 'd/m/Y', $data );
}

function formatarTempo( $data ) {
	if ( !$data )
		return '';
	if ( !is_numeric( $data ) && is_string( $data ) )
		$data = strtotime( $data );
	return date( 'H:i', $data );
}

function formatarDataHora( $data, $compacto = true ) {
	if ( !$data )
		return '';
	if ( !is_numeric( $data ) && is_string( $data ) )
		$data = strtotime( $data );
	return date( $compacto ? 'd/m H:i' : 'd/m/Y \\à\\s H:i:s', $data );
}

function formatarHora( $data, $compacto = true ) {
	if ( !$data )
		return '';
	if ( is_numeric( $data ) )
		// Data do PHP
		return date( $compacto ? 'd/m H:i' : 'd/m/Y H:i:s', $data );
	else
		// Data do MySQL no formato HH:MM:SS
		return $compacto ? substr( $data, 0, 5 ) : $data;
}



// HTML

function col( $n, $min = 2 ) {
	// Retorna a largura que cada coluna deve ter em um grid de 12 se este for dividido por $n
	// Respeita $min como largura mínima
	// Se a divisão for para 5, 7 ou 11 colunas, utiliza a classe custom Nths
	$n = ceil( $n );
	if ( !$n )
		return false;
	return 12 % $n && 12 / $n > $min ? $n . 'ths' : max( floor( 12 / $n ), $min );
}

function row( $n, $colunas, $imprimir = true ) {

	// Gera uma div com clearfix em breakpoints específicos, caso $n coincida com o final de um grupo de colunas naquele breakpoint
	// @param (int) $n - Coluna atual (começando por 1)
	// @param (string) $colunas - Classe com colunas do bootstrap (ex: col-xs-12 col-sm-6 col-lg-4); devem ser divisões perfeitas de 12 ou a classe custom '5ths'

	/*
	if ( $n > 12 ) {
		$n %= 12;
		if ( $n == 0 )
			$n = 12;
	}
	*/

	// O número 12 sempre corresponde ao final de qualquer divisão de colunas
	/*
	if ( $n == 12 ) {
		print '<div class="clearfix"></div>';
		return true;
	}
	*/

	// Alguns números nunca corresponderão com o final de uma linha
	if ( $n == 0 || $n == 1 || $n == 7 || $n == 11 || $n == 13 || $n == 17 || $n == 19 || $n == 23 ) //  || $n == 5
		return false;

	// Obtém informações das colunas
	preg_match( '/col-xs-(\d+)(ths)?/', $colunas, $matches );
	$xs = $matches
		? $matches[2] ? 12 / (int) $matches[1] : (int) $matches[1] // ceil( 12 / (int) $matches[1] )
		: 12
	;
	preg_match( '/col-sm-(\d+)(ths)?/', $colunas, $matches );
	$sm = $matches
		? $matches[2] ? 12 / (int) $matches[1] : (int) $matches[1]
		: $xs
	;
	preg_match( '/col-md-(\d+)(ths)?/', $colunas, $matches );
	$md = $matches
		? $matches[2] ? 12 / (int) $matches[1] : (int) $matches[1]
		: $sm
	;
	preg_match( '/col-lg-(\d+)(ths)?/', $colunas, $matches );
	$lg = $matches
		? $matches[2] ? 12 / (int) $matches[1] : (int) $matches[1]
		: $md
	;
	// print "$xs,$sm,$md,$lg\n";
	// Calcula se a coluna atual está no final de algum breakpoint
	$classes = array();
	if ( $xs != 1 && $xs != 12 && $n * $xs % 12 == 0 ) //  $n % $xs == 0
		$classes[] = 'visible-xs-block';
	if ( $sm != 1 && $sm != 12 && $n * $sm % 12 == 0 )
		$classes[] = 'visible-sm-block';
	if ( $md != 1 && $md != 12 && $n * $md % 12 == 0 )
		$classes[] = 'visible-md-block';
	if ( $lg != 1 && $lg != 12 && $n * $lg % 12 == 0 )
		$classes[] = 'visible-lg-block';

	// Gera o HTML

	if ( !$classes )
		return false;

	if ( count( $classes ) == 4 ) {
		$html = '<div class="clearfix"></div>';
		if ( $imprimir )
			print $html;
		else
			return $html;
	}

	$html = '<div class="clearfix ' . implode( ' ', $classes ) . '"></div>';
	if ( $imprimir )
		print $html;
	else
		return $html;

}

function src( $imgID, $tamanho = 'full' ) {
	if ( !$imgID || !is_numeric( $imgID ) )
		return false;
	$imgAttr = wp_get_attachment_image_src( (int) $imgID, $tamanho );
	if ( !$imgAttr )
		return false;
	return esc_url( current( $imgAttr ) );
}

function obterFundo( $imgID, $tamanho = 'full', $incluirStyle = true ) {
	// @requer src
	if ( !$imgID || !is_numeric( $imgID ) )
		return;
	$img = src( $imgID, $tamanho );
	if ( $img )
		return ( $incluirStyle ? " style='" : '' ) . "background-image: url(\"$img\");" . ( $incluirStyle ? "'" : '' );
}

function fundo( $imgID, $tamanho = 'full', $incluirStyle = true ) {
	// @alias obterFundo
	print obterFundo( $imgID, $tamanho, $incluirStyle );
}

function thumbFundo( $post_id = null, $tamanho = 'full', $incluirStyle = true, $fallbackPadrao = true, $imprimir = true ) {
	// @requer obterFundo
	global $post;
	if ( !$post_id && $post )
		$post_id = $post->ID;
	elseif ( $post_id instanceof WP_Post )
		$post_id = $post_id->ID;
	if ( !$post_id )
		return false;
	$imgID = get_post_thumbnail_id( $post_id );
	if ( !$imgID ) {
		if ( $fallbackPadrao && defined('THUMB_PADRAO') )
			$imgID = THUMB_PADRAO;
		else
			return false;
	}
	$fundo = obterFundo( $imgID, $tamanho, $incluirStyle );
	if ( $imprimir )
		print $fundo;
	else
		return $fundo;
}

function obterThumbFundo( $post_id = null, $tamanho = 'full', $incluirStyle = true, $fallbackPadrao = true ) {
	// @alias thumbFundo
	return thumbFundo( $post_id, $tamanho, $incluirStyle, $fallbackPadrao, false );
}

function obterImg( $imgID, $tamanho = 'full', $attr = array(), $alt = null ) {
	// Retorna o HTML para uma imagem registrada na biblioteca de mídia do WP
	// Reconhece SVGs e obtém o código fonte para inserção inline
	// @requer umDe, elemento, IMG_SVG_INLINE, attr
	static $uploadPath = null;
	if ( !$imgID || !is_numeric( $imgID ) )
		return false;
	if ( is_array( $attr ) )
		$attr = array_merge( array( 'class' => 'item_img' ), $attr );
	elseif ( is_string( $attr ) )
		$attr = array( 'class' => $attr );
	else
		$attr = array();
	// return wp_get_attachment_image( (int) $imgID, $tamanho, false, $attr );
	list( $src, $w, $h, $intermediate ) = wp_get_attachment_image_src( (int) $imgID, $tamanho );
	if ( IMG_SVG_INLINE && strtolower( substr( $src, -4 ) ) == '.svg' ) {
		// SVG - deve ser inserido inline
		if ( !$uploadPath )
			$uploadPath = wp_upload_dir( null, false, false )['basedir'];
		$path = $uploadPath . '/' . get_post_meta( $imgID, '_wp_attached_file', true );
		$xml = @file_get_contents( $path );
		if ( !$xml )
			return false;
		$inicio = strpos( $xml, '<svg' );
		if ( $inicio === false )
			return false;
		$xml = rtrim( '<svg ' . attr( $attr ) . substr( $xml, $inicio + 4 ) );
		return $xml;
	} else {
		// Imagem normal
		$attr['src'] = $src;
		$attr['width'] = $w;
		$attr['height'] = $h;
		if ( !isset( $attr['alt'] ) ) {
			if ( $alt )
				$attr['alt'] = $alt;
			else
				$attr['alt'] = umDe( get_post_meta( $imgID, '_wp_attachment_image_alt', true ), null );
		}
		return elemento( 'img', null, $attr, false );
	}
}

function img( $imgID, $tamanho = 'full', $attr = array() ) {
	// @alias obterImg
	print obterImg( $imgID, $tamanho, $attr );
}

function obterPostThumb( $post_id = null, $tamanho = 'full', $attr = array(), $fallbackPadrao = true ) {
	// Retorna o HTML para o thumbnail do post
	// @requer obterImg, THUMB_PADRAO
	global $post;
	if ( !$post_id && $post )
		$post_id = $post->ID;
	elseif ( $post_id instanceof WP_Post )
		$post_id = $post_id->ID;
	if ( !$post_id )
		return false;
	$imgID = get_post_thumbnail_id( $post_id );
	if ( !$imgID ) {
		if ( $fallbackPadrao && defined('THUMB_PADRAO') )
			$imgID = THUMB_PADRAO;
		else
			return false;
	}
	return obterImg( $imgID, $tamanho, $attr );
}

function lightbox( $imgID, $tamanho = 'thumbnail', $attr = array(), $gallery = 'padrao', $imprimir = true ) {
	// @requer ancora, src, obterImg
	$thumb = obterImg( $imgID, $tamanho, $attr );
	return ancora( src( $imgID, 'full' ), $thumb, true, array( 'rel' => 'lightbox', 'data-gallery' => $gallery, 'class' => 'lightbox' ), $imprimir );
}

function attr( $arrAttr ) {
	// Gera o HTML para a array associativa $arrAttr
	// Propriedades com valores nulos serão ignoradas; booleanos serão tratados de acordo
	// É possível fornecer mais de uma array para sobrescrever as propriedades que já estiverem presentes em $arrAttr
	// @requer array_replace_existing
	if ( !is_array( $arrAttr ) || !count( $arrAttr ) )
		return;
	if ( func_num_args() > 1 ) {
		$args = func_get_args();
		$arrAttr = call_user_func_array( 'array_replace_existing', $args );
		/*
		array_shift( $args );
		foreach ( $args as $arr ) {
			if ( !is_array( $arr ) || empty( $arr ) )
				continue;
			$arrAttr = array_merge( $arrAttr, $arr );
		}
		*/
	}
	$html = array();
	foreach ( $arrAttr as $a => $v ) {
		if ( is_null( $v ) || is_array( $v ) || is_object( $v ) )
			continue;
		if ( is_bool( $v ) ) {
			if ( $v )
				$html[] = $a;
		} else {
			$html[] = $a . '="' . esc_attr( $v ) . '"';
		}
	}
	return implode( ' ', $html );
}

function elemento( $tag, $conteudo = null, $attr = null, $imprimir = true ) {
	// @requer attr, sanitizarAttr
	if ( !is_string( $tag ) || !$tag )
		return false;
	// Gera o HTML
	$html = '<' . $tag;
	$attr = sanitizarAttr( $attr );
	$attrHtml = attr( $attr );
	if ( $attrHtml )
		$html .= ' ' . $attrHtml;
	if ( is_scalar( $conteudo ) ) {
		$html .= '>';
		$html .= $conteudo;
		$html .= "</$tag>";
	} else {
		$html .= ' />';
	}
	// Finaliza
	if ( $imprimir )
		print $html;
	else
		return $html;
}

function elementoNaoVazio( $tag, $conteudo = null, $attr = null, $imprimir = true ) {
	// @alias elemento
	// @requer vazio
	if ( !vazio( $conteudo ) )
		return elemento( $tag, $conteudo, $attr, $imprimir );
}

function subElemento( $tag, $conteudo = null, $attr = null ) {
	// @alias elemento
	return elemento( $tag, $conteudo, $attr, false );
}

function subElementoNaoVazio( $tag, $conteudo = null, $attr = null ) {
	// @alias elemento
	// @requer vazio
	if ( !vazio( $conteudo ) )
		return elemento( $tag, $conteudo, $attr, false );
}

function el( $tag, $conteudo = null, $attr = null ) {
	// @alias elemento
	// @requer vazio
	if ( !vazio( $conteudo ) )
		return elemento( $tag, $conteudo, $attr, false );
}

function wrap( $conteudo, $antes = '', $depois = '' ) {
	// @requer vazio
	if ( vazio( $conteudo ) )
		return;
	print $antes . $conteudo . $depois;
}

function ancora( $href, $innerHtml = '', $novaJanela = false, $attr = array(), $imprimir = true ) {
	// @requer sanitizarAttr, attr
	$html = '';
	if ( is_int( $href ) )
		$href = get_the_permalink( $href );
	$attr = sanitizarAttr( $attr );
	if ( $href ) {
		$href = esc_url( $href );
		if ( $novaJanela == -1 ) {
			// Modo automático - aciona para links externos
			$novaJanela = preg_match( '#^https?://#', $href ) && !preg_match( '#^'.site_url().'#', $href );
		}
		if ( $novaJanela )
			$attr['target'] = '_blank';
		$attrHtml = attr( $attr );
		$html .= "<a href='$href' $attrHtml>";
	}
	$html .= $innerHtml;
	if ( $href ) {
		$html .= "</a>";
	}
	if ( $imprimir )
		print $html;
	else
		return $html;
}

function ancoraNaoVazia( $href, $innerHtml = '', $novaJanela = false, $attr = array(), $imprimir = true ) {
	// @alias ancora
	if ( !$href )
		return false;
	return ancora( $href, $innerHtml, $novaJanela, $attr, $imprimir );
}

function a( $href, $innerHtml = '', $novaJanela = false, $attr = array() ) {
	// @alias ancora
	if ( !$href )
		return false;
	return ancora( $href, $innerHtml, $novaJanela, $attr, false );
}

function hotspot( $url, $novaJanela = false, $attr = array(), $imprimir = true ) {
	// @alias ancora
	if ( !$url )
		return false;
	if ( isset( $attr['class'] ) )
		$attr['class'] .= ' hotspot';
	else
		$attr['class'] = 'hotspot';
	return ancora( $url, '', $novaJanela, $attr, $imprimir );
}

function formatarEmail( $email, $attr = null ) {
	// Cria um link para o e-mail se o texto validar como tal
	// @requer validarEmail, ancora
	if ( !$email || !validarEmail( $email ) )
		return $email;
	return ancora( 'mailto:' . $email, $email, 0, $attr, false );
}

function formatarUrl( $url, $attr = null ) {
	// @alias ancora
	if ( strpos( $url, 'http' ) !== 0 )
		$url = 'http://' . $url;
	$rotulo = rtrim( preg_replace( '#^https?://#', '', $url ), '/' );
	return ancora( $url, $rotulo, 0, $attr, false );
}

function b( $conteudo ) {
	return '<strong>' . $conteudo . '</strong>';
}

function alt( $imgID, $padrao = 'Imagem' ) {
	if ( !$imgID || !is_numeric( $imgID ) )
		return $padrao;
	$meta = get_post_meta( (int) $imgID, '_wp_attachment_image_alt', true );
	return $meta ? $meta : $padrao;
}

function cl( $classe ) {
	// Atalho para retornar uma array de atributos com a classe CSS $classe
	return array( 'class' => $classe );
}

function componente( $template, $params = null, $_imprimir = true ) {
	// Carrega um arquivo PHP alimentado com os $params
	// @requer obter (utilizado nos componentes em si)
	$caminho = get_template_directory() . DIRECTORY_SEPARATOR . "componente-$template.php";
	if ( !file_exists( $caminho ) )
		return print "Componente \"$template\" não encontrado.";
	if ( is_array( $params ) )
		extract( $params, EXTR_PREFIX_ALL, 'param' );
	else
		$params = false;
	if ( !$_imprimir )
		ob_start();
	include $caminho;
	if ( !$_imprimir ) {
		$html = ob_get_contents();
		ob_end_clean();
		return $html;
	}
}

function anim( $anim = 'fadeIn', $duration = '0.4s', $delay = '0s' ) {
	if ( !is_string( $duration ) )
		$duration .= 's';
	if ( !is_string( $delay ) )
		$delay .= 's';
	return array(
		'data-anim'				=> $anim,
		'data-anim-duration'	=> $duration,
		'data-anim-delay'		=> $delay
	);
}

function animAttr( $anim = 'fadeIn', $duration = '0.4s', $delay = '0s', $imprimir = true ) {
	if ( !is_string( $duration ) )
		$duration .= 's';
	if ( !is_string( $delay ) )
		$delay .= 's';
	$attr = "data-anim='$anim' data-anim-duration='$duration' data-anim-delay='$delay'";
	if ( $imprimir )
		print $attr;
	return $attr;
}

function linksCompartilhamento( $tokens = null, $imprimir = true ) {
	// @requer gerarResumo
	global $post;
	if ( !$tokens || !is_array( $tokens ) ) {
		$tokens = array(
			'url'			=> get_the_permalink(),
			'titulo'		=> strip_tags( $post->post_title ),
			'resumo'		=> gerarResumo( 60, $post ),
		);
	}
	$linksOriginais = array(
		'facebook'		=> 'https://www.facebook.com/sharer/sharer.php?u=<%url%>',
		'twitter'		=> 'https://twitter.com/home?status=<%titulo%>%20-%20<%resumo%>%20<%url%>',
		'linkedin'		=> 'https://www.linkedin.com/shareArticle?mini=true&url=<%url%>&title=<%titulo%>',
		/*
		'google-plus'	=> 'https://plus.google.com/share?url=<%url%>',
		'whatsapp'		=> 'whatsapp://send?text=<%titulo%>%20<%url%>',
		'skype'			=> 'https://web.skype.com/share?url=<%url%>',
		'email'			=> 'mailto:email@su.amigo?subject=<%titulo%>&body=<%url%>',
		'telegram'		=> 'https://telegram.me/share/url?url=<%url%>&text=<%titulo%>',
		*/
	);
	$rotulos = array(
		'twitter'		=> 'Twitter',
		'facebook'		=> 'Facebook',
		'google-plus'	=> 'Google+',
		'linkedin'		=> 'LinkedIn',
		'whatsapp'		=> 'WhatsApp',
		'skype'			=> 'Skype',
		'telegram'		=> 'Telegram',
		'email'			=> 'E-mail',
	);
	$icones = array(
		'facebook'		=> 'facebook',
		'twitter'		=> 'twitter',
		'google-plus'	=> 'google-plus',
		'linkedin'		=> 'linkedin-square',
		'whatsapp'		=> 'whatsapp',
		'skype'			=> 'skype',
		'telegram'		=> 'location-arrow',
		'email'			=> 'envelope',
	);
	$protocolos = array( 'http', 'https', 'mailto', 'whatsapp' );
	$html = '';
	foreach ( $linksOriginais as $slug => $link ) {
		$link = str_replace( '<%titulo%>', rawurlencode( $tokens['titulo'] ), $link );
		$link = str_replace( '<%url%>', rawurlencode( $tokens['url'] ), $link );
		$link = str_replace( '<%resumo%>', rawurlencode( $tokens['resumo'] ), $link );
		$rot = $rotulos[ $slug ];
		$ico = $icones[ $slug ];
		$html .= "<li class='compartilhar_item'><a href='" . esc_url( $link, $protocolos ) . "' target='_blank' class='compartilhar_link' title='$rot'><span class='compartilhar_rotulo'>$rot</span><i class='fa fa-$ico compartilhar_icone'></i></a></li>\n";
	}
	if ( $imprimir )
		print $html;
	else
		return $html;
}

function fa( $icone, $attr = null ) {
	// @requer elemento
	$elAttr = array( 'class' => 'fa fa-' . $icone );
	if ( $attr && is_string( $attr ) )
		$elAttr['class'] .= ' ' . $attr;
	elseif ( is_array( $attr ) ) {
		if ( isset( $attr['class'] ) ) {
			$elAttr['class'] .= ' ' . $attr['class'];
			unset( $attr['class'] );
		}
		$elAttr += $attr;
	}
	return elemento( 'i', '', $elAttr, false );
}

function fa5( $icone, $classe = false, $estiloIcones = 'light' ) {
	return '<svg><use xlink:href="' . esc_url( TEMA . '/imagens/' . $estiloIcones . '.svg#' . $icone ) . '"' . ( $classe ? ' class="' . esc_attr( $classe ) . '"' : '' ) . '></use></svg>';
}

function valor( $chave, $arrAlt = null, $callback = null, $padrao = null, $imprimir = true ) {
	// Obtém uma chave do $_REQUEST ou de $arrAlt, executa o $callback e imprime o valor resultante dentro do atributo 'value'
	if ( isset( $_REQUEST[ $chave ] ) )
		$valor = $_REQUEST[ $chave ];
	elseif ( is_array( $arrAlt ) && isset( $arrAlt[ $chave ] ) )
		$valor = $arrAlt[ $chave ];
	elseif ( is_object( $arrAlt ) && isset( $arrAlt->$chave ) )
		$valor = $arrAlt->$chave;
	else
		$valor = $padrao;
	if ( is_callable( $callback ) )
		$valor = call_user_func( $callback, $valor );
	if ( $valor !== null && $valor !== false && $valor !== '' ) {
		if ( $imprimir )
			print 'value="' . htmlentities( $valor, ENT_QUOTES, 'UTF-8' ) . '"';
		else
			return $valor;
	}
}

function obterValor( $chave, $arrAlt = null, $callback = null, $padrao = null ) {
	// @alias valor
	return valor( $chave, $arrAlt, $callback, $padrao, false );
}

function checar( $chave, $arrAlt = null, $valorAlvo = 1, $padrao = null ) {
	// Obtém uma chave do $_REQUEST ou de $arrAlt, verifica se bate com o $valorAlvo e imprime 'checked'
	if ( isset( $_REQUEST[ $chave ] ) )
		$valor = $_REQUEST[ $chave ];
	elseif ( is_array( $arrAlt ) && isset( $arrAlt[ $chave ] ) )
		$valor = $arrAlt[ $chave ];
	else
		$valor = $padrao;
	if ( $valor == $valorAlvo )
		print 'checked';
}

function listaElementos( $tag, $lista, $opcoes = null, $attrGlobais = null, $imprimir = true ) {
	/* Cria um elemento de tipo $tag para cada item da $lista
	 * @param array_a $opcoes = (
	 *   attrChave = auto ('value' se $tag for 'option')
	 *     Nome do atributo que guardará a chave do item na array $lista
	 *   chaveComparacao => 'value'
	 *     Chave usada para comparar com o valor da opção 'selecionado'
	 *   selecionado => null
	 *     Valor para comparar com o atributo definido em chaveComparacao, gerando a flag 'selected' (se $tag for 'option') ou 'data-selected="true"'
	 *   chaveValor => auto ('value' se attrChave não for 'value')
	 *     Se os itens da $lista forem arrays, esse é o nome da chave para obter o valor do item
	 *   chaveConteudo => 'conteudo'
	 *     Se os itens da $lista forem arrays, esse é o nome da chave para obter o conteúdo do item
	 * )
	 * @requer elemento, vazio
	 */
	// Validação e definições
	if ( !is_array( $lista ) || !count( $lista ) )
		return false;
	$attrChave = $tag == 'option' ? 'value' : null;
	$chaveComparacao = 'value';
	$selecionado = null ;
	$chaveConteudo = 'conteudo';
	$chaveValor = null;
	if ( is_array( $opcoes ) )
		extract( $opcoes, EXTR_IF_EXISTS );
	if ( $attrChave != 'value' && $chaveValor === null )
		$chaveValor = 'value';
	$retorno = '';
	// Loop pelos itens
	foreach ( $lista as $chave => $item ) {
		// Conteúdo do item
		if ( is_array( $item ) ) {
			$attrItem =& $item;
			// Conteúdo
			if ( isset( $item[$chaveConteudo] ) ) {
				$conteudo = $item[$chaveConteudo];
				unset( $item[$chaveConteudo] );
			} else {
				$conteudo = null;
			}
			// Valor
			if ( $chaveValor && $chaveValor != 'value' ) {
				if ( isset( $item[$chaveValor] ) ) {
					$item['value'] = $item[$chaveValor];
					unset( $item[$chaveValor] );
				} else {
					$item['value'] = '';
				}
			}
		} else {
			$attrItem = array();
			$conteudo = $item;
		}
		// Chave
		if ( $attrChave )
			$attrItem[$attrChave] = $chave;
		// Atributos globais
		if ( is_array( $attrGlobais ) )
			$attrItem += $attrGlobais;
		// Seleção
		if ( isset( $attrItem[ $chaveComparacao ] ) && !vazio( $selecionado ) && $attrItem[ $chaveComparacao ] == $selecionado ) {
			if ( $tag == 'option' )
				$attrItem['selected'] = true;
			else
				$attrItem['data-selected'] = 'true';
		}
		// HTML
		$retorno .= elemento( $tag, $conteudo, $attrItem, $imprimir );
	}
	return $retorno;
}

function listaOptions( $lista, $selecionado = null, $opcoes = null, $imprimir = true ) {
	// @alias listaElementos
	if ( !is_array( $opcoes ) )
		$opcoes = array();
	return listaElementos( 'option', $lista, array( 'selecionado' => $selecionado ) + $opcoes, null, $imprimir );
}

function enfase( $str, $inverterEnfase = true ) {
	// Aplica <em> e <strong> numa $str utilizando sintaxe markdown
	// *palavra* = <em>palavra</em>
	// **palavra** = <strong>palavra</strong>
	// | = quebra de linha responsiva, presente somente em resoluções altas, aonde não há necessidade de refluir o texto
	// Se $inverterEnfase for true, alterna a sintaxe para <em> e <strong>
	$aberto = array( false, false );
	$tag = array(
		$inverterEnfase ? 'strong' : 'em',
		$inverterEnfase ? 'em' : 'strong'
	);
	if ( !is_string( $str ) || $str === '' )
		return '';
	$str = preg_replace( '#\*\*(.+?)\*\*#', "<$tag[1]>\$1</$tag[1]>", $str );
	$str = preg_replace( '#\*(.+?)\*#', "<$tag[0]>\$1</$tag[0]>", $str );
	$str = preg_replace( '#\|#', ' <br class="hidden-xs hidden-sm" />', $str );
	return $str;
}

function comOuSem( $var ) {
	return $var ? 'com' : 'sem';
}

function sanitizarAttr( $attr ) {
	if ( is_array( $attr ) )
		return $attr;
	elseif ( is_string( $attr ) ) {
		if ( substr( $attr, -1 ) == '"' )
			return explode( ' ', $attr );
		else
			return array( 'class' => $attr );
	}
	else
		return array();
}

function imgTema( $arquivo, $alt = 'Imagem', $attr = null, $imprimir = false ) {
	if ( !$arquivo || !is_string( $arquivo ) )
		return false;
	$attr = sanitizarAttr( $attr );
	$attr['src'] = TEMA . '/imagens/' . $arquivo;
	$attr['alt'] = $alt;
	return elemento( 'img', null, $attr, $imprimir );
}

function ofuscar( $email, $texto = null ) {

	$entmap = array( 0x0, 0xffff, 0, 0xffff );
	// $mailto = mb_encode_numericentity( 'mailto:', $entmap, 'UTF-8' );
	$mailto = 'mailto:';
	// $email = mb_encode_numericentity( $email, $entmap, 'UTF-8', true ); // Último parâmetro, is_hex, requer PHP 5.4.0
	$len = strlen( $email );
	$ofuscado_html = '';
	$ofuscado_js = '';

	for ( $c = 0; $c < $len; $c++ ) {
		$hex = base_convert( ord( $email{$c} ), 10, 16 );
		$ofuscado_html .= '&#x' . str_pad( $hex, 2, '0', STR_PAD_LEFT ) . ';';
		$ofuscado_js .= '\\u' . str_pad( $hex, 4, '0', STR_PAD_LEFT );
	}

	$uid = 'of' . base_convert( rand( 0x100000, 0xffffff ), 10, 16 );
	$href = $mailto . $ofuscado_js;

	if ( !$texto )
		$texto = $ofuscado_html;

	print "<a id='$uid' href='#' class='ofuscado'></a>";
	print "<script>\n";
	print "jQuery( function($) { $('#$uid').attr( 'href', '$href' ).html( '$texto' ); } );\n";
	print "</script>";

}

function quebraEstetica( $texto ) {
	$texto = preg_replace( '# (e|de|do|da|dos|das) #i', '<br class="hidden-xs hidden-sm" />$1 ', $texto );
	$texto = preg_replace( '# (&) #i', ' $1<br class="hidden-xs hidden-sm" />', $texto );
	return $texto;
}



// BD

function ajustarFusoHorario() {
	// Ajusta o fuso horário e configuração regional, para utilizar na função date() ou strftime()
	$current_offset = get_option('gmt_offset');
	$tzstring = get_option('timezone_string');
	if ( empty($tzstring) ) { // Create a UTC+- zone if no timezone string exists
		if ( 0 == $current_offset )
			$tzstring = 'UTC+0';
		elseif ($current_offset < 0)
			$tzstring = 'UTC' . $current_offset;
		else
			$tzstring = 'UTC+' . $current_offset;
	}
	date_default_timezone_set( $tzstring );
	setlocale( LC_ALL, get_locale() == 'en_US' ? array('en_US','en','english') : array('pt_BR','ptb','portuguese-brazilian','bra','brazil') ); // LC_TIME
	setlocale( LC_NUMERIC, array('en_US','en','english') );
	return $tzstring;
}

function arraySQL( $arr ) {
	/*
	 * Converte uma array para o formato SQL
	 * Suporta apenas os valores, sem chaves, e apenas contendo números ou strings
	 *
	 * @requer formatarSQL
	 */
	if ( !is_array( $arr ) || !count( $arr ) )
		return '()';
	$sql = '(';
	foreach ( $arr as $index => $val )
		$arr[$index] = formatarSQL( $val );
	$sql .= implode( ',', $arr );
	$sql .= ')';
	return $sql;
}

function formatarSQL( $valor, $achatarArrays = false ) {
	/*
	 * Converte valores para utilizar em SQL
	 *
	 * @requer arraySQL
	 */
	global $wpdb;
	if ( is_numeric( $valor ) )
		return (float) $valor;
	elseif ( is_string( $valor ) )
		return "'" . $wpdb->escape( $valor ) . "'";
	elseif ( is_null( $valor ) )
		return 'NULL';
	elseif ( is_bool( $valor ) )
		return intval( $valor );
	elseif ( is_array( $valor ) )
		return $achatarArrays
			? "'" . $wpdb->escape( json_encode( $valor ) ) . "'"
			: arraySQL( $valor )
		;
	elseif ( is_object( $valor ) )
		return "'" . $wpdb->escape( json_encode( $valor ) ) . "'";
	else
		return '';
}

function paresSQL( $campos, $separador = ', ' ) {
	// Retorna uma string relacionando os $campos
	// @requer formatarSQL
	$sql = array();
	if ( !is_array( $campos ) )
		return false;
	foreach ( $campos as $chave => $valor ) {
		if ( is_numeric( $chave ) )
			continue;
		$sql[] = $chave . ' = ' . formatarSQL( $valor );
	}
	return implode( $separador, $sql );
}

function igualdadeSQL( $campos, $negar = false, $and = true, $tabela = null ) {
	// Retorna uma string com condição de igualdade para os $campos
	// @requer formatarSQL
	$sql = array();
	if ( !is_array( $campos ) )
		return false;
	foreach ( $campos as $chave => $valor ) {
		if ( is_numeric( $chave ) )
			continue;
		if ( $tabela )
			$chave = $tabela . '.' . $chave;
		if ( is_string( $valor ) || is_numeric( $valor ) ) {
			if ( is_string( $valor ) && ( substr( $valor, 0, 1 ) == '%' || substr( $valor, -1, 1 ) == '%' ) )
				$sql[] = $chave . ( $negar ? ' NOT LIKE ' : ' LIKE ' ) . formatarSQL( $valor );
			else
				$sql[] = $chave . ( $negar ? ' <> ' : ' = ' ) . formatarSQL( $valor );
		} elseif ( is_array( $valor ) )
			$sql[] = $chave . ( $negar ? ' NOT IN ' : ' IN ' ) . formatarSQL( $valor );
		elseif ( is_null( $valor ) )
			$sql[] = ( $negar ? 'NOT ISNULL( ' : 'ISNULL( ' ) . $chave . ' )';
		elseif ( is_bool( $valor ) )
			$sql[] = ( $negar ? 'NOT ' : '' ) . $chave;
		else
			continue;
	}
	return implode( $and ? ' AND ' : ' OR ', $sql );
}

function insercaoSQL( $campos ) {
	// Retorna uma string para inserção dos $campos em uma tabela
	// @requer formatarSQL
	return '( ' . implode( ', ', array_keys( $campos ) ) . ' )'
		. ' VALUES ( ' . implode( ', ', array_map( 'formatarSQL', array_values( $campos ) ) ) . ' )'
	;
}

function tituloPost( $post_id ) {
	global $wpdb;
	return $wpdb->get_var(
		$wpdb->prepare( "SELECT post_title FROM $wpdb->posts WHERE post_status = 'publish' AND ID = %d LIMIT 1", $post_id )
	);
}

function hierarquiaCategoriasPost( $post_id = null ) {
	if ( !$post_id )
		$post_id = get_the_ID();
	if ( !$post_id || !is_int( $post_id ) )
		return false;
	$cats = get_the_category( $post_id );
	$retorno = array();
	if ( !$cats )
		return $retorno;
	// Organiza pelas IDs
	$ids = array();
	foreach ( $cats as $c ) {
		$ids[] = $c->term_id;
		$c->hierarquia = false;
	}
	$cats = array_combine( $ids, $cats );
	reset( $cats );
	// Define o nível
	$nivelMaisProfundo = 0;
	$catMaisProfunda = 0;
	$catsProcessadas = 0;
	$catsTotal = count( $cats );
	$catsInvisiveis = array(
		CAT_NULA,
		CAT_BLOG,
		CAT_EVENTOS,
	);
	$extrairNivel = function( $cat_id ) use ( &$cats, &$nivelMaisProfundo, &$catMaisProfunda, &$catsProcessadas, &$extrairNivel ) {
		$c =& $cats[ $cat_id ];
		if ( !$c ) {
			$cats[ $cat_id ] = $c = get_category( $cat_id );
			$c->hierarquia = false;
		}
		if ( $c->hierarquia !== false )
			return $c->hierarquia;
		$hierarquia = $c->parent
			? $extrairNivel( $c->parent ) + 1
			: 0
		;
		$c->hierarquia = $hierarquia;
		$catsProcessadas++;
		if ( !$catMaisProfunda || $hierarquia > $nivelMaisProfundo ) {
			$nivelMaisProfundo = $hierarquia;
			$catMaisProfunda = $cat_id;
		}
		return $hierarquia;
	};
	foreach ( $cats as $cat_id => $c ) {
		if ( $catsProcessadas >= $catsTotal )
			break;
		$extrairNivel( $cat_id );
	}
	// Puxa a categoria mais profunda e sobe na hierarquia
	$catAtual = $catMaisProfunda;
	while ( $catAtual ) {
		$c =& $cats[ $catAtual ];
		if ( !in_array( $c->term_id, $catsInvisiveis ) )
			$retorno[] =& $c;
		$catAtual = $c->parent;
	}
	// Fim
	$retorno = array_reverse( $retorno );
	return $retorno;
}

function termoRaiz( $termo ) {
	// Retorna o objeto do ancestral primário acima do $termo
	if ( is_int( $termo ) )
		$termo = get_term( $termo );
	if ( !is_object( $termo ) )
		return false;
	if ( $termo->parent )
		return termoRaiz( $termo->parent );
	return $termo;
}

function categoriaRaiz( $postAlvo = null ) {
	// Retorna o objeto da primeira categoria raíz daquela a qual pertence esse post
	// @requer termoRaiz
	global $post, $wpdb;
	if ( !$postAlvo )
		$postAlvo =& $post;
	if ( !( $postAlvo instanceof WP_Post ) )
		return false;
	$primeiraCat_id = (int) $wpdb->get_var("
		SELECT tt.term_id
		FROM $wpdb->term_relationships tr
		LEFT JOIN $wpdb->term_taxonomy tt ON tr.term_taxonomy_id = tt.term_taxonomy_id
		WHERE tr.object_id = $postAlvo->ID
		AND tt.taxonomy = 'category'
		LIMIT 1;
	");
	return termoRaiz( $primeiraCat_id );
}

function categoriaPrincipal( $post_id = null ) {
	// Retorna o objeto da categoria associada ao post que não seja uma das categorias-mãe
	global $post;
	if ( !$post_id )
		$post_id = $post->ID;
	$cats = get_the_category( $post_id );
	$catPrincipal = null;
	$catsSuperiores = array();
	foreach ( $cats as $c ) {
		if ( in_array( $c->term_id, $catsSuperiores ) )
			continue;
		$catPrincipal = $c;
		break;
	}
	return $catPrincipal;
}

function linksCategorias( $post_id = null, $separador = ', ' ) {
	// Retorna links das categorias associadas ao post que não sejam uma das categorias-mãe
	// @requer ancora
	global $post;
	if ( !$post_id )
		$post_id = $post->ID;
	$cats = get_the_category( $post_id );
	$catsSuperiores = array(
		CAT_NULA,
		CAT_BLOG,
		CAT_EVENTOS,
	);
	$catLinks = array();
	foreach ( $cats as $c ) {
		if ( in_array( $c->term_id, $catsSuperiores ) )
			continue;
		$catLinks[] = ancora( get_term_link( $c->term_id ), $c->name, 0, 'cat_link', false );
	}
	return implode( $separador, $catLinks );
}

function categoriasFilhas( $catMãe, $incluirLink = false ) {
	// Retorna todas as descendentes diretas de uma ou mais $catMãe, ordenadas alfabeticamente
	// @requer formatarSQL, ancora
	global $wpdb;
	$filhas = $wpdb->get_results("
		SELECT t.term_id, t.name
		FROM $wpdb->terms t
		LEFT JOIN $wpdb->term_taxonomy tt
			ON tt.term_id = t.term_id
		WHERE tt.taxonomy = 'category'
			AND tt.parent IN ".formatarSQL( (array) $catMãe )."
		ORDER BY t.name ASC
	", ARRAY_A );
	if ( $incluirLink ) {
		foreach ( $filhas as &$f ) {
			$f['permalink'] = get_term_link( (int) $f['term_id'], 'category' );
			$f['ancora'] = ancora( $f['permalink'], $f['name'], 0, array( 'class' => 'cat_link' ), false );
		}
	}
	return $filhas;
}

function listaCategoriasFilhas( $catMãe, $attrItem = null, $imprimir = true ) {
	// Imprime links das categorias filhas como lista
	// @requer listaElementos, categoriasFilhas
	global $wp_query;
	$attr = array( 'class' => 'cat_item' );
	if ( is_array( $attrItem ) && !empty( $attrItem ) )
		$attr = array_merge( $attr, $attrItem );
	listaElementos(
		'li',
		categoriasFilhas( $catMãe, true ),
		array(
			'attrChave'			=> null,
			'chaveConteudo'		=> 'ancora',
			'chaveComparacao'	=> 'term_id',
			'selecionado'		=> is_category() ? $wp_query->queried_object->term_id : false,
		),
		$attr,
		$imprimir
	);
}

function termRelationships( $object_id, $taxonomy = null ) {
	global $wpdb;
	if ( !is_int( $object_id ) )
		return false;
	$retorno = $wpdb->get_col("
		SELECT tt.term_id
		FROM $wpdb->term_relationships tr
		JOIN $wpdb->term_taxonomy tt
			ON tt.term_taxonomy_id = tr.term_taxonomy_id
		WHERE tr.object_id = $object_id
		" . ( $taxonomy ? ' AND tt.taxonomy = ' . formatarSQL( $taxonomy ) : '' ) . "
	");
	if ( !$retorno )
		return false;
	$retorno = array_map( 'intval', $retorno );
	return $retorno;
}



// Conexões

/**
* Send a POST requst using cURL
* @param string $url to request
* @param array $post values to send
* @param array $options for cURL
* @return string
*/
function curl_post($url, array $post = NULL, array $options = array())
{
    $defaults = array(
        CURLOPT_POST => 1,
        CURLOPT_HEADER => 0,
        CURLOPT_URL => $url,
        CURLOPT_FRESH_CONNECT => 1,
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_FORBID_REUSE => 1,
        CURLOPT_TIMEOUT => 4,
        CURLOPT_POSTFIELDS => http_build_query($post)
    );

    $ch = curl_init();
    curl_setopt_array($ch, ($options + $defaults));
    if( ! $result = curl_exec($ch))
    {
        trigger_error(curl_error($ch));
    }
    curl_close($ch);
	if ( is_string( $result ) ) {
		$firstChar = substr( $result, 0, 1 );
		if ( $firstChar == '<' )
			$result = @simplexml_load_string( $result, 'SimpleXMLElement', LIBXML_COMPACT );
		elseif ( $firstChar == '[' || $firstChar == '{' )
			$result = @json_decode( $result, true );
	}
    return $result;
}

/**
* Send a GET requst using cURL
* @param string $url to request
* @param array $get values to send
* @param array $options for cURL
* @return string
*/
function curl_get($url, array $get = NULL, array $options = array())
{
	// by David from Code2Design.com
	// http://php.net/manual/en/function.curl-exec.php#98628
    $defaults = array(
        CURLOPT_URL => $url. (strpos($url, '?') === FALSE ? '?' : ''). http_build_query($get),
        CURLOPT_HEADER => 0,
        CURLOPT_RETURNTRANSFER => TRUE,
        CURLOPT_TIMEOUT => 4
    );

    $ch = curl_init();
    curl_setopt_array($ch, ($options + $defaults));
    if( ! $result = curl_exec($ch))
    {
        trigger_error(curl_error($ch));
    }
    curl_close($ch);
	if ( is_string( $result ) ) {
		$firstChar = substr( $result, 0, 1 );
		if ( $firstChar == '<' )
			$result = @simplexml_load_string( $result, 'SimpleXMLElement', LIBXML_COMPACT );
		elseif ( $firstChar == '[' || $firstChar == '{' )
			$result = @json_decode( $result, true );
	}
    return $result;
}

function conBlog() {
	// Cria ou retorna uma instância do wpdb com a conexão MySQL para o blog
	// As constantes com as configurações estão no wp-config.php
	static $con = null;
	if ( $con )
		return $con;
	$con = new wpdb( BLOG_DB_USER, BLOG_DB_PASSWORD, BLOG_DB_NAME, BLOG_DB_HOST );
	if ( $con->error ) {
		error_log( implode( "\n\n", $con->error->get_error_messages() ) );
		die('Falha ao conectar com banco de dados do blog.');
		// return false;
	}
	$con->set_prefix( BLOG_DB_PREFIX );
	if ( LOCAL || DEBUG_BD )
		$con->show_errors();
	register_shutdown_function( array( $con, 'close' ) );
	return $con;
}

/*
function getPostsBlog( $args = null ) {
	// Obtém posts do blog via query direta ao BD
	// Parâmetros
	$padroes = array(
		'post_type'			=> 'post',
		'post_status'		=> 'publish',
		'category'			=> null,
		'order'				=> 'date',
		'orderby'			=> 'DESC',
		'posts_per_page'	=> (int) get_option('posts_per_page'),
		'paged'				=> 1,
		'post__in'			=> null,
		'post__not_in'		=> null,
	);
	extract( $padroes );
	if ( is_array( $args ) )
		extract( $args, EXTR_IF_EXISTS );
	// Montagem da query
	$con = conBlog();
	// Tabelas
	$sql = "
		SELECT
			p.ID,
			p.post_author,
			p.post_date,
			p.post_content,
			p.post_title,
			p.post_excerpt,
			p.post_status,
			p.post_name,
			p.post_modified,
			p.post_parent,
			p.post_type,
			p.comment_count
		FROM $con->posts p
	";
	if ( $category )
		$sql .= " JOIN $con->term_relationships tr ON tr.object_id = p.ID";
	// Condições
	$sql .= "\nWHERE";
	$condicoes = array();
	$negacoes = array();
	if ( $post__in )
		$condicoes['p.ID'] = $post__in;
	if ( $post_type )
		$condicoes['p.post_type'] = $post_type;
	if ( $post_status )
		$condicoes['p.post_status'] = $post_status;
	if ( $category )
		$condicoes['tr.term_taxonomy_id'] = $category;
	if ( $post__not_in )
		$negacoes['p.ID'] = $post__not_in;
	$sql .= ' ' . igualdadeSQL( $condicoes );
	$sql .= ' ' . igualdadeSQL( $negacoes, true );
	// Ordem
	switch ( $order ) {
		case 'name' :
		case 'title' :
			$order = 'p.post_title';
		break;
		case 'slug' :
			$order = 'p.post_name';
		break;
		case 'date' :
			$order = 'p.post_date';
		break;
		case 'menu_order' :
			$order = 'p.menu_order';
		break;
		default :
			$order = null;
	}
	if ( $orderby && $orderby != 'ASC' && $orderby != 'DESC' )
		$orderby = null;
	if ( $order && $orderby )
		$sql .= " ORDER BY $order $orderby";
	// Paginação
	if ( is_int( $posts_per_page ) && $posts_per_page > 0 ) {
		if ( !is_int( $paged ) || $paged <= 0 )
			$paged = 1;
		$inicio = $posts_per_page * ( $paged - 1 );
		$fim = $posts_per_page;
		$sql .= " LIMIT $inicio, $fim";
	}
	// Envio da query e criação de objetos WP_Post
	$resultados = $con->get_results( $sql );
	if ( !$resultados )
		return false;
	$total = count( $resultados );
	for ( $i = 0; $i < $total; $i++ )
		$resultados[$i] = new WP_Post( $resultados[$i] );
	// Retorno
	return $resultados;
}
*/

function getPostsBlog( $args = null ) {
	// Obtém posts do blog via API REST do WP
	// @requer getMediaBlog, getCategoriesBlog, getTagsBlog, obter, curl_get
	static $url = null;
	if ( !$url )
		$url = rtrim( get_field( 'url', PAGINA_BLOG ), '/' ) . '/wp-json/wp/v2/posts/';
	$query = array();
	if ( !is_array( $args ) )
		$args = array();
	// Params
	if ( isset( $args['page'] ) )
		$query['page'] = $args['page'];
	elseif ( isset( $args['paged'] ) )
		$query['page'] = $args['paged'];
	if ( isset( $args['posts_per_page'] ) )
		$query['per_page'] = $args['posts_per_page'];
	if ( isset( $args['s'] ) )
		$query['search'] = $args['s'];
	if ( isset( $args['post__not_in'] ) )
		$query['exclude'] = is_array( $args['post__not_in'] ) ? implode( ',', $args['post__not_in'] ) : $args['post__not_in'];
	if ( isset( $args['post__in'] ) )
		$query['include'] = is_array( $args['post__in'] ) ? implode( ',', $args['post__in'] ) : $args['post__in'];
	if ( isset( $args['offset'] ) )
		$query['offset'] = $args['offset'];
	if ( isset( $args['order'] ) )
		$query['order'] = $args['order'];
	if ( isset( $args['orderby'] ) )
		$query['orderby'] = $args['orderby'];
	if ( isset( $args['post_status'] ) )
		$query['status'] = is_array( $args['post_status'] ) ? implode( ',', $args['post_status'] ) : $args['post_status'];
	if ( isset( $args['category'] ) )
		$query['categories'] = is_array( $args['category'] ) ? implode( ',', $args['category'] ) : $args['category'];
	if ( isset( $args['tag'] ) )
		$query['tags'] = is_array( $args['tag'] ) ? implode( ',', $args['tag'] ) : $args['tag'];
	if ( isset( $args['sticky'] ) )
		$query['sticky'] = 1;
	// Requisição
	$retorno = curl_get( $url, $query );
	if ( !$retorno )
		return false;
	// Processamento dos dados para adequar ao formato de posts padrão
	$listaPosts = array();
	foreach ( $retorno as $p ) {
		$postDados = array(
			'ID'				=> $p['id'],
			'post_date'			=> $p['date'],
			'post_date_gmt'		=> $p['date_gmt'],
			'guid'				=> $p['guid']['rendered'],
			'post_modified'		=> $p['modified'],
			'post_modified_gmt'	=> $p['modified_gmt'],
			'post_name'			=> $p['slug'],
			'post_status'		=> $p['status'],
			'post_type'			=> $p['type'],
			'post_title'		=> $p['title']['rendered'],
			'post_content'		=> $p['content']['rendered'],
			'post_excerpt'		=> $p['excerpt'] ? $p['excerpt']['rendered'] : '',
			'post_author'		=> $p['author'],
			// Não padrão
			'permalink'			=> $p['link'],
			'thumb_id'			=> $p['featured_media'],
			'thumb'				=> null,
			'sticky'			=> $p['sticky'],
			'tag_ids'			=> $p['tags'],
			'tags'				=> null,
			'category_ids'		=> $p['categories'],
			'categories'		=> null,
		);
		if ( $p['featured_media'] ) {
			// Obtém dados do thumbnail
			$postDados['thumb'] = obter( 0, getMediaBlog( $p['featured_media'] ) );
		}
		if ( $p['categories'] ) {
			// Obtém dados do thumbnail
			$postDados['categories'] = getCategoriesBlog( $p['categories'] );
		}
		if ( $p['tags'] ) {
			// Obtém dados do thumbnail
			$postDados['tags'] = getTagsBlog( $p['tags'] );
		}
		$listaPosts[] = new WP_Post( (object) $postDados );
	}
	// Fim
	return $listaPosts;
}

function getMediaBlog( $ids, $tamanho = 'full' ) {
	static $cache = array();
	static $url = null;
	if ( !$url )
		$url = rtrim( get_field( 'url', PAGINA_BLOG ), '/' ) . '/wp-json/wp/v2/media/';
	if ( !is_array( $ids ) )
		$ids = array( $ids );
	$retorno = array();
	foreach ( $ids as $id ) {
		if ( isset( $cache[$id] ) ) {
			$retorno[] =& $cache[$id];
			continue;
		}
		$res = @curl_get( $url . $id );
		if ( !$res )
			continue;
		$thumb = $tamanho && isset( $res['media_details']['sizes'][ $tamanho ] )
			? $res['media_details']['sizes'][ $tamanho ]['source_url']
			: $res['media_details']['sizes']
		;
		$cache[$id] = $thumb;
		$retorno[] = $thumb;
	}
	return count( $retorno ) ? $retorno : false;
}

function getCategoriesBlog( $ids ) {
	static $cache = array();
	static $url = null;
	if ( !$url )
		$url = rtrim( get_field( 'url', PAGINA_BLOG ), '/' ) . '/wp-json/wp/v2/categories/';
	if ( !is_array( $ids ) )
		$ids = array( $ids );
	$retorno = array();
	foreach ( $ids as $id ) {
		if ( isset( $cache[$id] ) ) {
			$retorno[] =& $cache[$id];
			continue;
		}
		$res = @curl_get( $url . $id );
		if ( !$res )
			continue;
		$cat = array(
			'term_id'		=> $res['id'],
			'name'			=> $res['name'],
			'link'			=> $res['link'],
			'parent'		=> $res['parent'],
		);
		$cache[$id] = $cat;
		$retorno[] = $cat;
	}
	return count( $retorno ) ? $retorno : false;
}

function getTagsBlog( $ids ) {
	static $cache = array();
	static $url = null;
	if ( !$url )
		$url = rtrim( get_field( 'url', PAGINA_BLOG ), '/' ) . '/wp-json/wp/v2/tags/';
	if ( !is_array( $ids ) )
		$ids = array( $ids );
	$retorno = array();
	foreach ( $ids as $id ) {
		if ( isset( $cache[$id] ) ) {
			$retorno[] =& $cache[$id];
			continue;
		}
		$res = @curl_get( $url . $id );
		if ( !$res )
			continue;
		$tag = array(
			'term_id'		=> $res['id'],
			'name'			=> $res['name'],
			'link'			=> $res['link'],
			'parent'		=> $res['parent'],
		);
		$cache[$id] = $tag;
		$retorno[] = $tag;
	}
	return count( $retorno ) ? $retorno : false;
}



// Extensões

function cf7_adicionarReferrer( $dados ) {
	if ( isset( $_SERVER['HTTP_REFERER'] ) )
		$dados['referrer'] = $_SERVER['HTTP_REFERER'];
	elseif ( isset( $_POST['referrer'] ) )
		$dados['referrer'] = $_POST['referrer'];
	return $dados;
}

add_filter( 'wpcf7_posted_data', 'cf7_adicionarReferrer' );

function cf7_adicionarShortcodeReferrer() {
	if ( !function_exists('wpcf7_add_shortcode') )
		return;
	wpcf7_add_shortcode( 'referrer', 'cf7_processarShortcodeReferrer', false );
}

add_action( 'plugins_loaded', 'cf7_adicionarShortcodeReferrer' );

function cf7_processarShortcodeReferrer( $params ) {
	dump('cf7_processarShortcodeReferrer');
	dump( $params );
}

function cf7_adicionarCampoReferrer( $hiddenInputs ) {
	// @requer urlAtual
	$hiddenInputs['referrer'] = urlAtual();
}

add_filter( 'wpcf7_form_hidden_fields', 'cf7_adicionarCampoReferrer' );

function acf_gmapsApiKey( $api ) {
	$api['key'] = GMAPS_API_KEY;
	return $api;
}

add_filter( 'acf/fields/google_map/api', 'acf_gmapsApiKey' );

function sc_img( $params, $conteudo = null ) {
	/*
	 * Shortcode para puxar uma imagem
	 * Aceita ID da imagem (id) ou nome do arquivo (src)
	 *
	 * @param (int) id
	 * @param (string) src
	 * @param (string) size = 'full'
	 * @param (string) class = 'sc_img'
	 * @param (string) alt = (padrão da imagem)
	 *
	 * @requer obterImg, obter
	 */
	global $wpdb;
	if ( ( !isset( $params['id'] ) || !is_numeric( $params['id'] ) ) && !isset( $params['src'] ) )
		return '';
	if ( isset( $params['src'] ) ) {
		$params['id'] = $wpdb->get_var( $wpdb->prepare("
			SELECT ID
			FROM $wpdb->posts
			WHERE guid LIKE %s
			LIMIT 1
		", '%' . $params['src'] ) );
		if ( !$params['id'] )
			return '';
	}
	$attr = array(
		'class'		=> obter( 'class', $params, 'sc_img' ),
	);
	if ( isset( $params['alt'] ) )
		$attr['alt'] = $params['alt'];
	return obterImg( (int) $params['id'], obter( 'size', $params, 'full' ), $attr );
}

add_shortcode( 'img', 'sc_img' );



// WP Query

function acao_alterarQueryPrincipal($query) {
	// @requer umDe, simpleSessionGet, categoriasFilhas, array_column
	global $opcoesOrdenacao, $modulo_posts_puxados;
	if ( !$query->is_main_query() || is_admin() )
		return;
	$meta = umDe( $query->get('meta_query'), array( 'relation' => 'AND' ) );
	// Busca
	if ( $query->is_search ) {
		$query->set( 'post_type', array( 'post', 'page' ) );
	}
	// Meta
	if ( isset( $meta[0] ) )
		$query->set( 'meta_query', $meta );
}

add_action('pre_get_posts','acao_alterarQueryPrincipal');



// AJAX

function add_custom_endpoints() {
	global $wp_rewrite;
    add_rewrite_endpoint( 'ajax', EP_PERMALINK | EP_PAGES );
    add_rewrite_endpoint( 'id', EP_PERMALINK | EP_PAGES );
	$wp_rewrite->flush_rules();
}

add_action( 'init', 'add_custom_endpoints' );

function ajax_template_redirect() {

    global $wp_query, $post, $wpdb;

    // if this is not a request for ajax or a singular object then bail
    if ( !is_singular() || !isset( $wp_query->query_vars['ajax'] ) )
        return;

	$slug = $wp_query->query_vars['pagename'];
	$file = TEMA . "/ajax-$slug.php";

    // include custom template
	if ( file_exists( $file ) ) {
		the_post();
		require $file;
		exit;
	}

}

add_action( 'template_redirect', 'ajax_template_redirect' );



// Mensagens
// @requer plugin simple-session-support

if ( !function_exists('simpleSessionGet') ) {
	// Shim com armazenamento runtime em variável estática
	function simpleSessionGetSet( $var, $padrao = null, $set = false, $valor = null ) {
		static $session = array();
		if ( $set ) {
			$session[$var] = $valor;
			return $valor;
		} else {
			if ( !isset( $session[$var] ) )
				return $padrao;
			return $session[$var];
		}
	}
	function simpleSessionGet( $var, $padrao = null ) {
		return simpleSessionGetSet( $var, $padrao );
	}
	function simpleSessionSet( $var, $valor = null ) {
		return simpleSessionGetSet( $var, null, true, $valor );
	}
	/*
	// Polyfill com armazenamento persistente em sessão
	function simpleSessionGet( $var, $padrao = null ) {
		if ( !$_SESSION )
			return $padrao;
		return array_key_exists( $var, $_SESSION )
			? $_SESSION[ $var ]
			: $padrao
		;
	}
	function simpleSessionSet( $var, $valor = null ) {
		if ( !$_SESSION )
			return false;
		$_SESSION[ $var ] = $valor;
		return $valor;
	}
	*/
}

function iniciarSessao() {
	if ( !headers_sent() ) { //  && !session_id()
		session_start();
	}
}

function fecharSessao() {
	session_destroy();
}

if ( FALSE&&LOCAL ) :
	add_action( 'init', 'iniciarSessao', 1 );
	add_action( 'wp_logout', 'fecharSessao' );
endif;

function guardarMensagem( $tipo, $text ) {

	// Stores a message
	// If additional arguments are supplied, calls sprintf on the message with the extra arguments
	// @requer plugin simple-session-support

	$mensagens = simpleSessionGet( 'mensagens', array() );
	$argCount = func_num_args();

	if ( is_bool( $text ) )
		$text = $text ? 'TRUE' : 'FALSE';
	elseif ( is_null( $text ) )
		$text = 'NULL';
	elseif ( is_array( $text ) && !count( $text ) )
		$text = 'ARRAY()';
	elseif ( !is_scalar( $text ) )
		$text = print_r( $text, true );

	if ( $argCount > 2 ) {
		$args = func_get_args();
		array_shift( $args );
		$args[0] = $text;
		$text = call_user_func_array( 'sprintf', $args );
	}

	if ( !isset( $mensagens[$tipo] ) )
		$mensagens[$tipo] = array();

	$mensagens[$tipo][] = $text;
	simpleSessionSet( 'mensagens', $mensagens );

	return new Exception( $text );

}

function info( $text ) {

	// Stores a message and sets its status to info

	$args = array_merge( array('info'), func_get_args() );

	return call_user_func_array( 'guardarMensagem', $args );

}

function erro( $text ) {

	// Stores a message and sets its status to error

	$args = array_merge( array('erro'), func_get_args() );

	return call_user_func_array( 'guardarMensagem', $args );

}

function sucesso( $text ) {

	// Stores a message and sets its status to error

	$args = array_merge( array('sucesso'), func_get_args() );

	return call_user_func_array( 'guardarMensagem', $args );

}

function contagemMensagens( $tipo = null ) {

	$mensagens = simpleSessionGet( 'mensagens', array() );

	$contagem = 0;

	if ( !$tipo ) {
		foreach ( $mensagens as $status => $conteudo )
			$contagem += count( $conteudo );
	} else {
		$contagem = isset( $mensagens[$tipo] )
			? count( $mensagens[$tipo] )
			: 0
		;
	}

	return $contagem;

}

function checarErros() {
	if ( contagemMensagens('erro') )
		throw new Exception();
}

function mostrarMensagens( $limpar = true ) {
	// Imprime as mensagens presentes na sessão
	// @requer imprimirMensagens
	imprimirMensagens( simpleSessionGet( 'mensagens', array() ) );
	if ( $limpar )
		simpleSessionSet( 'mensagens', array() );
}

function imprimirMensagens( $lista, $tipo = null ) {
	// Imprime as mensagens dadas na $lista
	// Aceita três modos:
	// + $lista = array( tipo => (array_n) mensagens )
	//   Utiliza as chaves na primeira dimensão da array para determinar o $tipo
	// + $lista = (array_n) mensagens && $tipo != null
	//   Imprime as mensagens como sendo do $tipo dado
	// + $lista = (string) mensagem && $tipo != null
	//   Imprime uma única mensagem como sendo do $tipo dado
	if ( !is_array( $lista ) ) {
		if ( !$tipo || !is_string( $tipo ) )
			return false;
		$lista = array( $tipo => array( $lista ) );
	} elseif ( $tipo ) {
		$lista = array( $tipo => $lista );
	}
	if ( empty( $lista ) )
		return false;
	print '<div class="mensagens">'; // <div class="container">
	foreach ( $lista as $status => $conteudo ) {
		if ( $conteudo )
			print '<ul class="mensagens_lista '.$status.'"><li>' . nl2br( implode( "</li><li>", $conteudo ) ) . '</li></ul>';
	}
	print '</div>'; // </div>
	return true;
}

function mensagensAdmin( $limpar = true ) {

	$mensagens = simpleSessionGet( 'mensagens', array() );

	$classesStatus = array(
		'erro'		=> 'error',
		'info'		=> 'info',
		'sucesso'	=> 'updated',
	);

	foreach ( $mensagens as $status => $conteudo ) {
		if ( $conteudo ) {
			print '<div class="notice '.$classesStatus[$status].'"><p>' . nl2br( implode( "</p><p>", $conteudo ) ) . '</p></div>';
		}
	}

	if ( $limpar )
		simpleSessionSet( 'mensagens', array() );

}

function retornarAjax( $dadosAdicionais = null ) {
	$retorno = array(
		'status'		=> 1,
		'mensagens'		=> simpleSessionGet( 'mensagens', array() ),
	);
	if ( isset( $retorno['mensagens']['erro'] ) && $retorno['mensagens']['erro'] )
		$retorno['status'] = 0;
	if ( isset( $_SESSION['redirecionar'] ) && $_SESSION['redirecionar'] ) {
		$retorno['redirecionar'] = $_SESSION['redirecionar'];
		unset( $_SESSION['redirecionar'] );
	}
	if ( is_array( $dadosAdicionais ) && $dadosAdicionais )
		$retorno += $dadosAdicionais;
	foreach ( $retorno['mensagens'] as $tipo => &$msg ) {
		$msg = array_map( 'nl2br', $msg );
	}
	print json_encode( $retorno );
	simpleSessionSet( 'mensagens', array() );
	exit;
}

function redir( $redir = null ) {
	if ( is_int( $redir ) )
		$redir = get_the_permalink( $redir );
	elseif ( !$redir )
		$redir = site_url();
	wp_redirect( $redir, 302 );
	exit;
}

function redirecionarAjax( $redir = null ) {
	// Configura um redirecionamento a ser enviado via Ajax
	if ( is_int( $redir ) )
		$redir = get_the_permalink( $redir );
	// A função retornarAjax irá pegar a variável da sessão
	$_SESSION['redirecionar'] = $redir;
}



// Debugging

function toString( $var ) {
	// Converte valores em uma string
	if ( is_string( $var ) )
		return $var === ''
			? '""'
			: $var
		;
	if ( is_numeric( $var ) )
		return (string) $var;
	if ( is_array( $var ) )
		// return json_encode( $var );
		return print_r( $var, true );
	if ( is_object( $var ) )
		// return get_class( $var );
		return print_r( $var, true );
	if ( is_null( $var ) )
		return 'NULL';
	if ( is_bool( $var ) )
		return $var
			? 'TRUE'
			: 'FALSE'
		;
	return gettype( $var );
}

function dump( $valor ) {
	// @requer toString
	if ( !isset( $_SESSION['dump'] ) || !is_array( $_SESSION['dump'] ) )
		$_SESSION['dump'] = array();
	if ( func_num_args() > 1 ) {
		$args = func_get_args();
		$args = array_map( 'toString', $args );
		$valor = implode( "\t|\t", $args );
	}
	$_SESSION['dump'][] = toString( $valor );
}

function flushDump( $flushAndClean = true ) {
	if ( !isset( $_SESSION ) || !isset( $_SESSION['dump'] ) || !is_array( $_SESSION['dump'] ) || !count( $_SESSION['dump'] ) )
		return;
	print '<div class="container"><pre dump>';
	print htmlentities( implode( PHP_EOL . PHP_EOL, $_SESSION['dump'] ), ENT_QUOTES, 'UTF-8' );
	print '</pre></div>';
	if ( $flushAndClean )
		unset( $_SESSION['dump'] );
}

function logar( $valor, $log = 'debug' ) {
	// Grava num arquivo de log com data do dia o $valor convertido para string
	// @requer toString
	if ( !is_string( $log ) )
		return false;
	$fh = @fopen( ABSPATH . '/wp-content/logs/' . date('Y-m-d') . '_' . $log . '.log', 'w' );
	if ( !$fh )
		return false;
	fwrite( $fh, '====== ' . date('c') . ' ======' . "\n\n" );
	fwrite( $fh, toString( $valor ) . "\n\n" );
	fclose( $fh );
	return true;
}
