<?php
define( 'TEMA', get_template_directory_uri() );
define( 'COMUM', 'http://localhost/comum' );
define( 'MAIS', '<abbr title="Mais">+</abbr> ' );

define( 'DEBUG_BD', false );

define( 'PAGINA_HOME', 2 );
define( 'PAGINA_BLOG', 251 );
define( 'PAGINA_CONTATO', 260 );
define( 'THUMB_PADRAO', 289 );
define( 'FUNDO_PADRAO', 40 );
define( 'IMG_LOGO', 72 );
define( 'CAT_NULA', 1 );
define( 'CAT_BLOG', 13 );
define( 'CAT_EVENTOS', 19 );

define( 'GMAPS_API_KEY', 'AIzaSyALjpIZeAqoC8AD4uVplHRW4Xpm0TZPh80' ); // MS

global $tema;
$tema = TEMA;

/**
 * MS functions and definitions
 *
 * @package MS
 */

if ( ! function_exists( 'ms_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function ms_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on MS, use a find and replace
	 * to change 'ms' to the name of your theme in all the template files
	 */
	// load_theme_textdomain( 'ms', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'principal'		=> 'Menu Principal',
		'social'		=> 'Redes Sociais',
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 * See http://codex.wordpress.org/Post_Formats
	 */
	/*
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
	) );
	*/

	// Set up the WordPress core custom background feature.
	/*
	add_theme_support( 'custom-background', apply_filters( 'ms_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
	*/

	// Adiciona outros tamanhos de imagem
	// add_image_size( 'listagem', 400, 200, true );

	// Habilita o arquivo editor-style.css para ser usado na edição de posts
	add_editor_style();
    // $font_url = 'https://maxcdn.bootstrapcdn.com/font-awesome/4.6.0/css/font-awesome.min.css';
    // add_editor_style( $font_url );
    // $font_url = '//fonts.googleapis.com/css?family=' . rawurlencode('Lato:300,400,700,900|Quicksand:400|Playfair Display:400');
	// $font_url = get_template_directory_uri() . '/fonts.css';

	ajustarFusoHorario();

}
endif; // ms_setup
add_action( 'after_setup_theme', 'ms_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function ms_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'ms_content_width', 1170 );
}
add_action( 'after_setup_theme', 'ms_content_width', 0 );

/**
 * Register widget area.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
 */
function ms_widgets_init() {
	register_sidebar( array(
		'name'          => 'Cabeçalho',
		'id'            => 'cabecalho',
		'description'   => 'Insira um widget ACF configurado para Cabeçalho.',
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '<span class="hidden">',
		'after_title'   => '</span>',
	) );
	register_sidebar( array(
		'name'          => 'Rodapé',
		'id'            => 'rodape',
		'description'   => 'Insira um widget ACF configurado para Rodapé.',
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '<span class="hidden">',
		'after_title'   => '</span>',
	) );
	register_sidebar( array(
		'name'          => 'Rodapé - Scripts',
		'id'            => 'rodape_scripts',
		'description'   => 'Insira um widget de "texto normal ou HTML" com os scripts que deseja inserir no final da página.',
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '<span class="hidden">',
		'after_title'   => '</span>',
	) );
}
add_action( 'widgets_init', 'ms_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function ms_scripts() {

	$comum = COMUM;
	$tema = TEMA;

	// CSS
	wp_enqueue_style( 'google-fonts', ( LOCAL ? "$tema/google-fonts.css" : 'https://fonts.googleapis.com/css?family=Dosis:300,400,700|Shadows+Into+Light' ) );
	wp_enqueue_style( 'fontes', "$tema/fontes.css" );
	wp_enqueue_style( 'font-awesome', ( LOCAL ? "$comum/css/font-awesome.min.css" : 'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css' ) );
	wp_enqueue_style( 'bootstrap', ( LOCAL ? "$comum/css/bootstrap.min.css" : 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css' ) );
	wp_enqueue_style( 'animate', ( LOCAL ? "$comum/css/animate.min.css" : 'https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css' ) );
	wp_enqueue_style( 'ms-style', get_stylesheet_uri(), array(), 1.7 );
	wp_enqueue_style( 'ms-style-leo', get_template_directory_uri().'/style-leo.css',array(), 1.4 );
	wp_enqueue_style( 'ms-style-edu', get_template_directory_uri().'/style-edu.css', array(), 1.9 );
	wp_enqueue_style( 'ms-style-will', get_template_directory_uri().'/style-will.css', array(), 1.8 );
	// JS
	// wp_enqueue_script( 'jquery.mobile', ( LOCAL ? "$comum/js/jquery.mobile.min.js" : 'https://cdnjs.cloudflare.com/ajax/libs/jquery-mobile/1.4.5/jquery.mobile.min.js' ), array('jquery'), '1.4.5', true );
	wp_enqueue_script( 'bootstrap', ( LOCAL ? "$comum/js/bootstrap.min.js" : 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js' ), array('jquery'), '3.3.5', true );
	wp_enqueue_script( 'jquery.touchswipe', LOCAL ? "$comum/js/jquery.touchSwipe.min.js" : 'https://cdnjs.cloudflare.com/ajax/libs/jquery.touchswipe/1.6.18/jquery.touchSwipe.min.js', array('jquery'), '1.6.18', true );
	wp_enqueue_script( 'googlemaps-scrollprevent', LOCAL ? "$comum/js/googlemaps-scrollprevent.js" : 'https://cdn.rawgit.com/diazemiliano/googlemaps-scrollprevent/master/dist/googlemaps-scrollprevent.js', array(), 'latest', true );
	wp_enqueue_script( 'jquery.waypoints', LOCAL ? "$comum/js/waypoints.min.js" : 'https://cdnjs.cloudflare.com/ajax/libs/waypoints/4.0.1/jquery.waypoints.min.js', array('jquery'), '4.0.1', true );
	wp_enqueue_script( 'jquery.smooth-scroll', ( LOCAL ? "$comum/js/jquery.smooth-scroll.min.js" : 'https://cdnjs.cloudflare.com/ajax/libs/jquery-smooth-scroll/1.7.2/jquery.smooth-scroll.min.js' ), array('jquery'), '1.7.2', true );
	// wp_enqueue_script( 'jquery.form', "$tema/js/jquery.form.min.js", array('jquery'), '3.51.0', true );
	wp_enqueue_script( 'jquery.conterup', "$tema/js/jquery.counterup.min.js", array(), '1', true );
	wp_enqueue_script( 'jquery.waypoints', "$tema/js/waypoints.min.js", array(), '1', true );
	// wp_enqueue_script( 'fontawesome5.solid', "$tema/js/solid.min.js", array(), '5rc2', true );
	// wp_enqueue_script( 'fontawesome5', "$tema/js/fontawesome.min.js", array('fontawesome5.solid', 'fontawesome5.light'), '5rc2', true );
	wp_enqueue_script( 'custom', "$tema/js/ms.js", array('jquery', 'jquery.touchswipe', 'jquery.waypoints', 'jquery.smooth-scroll', 'googlemaps-scrollprevent'), 1.5, true );

	// Registra recursos para inclusão seletiva

	// Forms

	// wp_register_script( 'form', "$tema/js/form.js", array('jquery', 'custom'), 1, true );
	// wp_register_script( 'input', "$tema/js/input.js", array('jquery', 'custom'), 1, true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	// Home

	wp_register_style( 'jquery.owlcarousel', ( LOCAL ? "$comum/css/owl.carousel.min.css" : 'https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/assets/owl.carousel.min.css' ) );
	wp_register_style( 'jquery.owlcarousel-default', ( LOCAL ? "$comum/css/owl.theme.default.min.css" : 'https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/assets/owl.theme.default.min.css' ) );
	wp_register_script( 'jquery.owlcarousel', ( LOCAL ? "$comum/js/owl.carousel.min.js" : 'https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/owl.carousel.min.js' ), array('jquery'), '2.2.1', true );

	// Adiciona em todas as páginas, para utilização no rodapé

	wp_enqueue_style('jquery.owlcarousel');
	wp_enqueue_style('jquery.owlcarousel-default');
	wp_enqueue_script('jquery.owlcarousel');

	// Telas

	wp_register_script( 'telas', "$tema/js/telas.js", array('jquery'), '1', true );

}

add_action( 'wp_enqueue_scripts', 'ms_scripts' );

function ms_acf() {

	if(function_exists("register_field_group"))
	{
		register_field_group(array (
			'id' => 'acf_duas-colunas',
			'title' => 'Duas Colunas',
			'fields' => array (
				array (
					'key' => 'field_59dba4fcf5fca',
					'label' => 'Linhas',
					'name' => 'linhas',
					'type' => 'repeater',
					'required' => 1,
					'sub_fields' => array (
						array (
							'key' => 'field_59dba50ef5fcb',
							'label' => 'Coluna #1',
							'name' => 'coluna1',
							'type' => 'wysiwyg',
							'column_width' => 50,
							'default_value' => '',
							'toolbar' => 'full',
							'media_upload' => 'yes',
						),
						array (
							'key' => 'field_59dba53cf5fcc',
							'label' => 'Coluna #2',
							'name' => 'coluna2',
							'type' => 'wysiwyg',
							'column_width' => 50,
							'default_value' => '',
							'toolbar' => 'full',
							'media_upload' => 'yes',
						),
					),
					'row_min' => 1,
					'row_limit' => '',
					'layout' => 'table',
					'button_label' => 'Adicionar linha',
				),
			),
			'location' => array (
				array (
					array (
						'param' => 'page_template',
						'operator' => '==',
						'value' => 'template-duas-colunas.php',
						'order_no' => 0,
						'group_no' => 0,
					),
				),
			),
			'options' => array (
				'position' => 'normal',
				'layout' => 'no_box',
				'hide_on_screen' => array (
					0 => 'the_content',
				),
			),
			'menu_order' => 0,
		));
		register_field_group(array (
			'id' => 'acf_uso-interno',
			'title' => 'Uso Interno',
			'fields' => array (
				array (
					'key' => 'field_59dba66a0a815',
					'label' => '',
					'name' => '',
					'type' => 'message',
					'message' => '<span style="color:red">Esta página tem função especial no sistema. Tome cuidado ao alterá-la.</span>',
				),
			),
			'location' => array (
				array (
					array (
						'param' => 'page_template',
						'operator' => '==',
						'value' => 'template-uso-interno.php',
						'order_no' => 0,
						'group_no' => 0,
					),
				),
			),
			'options' => array (
				'position' => 'acf_after_title',
				'layout' => 'no_box',
				'hide_on_screen' => array (
				),
			),
			'menu_order' => 0,
		));
	}

}

add_action( 'init', 'ms_acf' );

/*
function form_enqueue() {
	wp_enqueue_script('input');
	wp_enqueue_script('form');
}

function home_enqueue() {
	wp_enqueue_style('jquery.owlcarousel');
	wp_enqueue_style('jquery.owlcarousel-default');
	wp_enqueue_script('jquery.owlcarousel');
}
*/

function enqueue_mapa() {
	wp_enqueue_script( 'google-maps', 'https://maps.googleapis.com/maps/api/js?key='.GMAPS_API_KEY.'&callback=', null, '3', true );
}

function home_enqueue() {
	// wp_enqueue_style('jquery.owlcarousel');
	// wp_enqueue_style('jquery.owlcarousel-default');
	// wp_enqueue_script('jquery.owlcarousel');
}

function telas_enqueue() {
	wp_enqueue_script('telas');
}

if ( LOCAL || DEBUG_BD )
	$wpdb->show_errors();



/* MS */

require 'abstract.componente.php';
require 'class.elemento.php';
require 'func.gerais.php';
require 'def.i18n.php';
