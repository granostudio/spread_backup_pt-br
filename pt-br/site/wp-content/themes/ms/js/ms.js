/* Desenvolvido pela MontarSite - 2017 - www.montarsite.com.br */

// Definições

function inputBackspace(event) {
	// Se o cursor estiver no final do input e o valor terminar num separador de número,
	// remove o separador e o número anterior ao separador ao pressionar backspace
	if ( this.selectionStart != this.value.length )
		return;
	if ( event.keyCode == 8 && /\d[-_,.\/\\()]\s*$/.test( this.value ) )
		this.value = this.value.trim().slice( 0, -1 );
}

function zeroPad( valor, comprimento ) {
	if ( !comprimento )
		comprimento = 2;
	valor += '';
	while ( valor.length < comprimento )
		valor = '0' + valor;
	return valor;
}

function inputTelefone(event) {
	var val = this.value.replace( /^0+|\D/g, '' ),
		format = '';
	if ( val.length )
		format = '(' + val.substring( 0, 2 );
	if ( val.length >= 2 )
		format += ') ';
	if ( val.length == 11 ) {
		format += val.substring( 2, 5 ) + '-'
				+ val.substring( 5, 8 ) + '-'
				+ val.substring( 8, 11 );
	} else {
		format += val.substring( 2, 6 );
		if ( val.length >= 6 )
			format += '-' + val.substring( 6, 10 );
	}
	this.value = format;
}

function equalizarAltura( $itens ) {

	var max = 0;

	if ( !$itens || !$itens.length )
		return false;

	$itens
		.each( function(n) {
			var $this = $itens.eq(n),
				// altura = $this.height();
				altura = Math.ceil( $this.prop('offsetHeight') + 1 ); // O +1 cobre eventuais frações de pixel que não são reportadas pelo offsetHeight
			max = Math.max( max, altura );
		})
		.each( function(n) {
			var $this = $itens.eq(n);
			$this.css( 'minHeight', max + 'px' );
		})
		.trigger('equalizarAltura');

	return max;

};

function lineHeight( $itens, $ancestral ) {

	var max = 0;

	if ( !$itens || !$itens.length || !$ancestral || !$ancestral.length )
		return false;

	$itens
		.each( function(n) {
			var $this = $itens.eq(n),
				// altura = $this.height();
				altura = $this.prop('offsetHeight');
			max = Math.max( max, altura );
		})

	$ancestral.css( 'lineHeight', max + 'px' );

	return max;

};

function progressBarCarousel( $carousel, $barra, percent, incremento ) {
	percent += incremento || 5;
	if ( percent >= 100 && percent < 1000 ) {
		$carousel.carousel('next');
		percent = 1000;
	}
	$barra.css({ width : Math.min( percent, 100 ) + '%' });
	return percent;
}

function waypointAnimate( $elements, $target ) {
	// @requer jquery.waypoints, animate.css, jquery.mobile
	$elements.each( function(){
		var $el = jQuery(this),
			i = $el.data("anim"),
			t = $el.data("anim-duration"),
			d = jQuery.browser.mobile ? 0 : $el.data("anim-delay"),
			$o = $target || $el;
		$el.addClass('anim');
		$el.css({
			"-webkit-animation-delay":d,
			"-moz-animation-delay":d,
			"animation-delay":d,
			"-webkit-animation-duration":t,
			"-moz-animation-duration":t,
			"-anim-duration":t,
			"animation-duration":t
		});
		$o.waypoint(
			function(){
				$el
					.addClass("animated")
					.addClass(i);
			},
			{
				triggerOnce:!0,
				offset: '100%', //"80%"
			}
		);
	})
}



// Startup

var mobile = jQuery(window).width() < 768 - 10 /* scrollbar */;

jQuery( function($) {

	jQuery.browser.mobile = mobile;

  // menu
	$("#menu-item-950 a:first-child").click(function(){
		$('.menu-mobile ul.sub-menu').toggleClass('active');
	})
	$('#menu-hamburguer').click(function(){
		$(this).toggleClass('open');
		$('.menu-mobile').toggleClass('active');
	});
	$('.hover-fechar').click(function(){
		$("#menu-hamburguer").toggleClass('open');
		$('.menu-mobile').toggleClass('active');
	});
	$('.news-box .fechar').click(function(){
		$(".news-box").toggleClass('open');
	});
	$('.btn-newsletter').click(function(){
		$(".news-box").toggleClass('open');
	});


	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	// Ativa os controles no lightbox

	$('#blueimp-gallery').addClass('blueimp-gallery-controls');

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	// Smooth scroll

	$('a[href^="#"], a[href*="/#"]').not('a[href$="#"], .carousel-control').smoothScroll({

	  offset: -1 * $('#sticky_nav').height(),
	  // offset: 0,

	  // one of 'top' or 'left'
	  direction: 'top',

	  // only use if you want to override default behavior
	  scrollTarget: null,

	  // fn(opts) function to be called before scrolling occurs.
	  // `this` is the element(s) being scrolled
	  beforeScroll: function() {},

	  // fn(opts) function to be called after scrolling occurs.
	  // `this` is the triggering element
	  afterScroll: function() {},
	  easing: 'swing',

	  // speed can be a number or 'auto'
	  // if 'auto', the speed will be calculated based on the formula:
	  // (current scroll position - target scroll position) / autoCoeffic
	  speed: 'auto',

	  // autoCoefficent: Only used when speed set to "auto".
	  // The higher this number, the faster the scroll speed
	  autoCoefficient: 3,

	  // $.fn.smoothScroll only: whether to prevent the default click action
	  preventDefault: true

	}).click( function() {
		this.blur();
	});

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	// Equalização da altura de itens

	var $equalizarLista = jQuery('.equalizar_lista');

	if ( !mobile ) {
		$equalizarLista.each( function(n, el) {
			var $this = $equalizarLista.eq(n);
			equalizarAltura( $this.find('.equalizar_item') );
		});
	}


	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	// Alinhamento vertical

	var $vertAncestrais = jQuery('.vert_ancestral');

	if ( !mobile ) {
		$vertAncestrais.each( function(n, el) {
			var $this = $vertAncestrais.eq(n),
				$containers = $this.find('.vert_container');
			lineHeight( $this.find('.vert_item'), $containers.length ? $containers : $this );
		});
	}

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	// Sliders

	var $todosSliders = jQuery('.carousel');

	$todosSliders.each( function(n, el) {
		var $this = $todosSliders.eq(n),
			$itens = $this.find('.slide_item'),
			percent = 0,
			$barra = $this.find('.carousel-progress'),
			intervalo = parseInt( $this.data('interval') || 5000 ),
			barraAnimacao = 200,
			barraIncrementoPercentual = barraAnimacao / intervalo * 100,
			barraIntervalo,
			barraFuncao,
			pausado = false;
		equalizarAltura( $itens );
		// Adiciona suporte a swipe (mobile)
		// @requer jquery.touchSwipe
		$this.find('.carousel-inner').swipe({
			swipeLeft : function() { // ( event, direction, distance, duration, fingerCount )
				$this.carousel('next');
			},
			swipeRight : function() {
				$this.carousel('prev');
			},
			threshold: 0
		});
		// Barra de progresso
		if ( $barra.length ) {
			barraFuncao = function() {
				percent = progressBarCarousel( $this, $barra, percent, barraIncrementoPercentual );
			};
			$this
				.carousel('pause')
				.on( 'slid.bs.carousel', function () {
					percent = 0;
				} )
				.hover(
					function() {
						if ( pausado )
							return;
						clearInterval( barraIntervalo );
					},
					function() {
						if ( pausado )
							return;
						barraIntervalo = setInterval( barraFuncao, barraAnimacao );
					}
				)
				.on( 'pausar.progresso', function() {
					pausado = true;
					clearInterval( barraIntervalo );
				} )
				.on( 'resumir.progresso', function() {
					pausado = false;
					barraIntervalo = setInterval( barraFuncao, barraAnimacao );
				} );
			barraIntervalo = setInterval( barraFuncao, barraAnimacao );
		}
	});

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	// Trava o scroll no Google Maps
	// @requer googlemaps-scrollprevent.js

	var $mapas = jQuery('.gmp_map_opts');

	if ( $mapas.length )
		$mapas.scrollprevent({ pressDuration: 350 }).start();

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	// Animações de entrada

	waypointAnimate( jQuery(".anim") ); // *[data-anim]

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	// Formulários

	jQuery('input[name=tel], input[name=cel]').each( function(n, el) {
		var $this = jQuery(el);
		$this
			.bind({
				'keydown' : inputBackspace,
				'keyup' : inputTelefone,
				'paste' : inputTelefone
			})
			// .attr( 'placeholder', '(##) [#]####-####' )
			.attr( 'maxlength', 17 );
	});

	jQuery('.wpcf7-select').each( function() {
		var $this = jQuery(this);
		if ( !$this.parent().hasClass('select_wrapper') )
			$this.wrap('<div class="select_wrapper" />');
		$this.removeClass('form-control');
	} );

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	// Links nulos

	jQuery('a[href=#]').click( function(e) {
		e.preventDefault();
	});

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	// Menu flutuante

	if ( !mobile ) {

		var $sticky = jQuery('#sticky_nav'),
			$header = jQuery('#masthead'),
			éHome = document.body.className.indexOf('home') != -1,
			$intro = jQuery('#intro, .entry-header');

		// $header.css( 'height', $sticky.height() + 'px' );

		if ( $intro.length ) { // éHome
			$intro.waypoint( function(direcao) {
				if ( direcao == 'down' ) {
					$sticky
						.hide()
						.addClass('flutuante')
						.slideDown(275);
				} else {
					$sticky
						.slideUp( 275, function() {
							$sticky
								.removeClass('flutuante')
								.show();
						});
				}
			}, {
				offset	: -1 * $intro.prop('offsetHeight')  // $header.height()
			} );
		} else {
			$sticky.addClass('flutuante');
		}

	}

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	// Submenus com maior permanência

	var menuDelay = 800,
		menuTimer,
		$menusPai = jQuery('ul.menu li.menu-item-has-children');

	$menusPai.each( function() {

		var $item = jQuery(this),
			$ancestrais = $item.parents('li.menu-item-has-children'),
			$arvoreAtiva = $item.add( $ancestrais ),
			timer,
			onMouseOut;

		onMouseOut = function() {
			$item.removeClass('hover');
		}

		$item.hover(
			function() {
				clearTimeout( timer );
				$menusPai.not( $arvoreAtiva ).removeClass('hover');
				$item.addClass('hover');
			},
			function() {
				timer = setTimeout( onMouseOut, menuDelay );
			}
		);

	});

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	// Tabs

	jQuery('.tabs').each( function() {

		var $raiz = jQuery(this),
			$tabLinks = $raiz.find('.tab_link'),
			$tabWrappers = $raiz.find('.tab_wrapper');

		$tabLinks.click( function(e) {
			var $this = jQuery(this),
				index = this.dataset['index'];
			e.preventDefault();
			if ( $this.hasClass('ativo') )
				return;
			$tabLinks.removeClass('ativo');
			$this.addClass('ativo');
			$tabWrappers.filter('.ativo')
				.removeClass('ativo')
				.trigger('tab.ocultado');
			$tabWrappers.eq(index)
				.addClass('ativo')
				.trigger('tab.revelado');
		});

		$tabWrappers.filter('.ativo')
			.trigger('tab.revelado');

	});

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	// Link de chat

	jQuery('a[href="#chat"').click( function(e) {
		e.preventDefault();
		if ( typeof $zopim == 'undefined' || typeof $zopim.livechat == 'undefined' )
			return console.warn('Zopim not loaded');
		$zopim.livechat.window.show();
	} );

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	// Esconder mensagens do CF7

	jQuery('.wpcf7').on( 'click', '.wpcf7-response-output', function(e) {
		jQuery( e.target ).fadeOut();
	} );

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	// Busca

	var $buscaBtn = jQuery('#busca_botao');

	$buscaBtn.click( function() {
		$buscaBtn.toggleClass('ativo');
	} );

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	// SEÇÕES DA HOME

	var $wrappers = jQuery('.secao_wrapper');

	$wrappers.each( function(n) {
		var $this = $wrappers.eq(n),
			$secao = $this.parents('.secao'),
			altura = $this.children('.container').prop('offsetHeight');
		$secao.css( 'minHeight', altura + 'px' );
	} );

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	// SVG

	/*
	jQuery('img[src$=".svg"]').each(function(){

		// by Drew Baker
		// https://stackoverflow.com/a/11978996/2611404

		var $img = jQuery(this);
		var imgID = $img.attr('id');
		var imgClass = $img.attr('class');
		var imgURL = $img.attr('src');

		jQuery.get(imgURL, function(data) {
			// Get the SVG tag, ignore the rest
			var $svg = jQuery(data).find('svg');

			// Add replaced image's ID to the new SVG
			if(typeof imgID !== 'undefined') {
				$svg = $svg.attr('id', imgID);
			}
			// Add replaced image's classes to the new SVG
			if(typeof imgClass !== 'undefined') {
				$svg = $svg.attr('class', imgClass+' replaced-svg');
			}

			// Remove any invalid XML tags as per http://validator.w3.org
			$svg.removeAttr('xmlns:a')
				.find('path')
					.removeAttr('fill');

			// Replace image with new SVG
			$img.replaceWith($svg);

		}, 'xml');

	});
	*/

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	// MODO TESTE - DEIXA TODOS OS LINKS NULOS

	/*
	jQuery('a:not([target]):not([href*="#"])').attr( 'href', '#' ).click( function(e) {
		e.preventDefault();
	} );
	*/

});
