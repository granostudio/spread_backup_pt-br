<?php

// Traduções

if ( function_exists('pll_register_string') ) {
	
	pll_register_string( 'Tema', 'Ir', 'Template' );
	pll_register_string( 'Tema', 'Você está aqui:', 'Template' );
	pll_register_string( 'Tema', 'Busca por "%s"', 'Template' );
	pll_register_string( 'Tema', 'Página inexistente', 'Template' );
	
	pll_register_string( 'Tema', 'Ir para o blog', 'Home' );
	
	pll_register_string( 'Tema', 'Organizador:', 'Posts' );
	pll_register_string( 'Tema', 'Saiba Mais', 'Posts' );
	pll_register_string( 'Tema', 'Não há posts que correspondem com sua busca.', 'Posts' );
	pll_register_string( 'Tema', 'Não há posts nesta seção.', 'Posts' );

}