<?php
/**
 * Template Name: Cases
 */

get_header(); ?>

<article id="servicos" <?php post_class(); ?>>


	<header class="entry-header header-cases">
		<div class="header-caption">
			<div class="container">

				<div class="row">
					<h1 class="entry-title">
						<span class="linha3">muito mais que ideias,</span>
						<span class="linha2">Cases</span>
					</h1>
				</div>

			</div>
		</div>
	</header><!-- .entry-header -->


	<div id="cases" class="entry-main">
		<div class="container">

			<div class="menu-categorias">
				<?php
				$categories = get_the_category();
				$separator = ' ';
				$output = '';
				if ( ! empty( $categories ) ) {
				    foreach( $categories as $category ) {
				        $output .= '<div class="list-category"><a href="' . esc_url( get_category_link( $category->term_id ) ) . '" alt="' . esc_attr( sprintf( __( 'View all posts in %s', 'textdomain' ), $category->name ) ) . '">' . esc_html( $category->name ) . '</a></div>' . $separator;
				    }
				    echo trim( $output, $separator );
				}
				?>
			</div>

			<div class="row-clientes">
				<?php
			      // start by setting up the query
			      $query = new WP_Query( array(
			          'post_type' => 'clientes'
			      ));

			      // now check if the query has posts and if so, output their content in a banner-box div
			      if ( $query->have_posts() ) { ?>

			      <?php while ( $query->have_posts() ) : $query->the_post(); ?>

				<div class="col-md-4">

					<div class="mask-clientes">
						<p>Cliente: <?php echo get_the_title(); ?></p>
						<p><?php the_field('pos_titulo_clientes'); ?></p>
						<p><?php the_field('campo_texto_clientes'); ?></p>
					</div>

					<?php

					$image = get_field('logo_cliente');

					if( !empty($image) ): ?>

						<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />

					<?php endif; ?>

					<h4><?php the_field('pos_titulo_clientes'); ?></h4>
					<p><?php echo the_excerpt_max(235); ?></p>


				</div>

				<?php endwhile; } ?>

			</div>

		</div>
	</div>

</article><!-- #servicos -->

<?php get_footer();
