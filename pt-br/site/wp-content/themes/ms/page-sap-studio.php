<?php
/**
 * Template Name: SAP Studio
 */

global $post;

add_action( 'wp_enqueue_scripts', 'telas_enqueue' );

get_header();
extract( get_fields() );

?>

<article id="servicos" <?php post_class(); ?>>

	<header class="entry-header" <?php fundo( umDe( $fundo, FUNDO_PADRAO ), 'full' ) ?>>
		<div class="header-caption">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-3 col-sm-offset-3 col_titulo ">
						<?=
						E::h1( null, 'entry-title' )
							->span( $titulo1, 'linha1' )
							->span( $titulo2, 'linha2' )
							->span( $titulo3, 'linha3' )
						?>
					</div><!-- .col -->
					<div class="col-xs-12 col-sm-5 col-md-4 col_texto ">
						<?=
						E::p( null, 'entry-desc' )
							->span( $desc, 'entry-desc-texto' )
						?>
					</div><!-- .col -->
				</div><!-- .row -->
			</div><!-- .container -->
		</div><!-- .header-caption -->
		<?php
		ancora(

			'#sap-studio',

			imgTema( 'seta_amarela.png', '&darr;', 'home_scroll_img' ),

			false,

		    'home_scroll amarelo'

			);
		?>
	</header><!-- .entry-header -->

	<div id="sap-studio" class="entry-main">
		<div class="entry-content">
			<div class="container">

				<div id="sap_escolha" class="vert_ancestral">
					<div class="row">
						<?php

						$telasParams = array(
							'id'		=> 'sap_telas',
							'telas'		=> array(),
						);

						$classeCols = 'botao col-xs-3 col-sm-' . col( count( $subsecoes ) );
						$s = 0;

						foreach ( $subsecoes as $tela ) :

							$s++;
							extract( $tela, EXTR_PREFIX_ALL, 'tela' );

							if ( is_numeric(  $tela_subpagina ) ) :
								$sub_id = (int) $wpdb->get_var( $wpdb->prepare("
									SELECT ID, post_content
									FROM $wpdb->posts
									WHERE post_parent = $post->ID
									AND menu_order = %d
									LIMIT 1
								", (int) $tela_subpagina ) );
							else :
								$sub_id = (int) $wpdb->get_var( $wpdb->prepare("
									SELECT ID, post_content
									FROM $wpdb->posts
									WHERE post_parent = $post->ID
									AND post_name = %s
									OR  post_title = %s
									LIMIT 1
								", $tela_subpagina, $tela_subpagina ) );
							endif;

							if ( $sub_id ) :

								$sub_conteudo = wpautop( do_shortcode( $wpdb->get_var( null, 1 ) ) );
								$telasParams['telas'][] = $sub_conteudo;

								?>
								<div class="<?= $classeCols ?>">
									<button type="button" class="subsecao_botao<?php if ( $s == 1 ) print ' ativo' ?>" data-sub="<?= $sub_id ?>" data-tela="<?= $s - 1 ?>">
										<span class="botao_conteudo">
											<span class="botao_icone vert_container vert_item">
												<?=
												obterImg( $tela_icone_inativo, 'full', array( 'class' => 'botao_icone_inativo', 'alt' => $tela_titulo ) ) .
												obterImg( $tela_icone_ativo, 'full', array( 'class' => 'botao_icone_ativo', 'alt' => $tela_titulo ) )
												?>
											</span><!-- .botao_icone -->
											<?= E::span( str_replace( ' ', '<br />', $tela_titulo ), 'botao_rotulo' ) ?>
										</span><!-- .botao_conteudo -->
									</button><!-- .subsecao_botao -->
								</div><!-- .col -->
								<?php

							else :

								printf( '<strong>Subpágina "%s" não encontrada!</strong>', $tela_subpagina );

							endif;

						endforeach;
						?>
					</div><!-- .row -->
				</div><!-- #sap_escolha -->

				<?php
				Componente::telas( $telasParams );
				?>

			</div><!-- .container -->
		</div><!-- .entry-content -->
	</div><!-- .entry-main -->

</article><!-- #servicos -->

<script>
	jQuery( function() {

		var $telas = jQuery('#sap_telas'),
			$handler = $telas[0].$,
			$botoes = jQuery('.subsecao_botao');

		$botoes.click( function(e) {
			var $this = jQuery(this),
				tela = parseInt( this.dataset['tela'] );
			e.preventDefault();
			if ( $this.hasClass('ativo') )
				return;
			$botoes.filter('.ativo').removeClass('ativo');
			$this.addClass('ativo');
			$handler.trocar( tela );
		} );

	} );
</script>

<?php

get_footer();
