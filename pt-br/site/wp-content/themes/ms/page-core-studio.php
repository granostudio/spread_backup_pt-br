<?php
/**
 * Template Name: Core Studio
 */

get_header();
extract( get_fields() );
?>

<article id="servicos" <?php post_class(); ?>>

	<header class="entry-header" <?php fundo( umDe( $fundo, FUNDO_PADRAO ), 'full' ) ?>>
		<div class="header-caption">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-4 col-sm-offset-2 ">
						<?=
						E::h1( null, 'entry-title' )
							->span( $titulo1, 'linha1' )
							->span( $titulo2, 'linha2' )
							->span( $titulo3, 'linha3' )
						?>
					</div><!-- .col -->
					<div class="col-xs-12 col-sm-5 col-md-4 ">
						<?=
						E::p( null, 'entry-desc' )
							->span( $desc, 'entry-desc-texto' )
						?>
					</div><!-- .col -->
				</div><!-- .row -->
			</div><!-- .container -->
		</div><!-- .header-caption -->
		<?php
		ancora(

			'#core-studio',

			imgTema( 'seta_amarela.png', '&darr;', 'home_scroll_img' ),

			false,

		    'home_scroll amarelo'

			);
		?>
	</header><!-- .entry-header -->

	<div id="core-studio" class="entry-main">
		<div class="entry-content">

			<?php
			// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
			// BLOCO 1
			file_put_contents('/tmp/bloco.txt',print_r($bloco1,true));
			extract( $bloco1[0], EXTR_PREFIX_ALL, 'bloco1' );
			?>

			<section id="bloco1" class="bloco">
				<div class="container">

					<div class="intro">
						<?=
						E::h2( null, 'intro_titulo' )
							->span( $bloco1_titulo1, 'linha1' )
							->span( $bloco1_titulo2, 'linha2' )
						?>
					</div><!-- .intro -->

					<div class="secoes">

						<div class="secao1">
							<div class="row">

								<div class="col-xs-12 col-sm-6">
									<div class="secao_info">
										<?php
										foreach ( $bloco1_secao1_itens as $item ) :
											extract( $item, EXTR_PREFIX_ALL, 'item' );
											?>
											<section class="secao_item">
												<div class="row">
													<div class="col-xs-2">
														<?=
														E::div( obterImg( $item_icone, 'medium', 'item_icone_img' ), 'item_icone' )
														?>
													</div><!-- .col -->
													<div class="col-xs-8">
														<?=
														E::h3( $item_nome, 'item_nome' ) .
														E::p( do_shortcode( htmlspecialchars_decode( $item_texto ) ), 'item_texto' )
														?>
													</div><!-- .col -->
												</div><!-- .row -->
											</section><!-- .secao_item -->
											<?php
										endforeach;
										?>
									</div><!-- .secao_info -->
								</div><!-- .col -->

								<div class="col-xs-12 col-sm-6">
									<div class="secao_ilustracao">
										<?=
										obterImg( $bloco1_secao1_imagem )
										?>
									</div><!-- .secao_ilustracao -->
								</div><!-- .col -->

							</div><!-- .row -->
						</div><!-- .secao1 -->

						<div class="secao2">
							<div class="row">

								<div class="col-xs-12 col-sm-5">
									<div class="secao_ilustracao">
										<?=
										obterImg( $bloco1_secao2_imagem )
										?>
									</div><!-- .secao_ilustracao -->
								</div><!-- .col -->

								<div class="col-xs-12 col-sm-7">
									<div class="secao_info">
										<?php
										foreach ( $bloco1_secao2 as $item ) :
											extract( $item, EXTR_PREFIX_ALL, 'item' );
											?>
											<section class="secao_item">
												<div class="row">
													<div class="col-xs-2">
														<?=
														E::div( obterImg( $item_icone, 'medium', array( 'class' => 'item_icone_img vert_item', 'alt' => $item_nome ) ), 'item_icone' )
														?>
													</div><!-- .col -->
													<div class="col-xs-8">
														<?=
														E::h3( $item_nome, 'item_nome' ) .
														E::p( do_shortcode( $item_texto ), 'item_texto' )
														?>
													</div><!-- .col -->
												</div><!-- .row -->
											</section><!-- .secao_item -->
											<?php
										endforeach;
										?>
									</div><!-- .secao_info -->
								</div><!-- .col -->

							</div><!-- .row -->
						</div><!-- .secao1 -->

					</div><!-- .secoes -->

				</div><!-- .container -->
			</section><!-- .bloco -->

			<?php
			// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
			// BLOCO 2

			extract( $bloco2[0], EXTR_PREFIX_ALL, 'bloco2' );
			?>

			<section id="bloco2" class="bloco">
				<div class="container">

					<div class="intro">
						<?=
						E::h2( null, 'intro_titulo' )
							->span( $bloco2_titulo1, 'linha1' )
							->span( $bloco2_titulo2, 'linha2' )
						?>
					</div><!-- .intro -->

					<div class="itens vert_ancestral">
						<div class="row">
							<?php
							$classeCols = 'col-xs-6 col-sm-4';
							$n = 0;
							foreach ( $bloco2_itens as $item ) :
								extract( $item, EXTR_PREFIX_ALL, 'item' );
								?>
								<div class="<?= $classeCols ?>">
									<section class="item">
										<?=
										E::div( obterImg( $item_icone, 'medium', array( 'class' => 'item_icone_img vert_item', 'alt' => $item_titulo ), 'item_icone vert_container' ), 'class_img' ) .
										E::h3( $item_titulo, 'item_nome' ) .
										E::p( str_replace( ' - ', ' &bull; ', $item_texto ), 'item_texto' )
										?>
									</section><!-- .item -->
								</div><!-- .col -->
								<?php
								row( $n, $classeCols );
							endforeach;
							?>
						</div><!-- .row -->
					</div><!-- .itens -->

				</div><!-- .container -->
			</section><!-- .bloco -->

		</div><!-- .entry-content -->
	</div><!-- .entry-main -->

</article><!-- #servicos -->

<?php

get_footer();
