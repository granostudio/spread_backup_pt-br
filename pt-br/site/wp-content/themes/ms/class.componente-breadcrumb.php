<?php

class Componente_Breadcrumb extends Componente_Abstrato {
	
	protected $_attrWrapper = array( 'id', 'class', 'style' );
	
	public function __construct( $novosParams = null ) {
		// Configurações do componente
		$this->_nome = 'Breadcrumb';
		$this->_slug = 'breadcrumb';
		$this->_aceitarParamsNovos = false;
		$this->_padroes = array(
			'id'					=> null,
			'class'					=> 'breadcrumb',
			'style'					=> null,
			'ignorar_cats'			=> array(1), // lista de categorias que não devem aparecer na hierarquia do post ou da página de categoria
			'rotulo'				=> null, // mostrado antes dos links
			'incluir_home'			=> true, // inclui um item para a homepage
			'icone_home'			=> 'home', // ícone do FontAwesome para a home; pode ser desativado
			'incluir_pagina_posts'	=> true, // se a home for uma página estática, inclui a página de posts no breadcrumb de posts e páginas de arquivo
			// Como nomear a home, em ordem de prioridade
			'titulo_home_da_pagina'	=> true, // aplicável se a home for uma página estática; obtém titulo definido na página
			'titulo_home_nome_site'	=> false, // utiliza o nome do site
			'titulo_home'			=> 'Home', // utiliza uma string customizada
			// Como nomear a página de posts, caso ela não seja a homepage
			'titulo_pp_da_pagina'	=> true, // obtém titulo definido na página
			'titulo_pp'				=> 'Blog', // utiliza uma string customizada
			// Outras páginas
			'titulo_busca'			=> 'Busca por "%s"', // %s representa os termos
			'titulo_404'			=> 'Página inexistente',
			// Liga ou desliga o breadcrumb em determinadas páginas
			'is_front_page'			=> false,
			'is_home'				=> false,
			'is_single'				=> true,
			'is_page'				=> false,
			'is_category'			=> true,
			'is_tag'				=> true,
			'is_author'				=> true,
			'is_date'				=> true,
			'is_search'				=> true,
			'is_attachment'			=> false,
			'is_404'				=> false,
		);
		$this->_sanitizadores = array(
		);
		$this->_obrigatorios = array(
		);
		$this->_validadores = array(
			'id'				=> 'is_string',
			'class'				=> 'is_string',
			'style'				=> 'is_string',
			'ignorar_cats'		=> 'is_array',
			'rotulo'			=> 'is_string',
			'icone_home'		=> 'is_string',
			'titulo_home'		=> 'is_string',
			'titulo_fp'			=> 'is_string',
			'titulo_busca'		=> 'is_string',
			'titulo_404'		=> 'is_string',
		);
		$this->_regrasJuncao = array(
			'class'				=> '+',
			'ignorar_cats'		=> '+',
		);
		// Inicialização
		parent::__construct( $novosParams );
		$this->_posInit();
	}
	
	private function _posInit() {
		if ( !$this->_permitirOutput )
			return;
		if ( is_front_page() )
			$this->_permitirOutput = (bool) $this->is_front_page;
		elseif ( is_home() )
			$this->_permitirOutput = (bool) $this->is_home;
		elseif ( is_single() )
			$this->_permitirOutput = (bool) $this->is_single;
		elseif ( is_page() )
			$this->_permitirOutput = (bool) $this->is_page;
		elseif ( is_category() )
			$this->_permitirOutput = (bool) $this->is_category;
		elseif ( is_tag() )
			$this->_permitirOutput = (bool) $this->is_tag;
		elseif ( is_author() )
			$this->_permitirOutput = (bool) $this->is_author;
		elseif ( is_date() )
			$this->_permitirOutput = (bool) $this->is_date;
		elseif ( is_search() )
			$this->_permitirOutput = (bool) $this->is_search;
		elseif ( is_search() )
			$this->_permitirOutput = (bool) $this->is_search;
		elseif ( is_attachment() )
			$this->_permitirOutput = (bool) $this->is_attachment;
		elseif ( is_404() )
			$this->_permitirOutput = (bool) $this->is_404;
	}
	
	public function tituloPagina( $post_id = null ) {
		global $wpdb;
		if ( !$post_id )
			$post_id = get_the_ID();
		if ( !$post_id || !is_int( $post_id ) )
			return false;
		return $wpdb->get_var("
			SELECT post_title
			FROM $wpdb->posts
			WHERE ID = $post_id
			LIMIT 1
		");
	}
	
	public function hierarquiaCategoriasPost( $post_id = null ) {
		if ( !$post_id )
			$post_id = get_the_ID();
		if ( !$post_id || !is_int( $post_id ) )
			return false;
		$cats = get_the_category( $post_id );
		$retorno = array();
		if ( !$cats )
			return $retorno;
		// Organiza pelas IDs
		$ids = array();
		foreach ( $cats as $c ) {
			$ids[] = $c->term_id;
			$c->hierarquia = false;
		}
		$cats = array_combine( $ids, $cats );
		reset( $cats );
		// Define o nível
		$nivelMaisProfundo = 0;
		$catMaisProfunda = 0;
		$catsProcessadas = 0;
		$catsTotal = count( $cats );
		$extrairNivel = function( $cat_id ) use ( &$cats, &$nivelMaisProfundo, &$catMaisProfunda, &$catsProcessadas, &$extrairNivel ) {
			$c =& $cats[ $cat_id ];
			if ( !$c ) {
				$cats[ $cat_id ] = $c = get_category( $cat_id );
				$c->hierarquia = false;
			}
			if ( $c->hierarquia !== false )
				return $c->hierarquia;
			$hierarquia = $c->parent
				? $extrairNivel( $c->parent ) + 1
				: 0
			;
			$c->hierarquia = $hierarquia;
			$catsProcessadas++;
			if ( !$catMaisProfunda || $hierarquia > $nivelMaisProfundo ) {
				$nivelMaisProfundo = $hierarquia;
				$catMaisProfunda = $cat_id;
			}
			return $hierarquia;
		};
		foreach ( $cats as $cat_id => $c ) {
			if ( $catsProcessadas >= $catsTotal )
				break;
			$extrairNivel( $cat_id );
		}
		// Puxa a categoria mais profunda e sobe na hierarquia
		$catAtual = $catMaisProfunda;
		while ( $catAtual ) {
			$c =& $cats[ $catAtual ];
			if ( $this->ignorar_cats && !in_array( $c->term_id, $this->ignorar_cats ) )
				$retorno[] =& $c;
			$catAtual = $c->parent;
		}
		// Fim
		$retorno = array_reverse( $retorno );
		return $retorno;
	}
		
}