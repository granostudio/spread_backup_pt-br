<?php
get_header();
?>

<article id="erro404" <?php post_class(); ?>>

	<header class="entry-header" <?php fundo( FUNDO_PADRAO, 'full' ) ?>>
		<h1 class="entry-title"><span class="anim" <?php animAttr( 'fadeInUp', 0.8, 0.5 ) ?>>Página inexistente</span></h1>
	</header><!-- .entry-header -->
	
	<div class="container">
		<div class="entry-main">
			<div class="entry-content">
	
				<h1>Página inexistente</h1>
				
				<p>Não há nenhuma página com este endereço.</p>
				
				<p>Utilize o menu acima ou o campo de busca para encontrar o que procura.</p>
					
			</div><!-- .entry-content -->
		</div><!-- .entry-main -->
	</div><!-- .container -->
	
</article><!-- #post-## -->

<?php

get_footer();
