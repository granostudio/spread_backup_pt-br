<?php
// @requer ancora, elemento, sluggify
?>
<div <?= $this->attr( $this->_attrWrapper ) ?>>
	
	<div class="tabs_nav">
		<?php
		foreach ( $param_tabs as $i => $t ) :
			ancora( '#', $t['rotulo'], 0, array(
				'class'			=> 'tab_link' . ( isset( $t['destaque'] ) && $t['destaque'] ? ' destaque' : '' ) . ($i?'':' ativo'),
				'data-index'	=> $i,
				'data-slug'		=> sluggify( $t['rotulo'], '_' ),
			) );
		endforeach;
		?>
	</div><!-- .tabs_nav -->
	
	<div class="tabs_conteudo">
		<?php
		foreach ( $param_tabs as $i => $t ) :
			elemento(
				'div',
				$t['conteudo'],
				array(
					'class'			=> 'tab_wrapper' . ( isset( $t['destaque'] ) && $t['destaque'] ? ' destaque' : '' ) . ($i?'':' ativo'),
					'data-index'	=> $i,
					'data-slug'		=> sluggify( $t['rotulo'], '_' ),
				)
			);
		endforeach;
		?>
		<div class="clearfix"></div>
	</div><!-- .tabs_conteudo -->
	
</div><!-- .tabs -->
