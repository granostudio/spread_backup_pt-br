<?php
// Gera uma trilha de links para o destino atual
// @requer hierarquiaCategoriasPost, cl, fa, elemento, tituloPost, ancora

if ( !is_front_page() && !is_404() ) :
	$classeAncoras = cl('breadcrumbs_link');
	?>
	<nav id="breadcrumbs">
		<ul class="breadcrumbs_list">
			<?php /*
			<li class="breadcrumbs_item desc">Você está aqui</li>
			*/ ?>
			<li class="breadcrumbs_item home"><?php ancora(
				site_url(),
				fa('home') . ' Home',
				0,
				$classeAncoras
			); ?></li><!-- .breadcrumbs_item -->
			<?php
			
			if ( is_single() ) :
			
				// Categorias
				$cats = hierarquiaCategoriasPost();
				if ( $cats ) :
					foreach ( $cats as $c ) :
						elemento(
							'li',
							ancora( get_term_link( $c, 'category' ), $c->name, 0, $classeAncoras, false ),
							cl( 'breadcrumbs_item nivel' . $c->hierarquia )
						);
					endforeach;
				endif;
					
				// Post
				elemento(
					'li',
					ancora( get_the_permalink(), get_the_title(), 0, $classeAncoras, false ),
					cl( 'breadcrumbs_item atual' )
				);
				
			elseif ( is_page() ) :
			
				elemento(
					'li',
					ancora( get_the_permalink(), get_the_title(), 0, $classeAncoras, false ),
					cl( 'breadcrumbs_item atual' )
				);
				
			elseif ( is_category() ) :
			
				// Obtém os antecessores
			
				// $taxonomy = is_category() ? 'category' : 'post_tag';
				$taxonomy = 'category';
				$termoAtual = $wp_query->queried_object;
				$hierarquia = array( $termoAtual );
				
				while ( $termoAtual->parent ) {
					$termoAtual = get_term( $termoAtual->parent, $taxonomy );
					$hierarquia[] = $termoAtual;
				}
				
				$hierarquia = array_reverse( $hierarquia );
				$ultimo = count( $hierarquia ) - 1;
			
				foreach ( $hierarquia as $i => $termo ) :
					elemento(
						'li',
						ancora( get_term_link( $termo->term_id, $taxonomy ), $termo->name, 0, $classeAncoras, false ),
						cl( 'breadcrumbs_item ' . ( $i == $ultimo ? 'atual' : 'nivel' . $i ) )
					);
				endforeach;
				
			elseif ( is_tag() ) :
			
				elemento(
					'li',
					ancora( get_the_permalink( PAGINA_BLOG ), get_the_title( PAGINA_BLOG ), 0, $classeAncoras, false ),
					cl( 'breadcrumbs_item' )
				);
			
				elemento(
					'li',
					ancora( get_term_link( $wp_query->queried_object->term_id, 'post_tag' ), 'Tag: ' . $wp_query->queried_object->name, 0, $classeAncoras, false ),
					cl( 'breadcrumbs_item atual' )
				);
				
			elseif ( is_search() ) :
			
				$query = get_search_query();
			
				if ( $query )
					$titulo = sprintf( 'Busca por "%s"',
						$query
					);
				else
					$titulo = 'Busca';
			
				elemento(
					'li',
					ancora( '#', $titulo, 0, $classeAncoras, false ),
					cl( 'breadcrumbs_item atual' )
				);
			
			elseif ( is_date() ) :
			
			elseif ( is_author() ) :
			
			elseif ( is_home() ) :
			
				elemento(
					'li',
					ancora( '#', get_the_title( PAGINA_BLOG ), 0, $classeAncoras, false ),
					cl( 'breadcrumbs_item atual' )
				);
			
			endif;
			?>
		</ul><!-- .breadcrumbs_list -->
	</nav><!-- #breadcrumbs -->
	<?php
endif;
?>
