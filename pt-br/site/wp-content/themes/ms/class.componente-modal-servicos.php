<?php

class Componente_Modal_Servicos extends Componente_Abstrato {
	
	protected $_attrWrapper = array( 'id', 'class' );
	
	public function __construct( $novosParams = null ) {
		// Configurações do componente
		$this->_nome = 'Modal de Serviços';
		$this->_slug = 'modal_servicos';
		$this->_aceitarParamsNovos = false;
		$this->_padroes = array(
			'modal_id'			=> null,
			'post_id'			=> null,
			'intro'				=> null,
			'acordiao'			=> null,
		);
		$this->_sanitizadores = array(
		);
		$this->_obrigatorios = array(
			'post_id'			=> 1,
		);
		$this->_validadores = array(
			'modal_id'			=> 'is_string',
			'post_id'			=> 'is_int',
			'intro'				=> 'is_array',
			'acordiao'			=> 'is_array',
		);
		$this->_regrasJuncao = array(
		);
		// Inicialização
		parent::__construct( $novosParams );
	}
	
	public function renderizar( $_imprimir = true ) {
		
		// ID do modal
		if ( !$this->modal_id )
			$this->modal_id = 'modal_servicos' . $this->obterIndice();
		
		// Carrega conteúdo do post
		if ( !$this->intro )
			$this->intro = get_field( 'field_5a5119611f4b1', $this->post_id );
		if ( !$this->acordiao )
			$this->acordiao = get_field( 'field_5a5119aa1f4b5', $this->post_id );
				
		return parent::renderizar( $_imprimir );
		
	}
		
}