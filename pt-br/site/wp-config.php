<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa user o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/pt-br:Editando_wp-config.php
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações
// com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define('DB_NAME', 'spread');

/** Usuário do banco de dados MySQL */
define('DB_USER', 'root');
define('WP_ALLOW_MULTISITE', true);

/** Senha do banco de dados MySQL */
define('DB_PASSWORD', 'q1w2e3*2018');

/** Nome do host do MySQL */
define('DB_HOST', 'localhost');

/** Charset do banco de dados a ser usado na criação das tabelas. */
define('DB_CHARSET', 'utf8mb4');

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

// MODIFICADO
// Conexão do blog
define('BLOG_DB_NAME', 'blog');
define('BLOG_DB_USER', 'root');
define('BLOG_DB_PASSWORD', 'q1w2e3*2018');
define('BLOG_DB_HOST', 'localhost');
define('BLOG_DB_PREFIX', 'gs_');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para desvalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'EuF`s0!kgXWt_|xP>hsu*mcR|g(MVe(1^5S`+8 OLiKi*LVj_+Hn|$#I)1f38tE1');
define('SECURE_AUTH_KEY',  '#_2Loa`CpdATg:`Hk_,bZabn&Jc{3(ai_M,`y4q(-EK!&-p|&|ZJ+*m%(n/fHIV3');
define('LOGGED_IN_KEY',    'N^MG=j]c+c4J+>x ,`sA*wY-WYjUp(*Om}K+B|rgIz4zE aNiB6 VVk)[,P@=_aq');
define('NONCE_KEY',        'HBrLv,MDR/2;`g4nmL0Oz)P<&nnyd!4PZEM{U-1uku3|9rNeWdH3j$r4pn[X:7/7');
define('AUTH_SALT',        'X[#vZUgdLbZn/N{_wjmL0=R%Ri0L^GHglM%. %-e0D05c%[QOP!W>gKK>(G[qA2R');
define('SECURE_AUTH_SALT', 'lwP}V}L ]B;|C/WEpp8hF9O*7!`v26>0fH-@#9A`:-+b>ZAi6]~R,1.|eWX9eNz{');
define('LOGGED_IN_SALT',   '}JW+Z!_{BJ)9oG,FjMdXX@<iUps-4N=2aB*itm4kE%gs DVKy/CdNUc.2@MW>y5x');
define('NONCE_SALT',       'Y>;kXN`7DcTjSVsg:%^FfP`)cb,X=J5<|40j4bVr~Z4F8vy+$+YA*_]1%46La|D5');

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * para cada um um único prefixo. Somente números, letras e sublinhados!
 */
$table_prefix  = 'wp_';

/**
 * Para desenvolvedores: Modo debugging WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://codex.wordpress.org/pt-br:Depura%C3%A7%C3%A3o_no_WordPress
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

define( 'LOCAL', (bool) preg_match( '#(?:localhost|127.0.0.1)#i', $_SERVER['SERVER_NAME'] ) );

if ( LOCAL ) {
	define( 'WP_PLUGIN_DIR', $_SERVER['DOCUMENT_ROOT'] . '/comum/wp/plugins' );
	define( 'WP_PLUGIN_URL', 'http://localhost/comum/wp/plugins');
}

/** Configura as variáveis e arquivos do WordPress. */
require_once(ABSPATH . 'wp-settings.php');
