<?php
/**
 * Template Name: Serviços
 */

global $post;

get_header();
extract( get_fields() );

$modais = array();

?>

<article id="servicos" <?php post_class(); ?>>

	<header class="entry-header" <?php fundo( umDe( $fundo, FUNDO_PADRAO ), 'full' ) ?>>
		<div class="header-caption">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-6 col_titulo anim" <?php animAttr( 'fadeInBottom', 1.4, 0.4 ) ?>>
						<?=
						E::h1( null, 'entry-title' )
							->span( $titulo1, 'linha1' )
							->span( $titulo2, 'linha2' )
							->span( $titulo3, 'linha3' )
						?>
					</div><!-- .col -->
					<div class="col-xs-12 col-sm-5 col-md-4 col_texto anim" <?php animAttr( 'fadeInLeft', 0.4, 2 ) ?>>
						<?=
						E::p( null, 'entry-desc' )
							->span( $desc, 'entry-desc-texto' )
						?>
					</div><!-- .col -->
				</div><!-- .row -->
			</div><!-- .container -->
		</div><!-- .header-caption -->
		<?php
		ancora(
			'#',

			imgTema( 'seta_amarela.png', '&darr;', 'home_scroll_img' ),

			false,

		    'home_scroll amarelo'

			);
		?>
	</header><!-- .entry-header -->

	<div id="servicos_conteudo" class="entry-main">
		<div class="entry-content">

			<div class="servicos_blocos">
				<?php

				$totalBlocos = count( $bloco_itens );
				$b = 0;

				foreach ( $blocos as $bloco ) :
					$b++;
					extract( $bloco, EXTR_PREFIX_ALL, 'bloco' );
					?>
					<section class="bloco cor_<?= $bloco_cor ?>">
						<div class="container">

							<div class="bloco_sobre">
								<?=
								E::h2( null, 'sobre_titulo' )
									->span( $bloco_titulo1, 'linha1' )
									->span( $bloco_titulo2, 'linha2' )
									->span( $bloco_titulo3, 'linha3' )
									->span( $bloco_titulo4, 'linha4' ) .
								E::p( $bloco_intro, 'sobre_intro' )
								?>
							</div><!-- .bloco_sobre -->

							<?php
							if ( $bloco_itens ) :
								?>
								<div class="bloco_itens equalizar_lista vert_ancestral colunas_<?= $bloco_colunas ?> layout_<?= $bloco_layout ?>">
									<div class="row inline">
										<?php
										$cols = (int) sanitizarNumerico( $bloco_colunas );
										$classeCols = 'col-xs-12 col-sm-' . col( $cols );
										$n = 0;
										foreach ( $bloco_itens as $item ) :
											$n++;
											extract( $item, EXTR_PREFIX_ALL, 'item' );
											?>
											<div class="col <?= $classeCols ?>">
												<section class="bloco_item equalizar_item">
													<?php

													// ---------------------------------------------------------------------------------
													// HTML DO ITEM
													$output = str_replace("/n", '<br />', E::h3( $item_nome, 'item_nome' ));
													print
														E::div(
															obterImg( $item_imagem, 'medium', array( 'class' => 'item_imagem_img vert_item', 'alt' => $item_nome ) ),
															'item_imagem vert_container ' . ( $bloco_borda_imagens ? 'borda_tipo' . $bloco_borda_imagens : 'sem_borda' )
														) .
														$output .
														E::p( $item_descricao, 'item_descricao' )
													;

													// ---------------------------------------------------------------------------------
													// MODAL

													if ( $item_modal ) :

														if ( is_numeric(  $item_modal ) ) :
															$sub_id = (int) $wpdb->get_var( $wpdb->prepare("
																SELECT ID
																FROM $wpdb->posts
																WHERE post_parent = $post->ID
																AND menu_order = %d
																LIMIT 1
															", (int) $item_modal ) );
														else :
															$sub_id = (int) $wpdb->get_var( $wpdb->prepare("
																SELECT ID
																FROM $wpdb->posts
																WHERE post_parent = $post->ID
																AND post_name = %s
																OR  post_title = %s
																LIMIT 1
															", $item_modal, $item_modal ) );
														endif;

														if ( $sub_id ) :

															$modal_id = 'modal_' . $sub_id;

															$modais[] = Componente::modalServicos( array(
																'post_id'	=> $sub_id,
																'modal_id'	=> $modal_id,
															), false );

															print E::button(
																pll__('Saiba Mais') . imgTema( 'mais_lilas.png', '&rarr;', 'item_mais_seta' ),
																array(
																	'type'			=> 'button',
																	'class'			=> 'btn item_mais',
																	'data-toggle'	=> 'modal',
																	'data-target'	=> '#' . $modal_id,
																)
															);

														else :

															printf( '<strong>Subpágina "%s" não encontrada!</strong>', $item_modal );

														endif;

													endif;

													?>
												</section><!-- .bloco_item -->
											</div><!-- .col -->
											<?php
											row( $n, $classeCols );
										endforeach;
										?>
									</div><!-- .row -->
								</div><!-- .bloco_itens -->
								<?php
							endif;
							?>

						</div><!-- .container -->
					</section><!-- .bloco -->
					<?php
				endforeach;
				?>
			</div><!-- .servicos_blocos -->

		</div><!-- .entry-content -->
	</div><!-- .entry-main -->

</article><!-- #servicos -->

<?php

if ( $modais )
	print implode( "\n\n", $modais );

get_footer();
