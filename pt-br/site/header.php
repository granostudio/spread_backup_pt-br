<?php
global $post, $tema, $wp_query;

if ( is_singular() || is_front_page() )
	the_post();

extract( get_fields('widget_acf_widget_3'), EXTR_PREFIX_ALL, 'cabecalho' );

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

	<meta property="og:url" content="https://spread.com.br/pt-br/site/">
	<meta property="og:site_name" content="Spread Tecnologia" />
	<meta property="og:title" content="Spread Tecnologia" />
	<meta property="og:description" content="Boas ideias transformam empresas, negócios, a sociedade, o mundo, pessoas, você. Transformamos ideias e desafios em soluções que somam valor aos negócios de nossos clientes.">

	<meta property="og:image" content="https://spread.com.br/pt-br/site/wp-content/themes/ms/imagens/metaimage.png" />
    <meta property="og:image:type" content="image/jpeg" />
    <meta property="og:image:width" content="200" />
    <meta property="og:image:height" content="200" />


	<?php /*
	<link rel="icon" href="<?= TEMA ?>/imagens/favicon.png" sizes="32x32" />
	<link rel="icon" href="<?= TEMA ?>/imagens/favicon.png" sizes="192x192" />
	<link rel="apple-touch-icon-precomposed" href="<?= TEMA ?>/imagens/favicon.png" />
	<meta name="msapplication-TileImage" content="<?= TEMA ?>/imagens/favicon.png" />
	*/ ?>
    <!--[if lt IE 9]>
      <script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	<link rel="dns-prefetch" href="//<?= parse_url( get_field( 'url', PAGINA_BLOG ), PHP_URL_HOST ) ?>" />
	<?php wp_head(); ?>

	<!-- RD Station -->
	<script type="text/javascript" async src="https://d335luupugsy2.cloudfront.net/js/loader-scripts/29deb513-6eb0-4dcf-8baf-95dcd29b4d1f-loader.js" ></script>

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-112488293-1"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-112488293-1');
	</script>

	<!-- Hotjar Tracking Code for https://spread.com.br/pt-br/site/ -->
	<script>
	    (function(h,o,t,j,a,r){
	        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
	        h._hjSettings={hjid:875298,hjsv:6};
	        a=o.getElementsByTagName('head')[0];
	        r=o.createElement('script');r.async=1;
	        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
	        a.appendChild(r);
	    })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
	</script>

</head>

<body <?php body_class(); ?>><div id="page" class="hfeed site">

	<header id="masthead" class="site-header" role="banner">
		<div id="sticky_nav">

			<div id="cabecalho_topo">
				<div class="row">

					<div class="col-xs-6 col-sm-6">
						<nav id="cabecalho_breadcrumb">
							<?php
							Componente::breadcrumb( array(
								'id'				=> 'breadcrumb_nav',
								'ignorar_cats'		=> array( CAT_BLOG, CAT_EVENTOS ),
								'rotulo'			=> 'You are here:',
								'icone_home'		=> null,
								'titulo_busca'		=> 'Busca por "%s"',
								'titulo_404'		=> 'Página inexistente',
								'is_front_page'		=> true,
								'is_home'			=> true,
								'is_page'			=> true,
								'is_404'			=> true,
							) );
							?>
						</nav>
					</div><!-- .col -->

					<div class="col-xs-6 col-sm-6">
						<nav id="cabecalho_linguas">
							<h3 class="linguas_rotulo">language</h3>
							<?php
								$caminhoPT = str_replace('en/','pt-br/', $_SERVER['REQUEST_URI']);
								$caminhoEN = str_replace('pt-br/','en/', $_SERVER['REQUEST_URI']);
								?>
								<a href="https://spread.com.br<?= $caminhoPT ?>">Português <img src="//spread.com.br/en/site/wp-content/themes/ms/imagens/brazil.png" alt=""></a>

								<a href="https://spread.com.br<?= $caminhoEN ?>">Inglês <img src="//spread.com.br/en/site/wp-content/themes/ms/imagens/united-states.png" alt=""></a>
								<?php
								// pll_the_languages( array(
								// 	'dropdown'		=> 1,
								// 	'show_names'	=> 1,
								// 	'hide_if_empty'	=> 0,
								// 	'show_flags'	=> 0,
								// ) )
								?>
						</nav><!-- #cabecalho_linguas -->
					</div><!-- .col -->

				</div><!-- .row -->
			</div><!-- #cabecalho_topo -->
			<div id="menu-hamburguer">
				<span></span>
				<span></span>
				<span></span>
				<span></span>
			</div>
			<div class="menu-mobile">
				<div class="hover-fechar"></div>
				<?php wp_nav_menu( array( 'theme_location' => 'principal', 'menu_class' => 'menu' ) ); ?>
				<div class="col">
					<a href="#" class="btn btn-primary btn-newsletter">Receba nossa newsletter</a>
					<?php wp_nav_menu( array( 'theme_location' => 'social', 'menu_class' => 'menu_social' ) ); ?>
				</div>

			</div>
			<div id="cabecalho_principal" class="vert_ancestral">

				<div id="cabecalho_logo" class="vert_item">
					<a href="<?= site_url() ?>" rel="home">
						<?php img( $cabecalho_logo, 'full', cl('cabecalho_logo_img') ) ?>

					</a>

				</div><!-- #cabecalho_logo -->




				<div id="cabecalho_direita">

					<nav id="menu_principal" class="vert_item">
						<div class="collapse navbar-collapse">
							<?php wp_nav_menu( array( 'theme_location' => 'principal', 'menu_class' => 'menu' ) ); ?>
						</div><!-- .collapse -->

					</nav><!-- #menu_principal -->

					<div id="cabecalho_newsletter" class="vert_item"><a href="#" class="btn btn-primary btn-newsletter">Receba nossa newsletter</a></div>



					<nav id="cabecalho_redes" class="vert_item">
						<?php wp_nav_menu( array( 'theme_location' => 'social', 'menu_class' => 'menu_social' ) ); ?>
					</nav><!-- #cabecalho_redes -->

				</div><!-- #cabecalho_direita -->

			</div><!-- #cabecalho_principal -->

		</div><!-- #sticky_nav -->

		<!-- newsletter -->
		<div class="news-box">
			<div class="fechar">
				<span></span>
				<span></span>
				<span></span>
				<span></span>
			</div>
			<h1>Receba a nossa newsletter, a Sprint Spread</h1>
			<div id="newsletter-site-0000ba31a03c53d2e794"></div>
			<script type="text/javascript" src="https://d335luupugsy2.cloudfront.net/js/rdstation-forms/stable/rdstation-forms.min.js"></script>
			<script type="text/javascript">
			  new RDStationForms('newsletter-site-0000ba31a03c53d2e794-html', '').createForm();
			</script>
			<style>
				#conversion-newsletter-site-0000ba31a03c53d2e794{
					background-color:transparent!important;
				}
				#conversion-modal-newsletter-site-0000ba31a03c53d2e794 .modal-content section form input, #conversion-modal-newsletter-site-0000ba31a03c53d2e794 .modal-content section form select, #conversion-modal-newsletter-site-0000ba31a03c53d2e794 .modal-content section form textarea, #conversion-modal-newsletter-site-0000ba31a03c53d2e794 .modal-content section form .select2-choice, #conversion-newsletter-site-0000ba31a03c53d2e794 section input, #conversion-newsletter-site-0000ba31a03c53d2e794 section select, #conversion-newsletter-site-0000ba31a03c53d2e794 section textarea, #conversion-newsletter-site-0000ba31a03c53d2e794 section .select2-choice {
				    display: inline-block !important;
				    background-color: #FFFFFF !important; 
				    vertical-align: middle !important;
				    font-size: 18px !important;
				    line-height: 20px !important;
				    width: 100% !important;
				    height: 50px!important;
				    margin: 0 !important;
				    padding: 20px !important;
				    -webkit-border-radius: 3px !important;
				    -moz-border-radius: 3px !important;
				    border-radius: 25px !important;
				    color: gray !important;
				}
				#conversion-newsletter-site-0000ba31a03c53d2e794 section div.actions .call_button{
					cursor: pointer !important;
			    height: auto !important;
			    text-align: center !important;
			    text-decoration: none !important;
			    font-weight: bold !important;
			    font-size: 20px !important;
			    word-break: break-word !important;
			    line-height: 1.2em !important;
			    white-space: normal !important;
			    vertical-align: middle !important;
			    margin: 2px auto 24px auto !important;
			    padding: 15px 20px 17px 20px !important;
			    -webkit-border-radius: 3px !important;
			    -moz-border-radius: 3px !important;
			    border-radius: 25px !important;
				}
				#conversion-newsletter-site-0000ba31a03c53d2e794 #conversion-form-newsletter-site-0000ba31a03c53d2e794 p.notice{
					text-align: center!important;
					color:#fff!important;
				}
			</style>
			<?php // do_shortcode( $cabecalho_newsletter ) ?>
		</div>

	</header><!-- #masthead -->

	<div id="content" class="site-content">
		<main id="content-area">
