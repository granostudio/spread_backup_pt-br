<?php

function tirarAcentos($string){
    return preg_replace(array("/(á|à|ã|â|ä)/","/(Á|À|Ã|Â|Ä)/","/(é|è|ê|ë)/","/(É|È|Ê|Ë)/","/(í|ì|î|ï)/","/(Í|Ì|Î|Ï)/","/(ó|ò|õ|ô|ö)/","/(Ó|Ò|Õ|Ô|Ö)/","/(ú|ù|û|ü)/","/(Ú|Ù|Û|Ü)/","/(ñ)/","/(Ñ)/"),explode(" ","a A e E i I o O u U n N"),$string);
}

header('Content-Type: text/html; charset=utf-8');
define('DB_NAME_SPREAD', 'spread');
define('DB_NAME_SPREAD_US', 'spread_us');

define('DB_NAME_BLOG', 'blog');
define('DB_NAME_BLOG_US', 'blog_us');



/** Usuário do banco de dados MySQL */
define('DB_USER', 'root');
define('WP_ALLOW_MULTISITE', true);

/** Senha do banco de dados MySQL */
define('DB_PASSWORD', 'q1w2e3*2018');

/** Nome do host do MySQL */
define('DB_HOST', 'localhost');


$pesquisa = tirarAcentos($_POST['pesquisa']);
$pesquisa = str_replace('ç','c',$pesquisa);

$pesq = explode(" ",$pesquisa);
$str_pesq1  = '( ';
$str_pesq2 =  '( ';
foreach($pesq as $dados){

$str_pesq1 .= " slug like '%{$dados}%' or ";

$str_pesq2 .= " meta_value like '%{$dados}%' or ";

}

$str_pesq1 = substr($str_pesq1,0,strlen($str_pesq1)-3).')';
$str_pesq2 = substr($str_pesq2,0,strlen($str_pesq2)-3).')';

$lingua = $_POST['lingua'];

$conn = mysqli_connect(DB_HOST,DB_USER,DB_PASSWORD) ;

if($lingua == null){
$lingua = 'pt-br';
}


switch($lingua){

case 'pt-br':
$banco_spread = DB_NAME_SPREAD;
$blog_spread  = DB_NAME_BLOG;
break;


case 'us':
$banco_spread = DB_NAME_SPREAD_US;
$blog_spread  = DB_NAME_BLOG_US;
break;

default:
$banco_spread = DB_NAME_SPREAD;
$blog_spread  = DB_NAME_BLOG;
break;
}

$tempo1 = microtime();
$db = mysqli_select_db($conn,$banco_spread);


/*$sql = "
select post_title,post_type,ID,guid,post_date,meta_value as post_content, post_parent  from wp_postmeta wp join wp_posts p on wp.post_id = p.ID  where 1=1 and {$str_pesq2}  and p.post_type in ('page','post') and post_parent != 0  group by ID

UNION
(


select post_title,post_type,ID,guid,post_date,post_content, post_parent from wp_posts  where post_type in ('page','post') and 1=1 and {$str_pesq1}  and post_parent != 0 group by ID
) ;";
*/


$sql = "
select post_title,post_type,ID,guid,post_date,post_content, post_parent from wp_posts  where post_type in ('page','post') and 1=1 and ID in (

 SELECT wp_posts.ID
      
       from wp_posts
          join wp_term_relationships on wp_posts.ID = wp_term_relationships.object_id
          join wp_terms on wp_term_relationships.term_taxonomy_id = wp_terms.term_id
          join wp_term_taxonomy on wp_term_relationships.term_taxonomy_id = wp_term_taxonomy.term_taxonomy_id
       WHERE wp_posts.post_status = 'publish' and {$str_pesq1}
ORDER BY wp_terms.slug,wp_posts.ID

)
 ;";



$resultado = mysqli_query($conn,$sql) or exit(mysqli_error($conn));

while($dados = mysqli_fetch_assoc($resultado)){
 foreach($dados as $idx=>$data){
	$dados[$idx] = utf8_encode ($data);
}


if($dados['ID'] > 300){
$dados['ID'] = $dados['post_parent'];
}

$dados["post_date"] = date("d/m/Y", strtotime($dados["post_date"])); 
 $busca['site'][] = $dados;

}
 $busca['total'] = count($busca['site']);




$db = mysqli_select_db($conn,$blog_spread);

$sql = " select post_type,ID , guid,post_date, post_content, meta_value ,meta_key, post_title from gs_posts gs_p LEFT join gs_postmeta gs_m on gs_m.post_id = gs_p.ID  where  post_type in ('page','post') and (post_content like '%{$pesquisa}%' or meta_value like '%{$pesquisa}%') group by ID order by ID ASC;";



$resultado = mysqli_query($conn,$sql) or exit(mysqli_error($conn));

while($dados = mysqli_fetch_assoc($resultado)){


 foreach($dados as $idx=>$data){

        $dados[$idx] = utf8_encode ($data);
}
$dados["post_date"] = date("d/m/Y", strtotime($dados["post_date"]));

$busca['blog'][] = $dados;
}
$busca['total']+=count($busca['blog']);

$tempo2=  microtime();


$total = $tempo2-$tempo1;
$busca['tempo'] = str_replace('.',',',round($total,2));
echo json_encode($busca,JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
