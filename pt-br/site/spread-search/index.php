<?php

require('../wp-blog-header.php' );

#include_once('../pt-br/site/wp-includes/general-template.php');
error_reporting(E_ALL);
?>
<!DOCTYPE html>

<html>
<head>
	<title>spread-pesquisa</title>

	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	
	<link rel="stylesheet" href="css/styles.css">
	<link href="https://fonts.googleapis.com/css?family=Dosis" rel="stylesheet">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
<script
  src="https://code.jquery.com/jquery-2.2.4.min.js"
  integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
  crossorigin="anonymous">
</script>
<script>
($(function()
{
  $("#cabecalho_topo").hide();

}))
  $("#cabecalho_topo").hide();

</script>
<script>
($(function(){
  $('#input_buscar').on('keypress', function (e) {

         if(e.which === 13){
		 window.scrollTo(0, 600); 
            //Disable textbox to prevent multiple submit
            $(this).attr("disabled", "disabled");
		                                $(".resultado_site").html('');

            //Do Stuff, submit, etc..
		$.post('pesquisa.php',{pesquisa:$("#input_buscar").val(),lingua: "pt-br"},function(data){
		myObject = eval('(' + data + ')');
			if(!myObject){
				return false;	
			}	
			$(".resultado-seg").html(" "+myObject['total']+ " registros em "+myObject['tempo'] + " segundo(s)");
			if(myObject['site']){
				$(".resultado_site").html('');
			  for(i=0;i!=4;i++){

				if(myObject['site'][i]){
					console.log(myObject['site'][i]);
					post_content = new String(myObject['site'][i].post_content);
					post_content = post_content.substr(0,250)
				 $(".resultado_site").append('<a href="../site/?page_id='+myObject['site'][i].ID+'" target="_blank"><div class="resultado-artigo"><h5>'+myObject['site'][i].post_title+'</h5><p class="postado">Postado em '+myObject['site'][i].post_date+'</p><p class="conteudo-artigo">'+post_content+'</p></div></a>'); 
				}
			 }
			}

                                $(".resultado_blog").html('');

			  if(myObject['blog']){
                                $(".blog").html('');
                          for(i=0;i!=4;i++){

                                if(myObject['blog'][i]){
                                 
						     post_content = new String(myObject['blog'][i].post_content);
                                        post_content = post_content.substr(0,250)+'...';

			
					$(".resultado_blog").append('<a  href="../blog/?page_id='+myObject['blog'][i].ID+'" target="_blank" ><div class="resultado-artigo"><h5>'+myObject['blog'][i].post_title+'</h5><p class="postado">Postado em '+myObject['blog'][i].post_date+'</p><p class="conteudo-artigo">'+post_content+'</p></div></a>'); 
                                }
                         }
                        }



		})
            //Enable the textbox again if needed.
            $(this).removeAttr("disabled");
         }
   });
}))
</script>
</head>
<body>


<?php get_header(); ?>


<header class="entry-header header-pesquisa">


	<div class="box-buscar">
		<form role="search" method="post" id="searchform" onsubmit="return false">
		  <div class="input-group">
		    <input type="text" class="campo-buscar" name="input_buscar" id="input_buscar" placeholder="Buscar" />
		  </div>
		</form>
	</div>
	
</header><!-- .entry-header -->



<div class="container resultado-pesquisa">
	<div class="row pesquisa-relacionada">
		<h4>Pesquisa relacionada<img src="img/busca.png" alt=""></h4>
		<p class="resultado-seg"></p>
	</div>
	<div class="row">

		<div class="col-sm-6">
			
			<div>
				<h2>Resultados da pesquisa <br><span>no site</span></h2>
			</div>

			<div class="site resultado_site">
			</div>

		</div>

		<div class="col-sm-6">
			
			<div>
				<h2>Resultados da pesquisa <br><span>no blog</span></h2>
			</div>
			<div class="blog resultado_blog">
			</div>

		</div>
	</div>
</div>


<?php get_footer(); ?>

</body>
</html>
