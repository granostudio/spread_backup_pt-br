<?php
/**
 * Template Name: Digital Studio
 */

get_header();
extract( get_fields() );

?>

<article id="servicos" <?php post_class(); ?>>

	<header class="entry-header" <?php fundo( umDe( $fundo, FUNDO_PADRAO ), 'full' ) ?>>
		<div class="header-caption">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-6 col_titulo anim">
						<?=
						E::h1( null, 'entry-title' )
							->span( $titulo1, 'linha1' )
							->span( $titulo2, 'linha2' )
							->span( $titulo3, 'linha3' )
						?>
					</div><!-- .col -->
					<div class="col-xs-12 col-sm-5 col-md-4 col_texto anim">
						<?=
						E::p( null, 'entry-desc' )
							->span( $desc, 'entry-desc-texto' )
						?>
					</div><!-- .col -->
				</div><!-- .row -->
			</div><!-- .container -->
		</div><!-- .header-caption -->
		<?php
		ancora(

			'#digital-studio',

			imgTema( 'seta_amarela.png', '&darr;', 'home_scroll_img' ),

			false,

		    'home_scroll amarelo'

			);
		?>
	</header><!-- .entry-header -->

	<div id="digital-studio" class="entry-main">
		<div class="entry-content">

			<?php
			// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
			// BLOCO 1

			extract( $bloco1[0], EXTR_PREFIX_ALL, 'bloco1' );
			?>

			<section id="bloco1" class="bloco">
				<div class="container">

					<div class="intro">
						<?=
						E::h2( null, 'intro_titulo' )
							->span( $bloco1_titulo1, 'linha1' )
							->span( $bloco1_titulo2, 'linha2' ) .
						E::p( $bloco1_intro, 'intro_texto' )
						?>
					</div>

					<div class="row colado equalizar_lista vert_ancestral">

						<div class="col-xs-12 col-sm-4">
							<div class="sidebox equalizar_item">
								<?php
								foreach ( $bloco1_itens as $item ) :
									extract( $item, EXTR_PREFIX_ALL, 'item' );
									?>
									<section class="sidebox_item">
										<?=
										E::div( obterImg( $item_icone, 'medium', cl('item_icone_img vert_item') ), 'item_icone vert_container' ) .
										E::h3( $item_nome, 'item_nome' )
										?>
										<ul class="item_texto">
											<?php
											listaElementos( 'li', explode( "\n", $item_texto ) );
											?>
										</ul><!-- .item_texto -->
									</section><!-- .sidebox_item -->
									<?php
								endforeach;
								?>
							</div><!-- .sidebox -->
						</div><!-- .col -->

						<div class="col-xs-12 col-sm-8">
							<div class="secao_grafico equalizar_item">
								<blockquote><p><?= $bloco1_frase1 ?></p></blockquote>
								<?php img(450,'full') ?>
								<blockquote><p><?= $bloco1_frase2 ?></p></blockquote>
								<div class="passos">
									<div class="container">
										<div class="row">
											<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
												<img src="<?php echo get_template_directory_uri()?>/imagens/digstu_prod1.png" alt="">
											</div>
											<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
												<img src="<?php echo get_template_directory_uri()?>/imagens/digstu_prod2.png" alt="">
											</div>
											<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
												<img src="<?php echo get_template_directory_uri()?>/imagens/digstu_prod3.png" alt="">
											</div>
										</div>
									</div>
								</div><!-- .passos -->
								<!-- <div class="passos-mobile desktop-sumir">
									<img src="<?php echo get_template_directory_uri()?>/imagens/digital-Studio-grafismo" alt="">
								</div> -->
							</div><!-- .secao_grafico -->
						</div><!-- .col -->

					</div><!-- .row -->

				</div><!-- .container -->
			</section><!-- .bloco -->

			<?php
			// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
			// BLOCO 2

			extract( $bloco2[0], EXTR_PREFIX_ALL, 'bloco2' );
			?>

			<section id="bloco2" class="bloco">
				<div class="container">
					<div class="row">

						<div class="col-xs-12 col-sm-4 col_lateral mobile-sumir">
							<?=
							E::div( obterImg( $bloco2_imagem_lateral, 'full', 'imagem_lateral_img' ), 'imagem_lateral' )
							?>
						</div><!-- .col -->

						<div class="col-xs-12 col-sm-8">
							<div class="intro">
								<?=
								E::h2( null, 'intro_titulo' )
									->span( $bloco2_titulo1, 'linha1' )
									->span( $bloco2_titulo2, 'linha2' ) .
								E::p( $bloco2_intro, 'intro_texto' )
								?>
							</div><!-- .intro -->
							<div class="grafico">
								<?php img( 452, 'full', 'grafico_img' ) ?>
							</div><!-- .grafico -->
							<?= E::p( enfase( $bloco2_texto_final ), 'texto_final' ) ?>
						</div><!-- .col -->

					</div><!-- .row -->
				</div><!-- .container -->
			</section><!-- .bloco -->

			<?php
			// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
			// BLOCO 3

			extract( $bloco3[0], EXTR_PREFIX_ALL, 'bloco3' );
			?>

			<section id="bloco3" class="bloco" <?php fundo( $bloco3_fundo, 'full' ) ?>>
				<div class="container">
					<div class="row largo">

						<div class="col-xs-12 col-sm-6">
							<?=
							E::h2( null, 'intro_titulo' )
								->span( $bloco3_titulo1, 'linha1' )
								->span( $bloco3_titulo2, 'linha2' )
							?>
						</div><!-- .col -->

						<div class="col-xs-12 col-sm-6">
							<?=
							E::p( $bloco3_texto, 'intro_texto' )
							?>
						</div><!-- .col -->

					</div><!-- .row -->
				</div><!-- .container -->
			</section><!-- .bloco -->



			<?php
			// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
			// BLOCO 4

			extract( $bloco4[0], EXTR_PREFIX_ALL, 'bloco4' );
			?>

			<section id="bloco4" class="bloco">
				<div class="container">

					<div class="intro">
						<?=
						E::h2( null, 'intro_titulo' )
							->span( $bloco4_titulo1, 'linha1' )
							->span( $bloco4_titulo2, 'linha2' )
						?>
					</div><!-- .intro -->

					<div class="logotipos vert_ancestral">
						<div class="row largo inline">
							<?php
							$classeCols = 'col-xs-4 col-sm-3 col-md-2';
							$n = 0;
							foreach ( $bloco4_logotipos as $item ) :
								$n++;
								?>
								<div class="<?= $classeCols ?>">
									<div class="logo">
										<?php img( $item['imagem'], 'medium', 'logo_img vert_item' ) ?>
									</div><!-- .logo -->
								</div><!-- .col -->
								<?php
								row( $n, $classeCols );
							endforeach;
							?>
						</div><!-- .row -->
					</div><!-- .logotipos -->

				</div><!-- .container -->
			</section><!-- .bloco -->

		</div><!-- .entry-content -->
	</div><!-- .entry-main -->

</article><!-- #servicos -->

<?php

get_footer();
